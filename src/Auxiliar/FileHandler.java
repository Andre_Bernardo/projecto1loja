package Auxiliar;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 * Exemplo do javadoc de uma class que permite leitura e escrita de ficheiros
 * de texto e objectos
 * @author Evgheni Polisciuc
 */

public class FileHandler {
    // readers
    private FileInputStream fileInputStream;
    private InputStreamReader inputStreamReader;
    private BufferedReader bufferedReader;
    private ObjectInputStream objectInputStream;

    // writers
    private FileOutputStream fileOutputStream;
    private OutputStreamWriter outputStreamWriter;
    private BufferedWriter bufferedWriter;
    private FileChannel fileChannelWriter;
    private FileLock fileLockWriter;
    private ObjectOutputStream objectOutputStream;

    /**
    * Exemplo do javadoc de um methodo que permite leitura de ficheros
    * @throws FileNotFoundException caso não exista o ficheiro pretendido
    * @param path recebe on nome do ficheiro a abrir
    * @return BufferedReader para leitura do ficheiro especificado
     */
    public BufferedReader openFileReader(String path) throws FileNotFoundException {
        fileInputStream = new FileInputStream(path);
        inputStreamReader = new InputStreamReader(fileInputStream);
        bufferedReader = new BufferedReader(inputStreamReader);
        return bufferedReader;
    }

    public void closeFileReader() throws IOException {
        if (fileInputStream != null) fileInputStream.close();
        if (inputStreamReader != null) inputStreamReader.close();
        if (bufferedReader != null) bufferedReader.close();
    }

    public ObjectInputStream openObjectReader(String path) throws IOException {
        fileInputStream = new FileInputStream(path);
        if (fileInputStream.available() == 0) return null;

        try{
            objectInputStream = new ObjectInputStream(fileInputStream);
        } catch (EOFException e){
            return null;
        }
        return objectInputStream;
    }

    public void closeObjectReader() throws IOException {
        if (fileInputStream != null) fileInputStream.close();
        if (objectInputStream != null) objectInputStream.close();
    }

    public BufferedWriter tryOpenAndLockFileWriter(String path) throws FileNotFoundException {
        fileOutputStream = new FileOutputStream(path);
        fileChannelWriter = fileOutputStream.getChannel();
        try {
            tryLockWriter();
            outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            bufferedWriter = new BufferedWriter(outputStreamWriter);
            return bufferedWriter;
        } catch (IOException e) {
            e.printStackTrace();
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    public BufferedWriter openFileWriter(String path) throws FileNotFoundException {
        fileOutputStream = new FileOutputStream(path);
        fileChannelWriter = fileOutputStream.getChannel();

        outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        bufferedWriter = new BufferedWriter(outputStreamWriter);

        return bufferedWriter;
    }

    public void closeFileWriter() throws IOException {
        if (bufferedWriter != null) bufferedWriter.close();
        if (outputStreamWriter != null) outputStreamWriter.close();
        closeFileOutputStream();
        closeFileChannelWriter();
    }

    public ObjectOutputStream tryOpenAndLockObjectWriter(String path) throws FileNotFoundException {
        fileOutputStream = new FileOutputStream(path);
        fileChannelWriter = fileOutputStream.getChannel();
        try {
            tryLockWriter();
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            return objectOutputStream;
        } catch (IOException e) {
            e.printStackTrace();
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    public ObjectOutputStream openObjectWriter(String path) throws IOException {
        fileOutputStream = new FileOutputStream(path);
        fileChannelWriter = fileOutputStream.getChannel();

        objectOutputStream = new ObjectOutputStream(fileOutputStream);
        return objectOutputStream;
    }

    public void closeObjectWriter() throws IOException {
        if (objectOutputStream != null) objectOutputStream.close();
        closeFileOutputStream();
        closeFileChannelWriter();
    }

    private void closeFileOutputStream() throws IOException {
        if (fileOutputStream != null) fileOutputStream.close();
    }

    private void closeFileChannelWriter() throws IOException {
        if (fileChannelWriter != null) fileChannelWriter.close();
    }

    public FileLock tryLockWriter() throws IOException {
        fileLockWriter = fileChannelWriter.lock();
        return fileLockWriter;
    }

    public void releaseLockWriter() throws IOException {
        if (!isFileWriterLocked()) fileLockWriter.release();
    }

    public boolean isFileWriterLocked(){
        return fileLockWriter == null;
    }
}
