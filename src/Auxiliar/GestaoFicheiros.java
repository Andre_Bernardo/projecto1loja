package Auxiliar;

/**
 * Interface Gestao Ficheiros
 * Permite o carregamento de dados a partir do ficheiro, descarregar para o ficheiro
 * Construção de dados para apresentar em formato de tabela
 * Pesquisa por Ids dos elementos que a compõem
 */
public interface GestaoFicheiros {
    String divisorColunas="!-!";
    String caminhoFuncionarios="data/Funcionarios.txt";
    String caminhoCliente="data/Clientes.txt";
    String caminhoHierarquias="data/HierarquiaProdutos.txt";
    String caminhoProdutos="data/Produtos.txt";
    String caminhoMensagens="data/Mensagens.txt";
    String caminhoEnvioMensagens="data/EnvioMensagens.txt";
    String caminhoLogs="data/Logs.txt";
    String caminhoEncomendas="data/Encomendas.txt";
    String caminhoVendas="data/Vendas.txt";
    String caminhoEstados="data/Estados.txt";
    String caminhoPagamentos="data/Pagamentos.txt";

    /**
     * Realizar uma pesquisa na listagem a través do ID
     * @param id int
     * @return Devolve o objecto com o valor de ID passado no argumento
     */
    Object pesquisa(int id);


    /**
     * Carregar informação do ficheiro para o programa
     */
    void downloadFicheiro();

    /**
     * Guardar a informação do programa no ficheiro
     */
    void uploadFicheiro();

    /**
     * Devolve o array que serve de cabeçalho para uma tabela
     * @return String[] - Cabeçalho
     */
    String[] listarCabecalhoTabela();

    /**
     * Devolve a matriz com os dados para uma tabela
     * @return Object[][] - Dados
     */
    Object[][] listarDadosTabela();

    /**
     * Adiciona elemento a listagem
     * @param object Object o objecto a adicionar a listage,
     */
    void adicionarElemento(Object object);

    /**
     * Conta quantos itens estão guardados no ficheiro .txt
     * @return quantidade total
     */
    int getTotal();

    /**
     * Verifica se o ficheiro existe no arranque, e caso não exista cria um novo ficheiro
     * @return true se existir o ficheiro.
     */
    boolean verificaFicheiro();
}
