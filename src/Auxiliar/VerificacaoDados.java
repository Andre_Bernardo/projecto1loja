package Auxiliar;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class implementada com a "tecnica" single-tone
 * Ao longo do programa só existe uma unica instancia desta classe
 *
 * Esta classe contem os metodos para a verificação de inputs do utilizador e é a responsavel pelo envio
 * de mensagens de erro ao utilizador
 */
public class VerificacaoDados {
    private static VerificacaoDados instancia;

    private static String[] mesesExtenso = {"janeiro", "fevereiro", "março","abril","maio","junho","julho","agosto","setembro","outubro","novembro","dezembro"};


    private Container painelPai;

    private VerificacaoDados(){
    }


    public void setPainelPai(Container painelPai) {
        this.painelPai = painelPai;
    }

    public static VerificacaoDados getInstancia(){
        if(instancia==null){
            instancia=new VerificacaoDados();

        }
        return instancia;
    }


    /**
     * Verifica se exite os caracteres de divisor de coluna no corpo de texto, caso exista dá erro
     * @param valor String - valor a testar
     * @return true - Se não contem caracteres que possam danificar o ficheiro, se não for dá popup erro
     */
    public boolean txtValido(String valor){
        if(valor.contains("!-!") ||valor.contains("\r") ||valor.contains("\n") || valor.contains("\t") ){
            JOptionPane.showMessageDialog(painelPai, "Não pode existir '!-!', quebras de linha nem tabulações nos campos de texto",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }
    /**
     * Verifica se o valor do campo tem conteudo
     * @param valor String valor / input dado pelo utilizador
     * @param campo String campo respectivo
     * @return true - Se conter informação, se não for dá popup erro
     */
    public boolean campoPreenchido(String valor, String campo){
        if(valor.length()<1){
            JOptionPane.showMessageDialog(painelPai,
                    "O campo '"+campo+"' é um campo obrigatório",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }
    /**
     * Verificar se o nome é apenas composto por letras, e começa por uma letra Maiuscula
     * @param nome String - nome a testar
     * @param campo String - campo em teste (inportante para dar erro)
     * @return true - Se apenas conter letras e começar por letra maiuscula, se não for dá popup erro
     */
    public boolean verificarNome(String nome, String campo){
        String expressao = "[A-ZÁÀÃÂÉÈÊÍÌÎÓÒÔÕÚÙÛÇ][a-záàãâéèêíìîóòôõúùûç]+";

        if(nome.matches(expressao)){
            return true;
        }
        JOptionPane.showMessageDialog(painelPai, "Valor '"+nome+"' não é válido no campo '"+campo+"'\n" +
                        "Só pode conter letras, e deve começar por letra maiuscula",
                "Erro Dados", JOptionPane.WARNING_MESSAGE);

        return false;
    }
    /**
     * Verificar se é um número inteiro
     * @param numero String - nome a testar
     * @param campo String - campo em teste (inportante para dar erro)
     * @return true - Se é um número inteiro, se não for dá popup erro
     */
    public boolean verificarNumeroInteiro(String numero, String campo){
        String expressao = "\\d+"; //pode conter 1 ou mais numeros inteiros
        if(numero.matches(expressao)){
            return true;
        }
        JOptionPane.showMessageDialog(painelPai, "Valor '"+numero+"' não é válido no campo '"+campo+"'\n" +
                        "Só pode conter números inteiros",
                "Erro Dados", JOptionPane.WARNING_MESSAGE);
        return false;
    }

    /**
     * Verificar se é um número inteiro
     * @param numero String - nome a testar
     * @param campo String - campo em teste (inportante para dar erro)
     * @return true - Se é um número decimal com 2 casas décimais, se não for dá popup erro
     */
    public boolean verificarNumeroDecimal(String numero, String campo){
        String expressao = "(\\d)+(\\.\\d{1,2})?"; //pode ou não conter até 2 casas décimais
        if(numero.matches(expressao)){
            return true;
        }
        JOptionPane.showMessageDialog(painelPai, "Valor '"+numero+"' não é válido no campo '"+campo+"'\n" +
                        "Número que pode conter até 2 casas décimais. O '.' é o separador decimal.",
                "Erro Dados", JOptionPane.WARNING_MESSAGE);
        return false;
    }

    /**
     * Verifica se a hiearquia não é null
     * @param object Hierarquia
     * @param campo campo para mostrar erro
     * @return true - Se é valido, se não for dá popup erro
     */
    public  boolean verificarHierarquia(Object object, String campo){
        if(object==null){
            JOptionPane.showMessageDialog(painelPai, "O campo '"+campo+"' não pode ser Vazio\n",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * Verifica se a pessoa tem uma data válida e se tem mais de 18 anos.
     * @param ano String - input do ano
     * @param mes String - input do mês
     * @param dia String - input do dia
     * @return true - Se a informação dada é válida e se idade do utilizador faz algum sentido,se não for dá popup erro
     */
    public boolean verificarDataNascimento(String ano, String mes, String dia){

        if(!verificarNumeroInteiro(ano, "Ano")){
            return false;
        }
        if(!verificarNumeroInteiro(mes, "Mes")){
            return false;
        }
        if(!verificarNumeroInteiro(dia, "Dia")){
            return false;
        }

        int anoInt=Integer.parseInt(ano);
        int mesInt=Integer.parseInt(mes)-1;//menos 1 é a correção necessária para o Gregorian Calendar
        int diaInt=Integer.parseInt(dia);

        //Verificar se o mes existe
        if(mesInt<0 || mesInt>11){
            JOptionPane.showMessageDialog(painelPai, "Mês inválido, " +
                            "o mês deve ser entre 1 [janeiro] e 12 [dezembro]",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        //Verificação do intervalo do dia, se o dia é valido para o ano / mes
        GregorianCalendar diaNascimento = new GregorianCalendar(anoInt,mesInt,1 );
        if(diaInt>diaNascimento.getActualMaximum(Calendar.DAY_OF_MONTH)){
            JOptionPane.showMessageDialog(painelPai, "Dia inválido," +
                            " para o mês "+mesesExtenso[mesInt]+", o dia deve ser entre "+diaNascimento.getActualMinimum(Calendar.DAY_OF_MONTH)+
                            " e "+diaNascimento.getActualMaximum(Calendar.DAY_OF_MONTH),
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        //Verificar se o utilizador tem 18+ anos
        diaNascimento.set(Calendar.DAY_OF_MONTH,diaInt);
        GregorianCalendar diaTeste= new GregorianCalendar();
        diaTeste.add(Calendar.YEAR,-18);//Pessoa deve nascer ANTES da data de HOJE - 18 anos
        if(diaNascimento.after(diaTeste)){
            JOptionPane.showMessageDialog(painelPai, "Data inválida. Utilizador deve ter mais do 18 anos",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        diaTeste.add(Calendar.YEAR,-82); //Pessoa deve nascer DEPOIS da data de HOJE - 100 anos
        if(diaNascimento.before(diaTeste)){
            JOptionPane.showMessageDialog(painelPai, "Data inválida. Utilizador não pode ter mais do 100 anos",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }


    /**
     * Verificar se o email é válido
     * @param email String email a verificar
     * @return true - se email for valido, se não for dá popup erro
     */
    public boolean verificarEmail(String email){
        String expressao = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+[/.][A-Za-z]{2,64}";

        if(email.matches(expressao)){
            return true;
        }
        JOptionPane.showMessageDialog(painelPai, "Valor '"+email+"' não é válido no campo 'Email'\n" +
                        "O email deve ter o seguinte formato: exemplo@exemplo.com",
                "Erro Dados", JOptionPane.WARNING_MESSAGE);

        return false;
    }

    /**
     * Verifificar se o valor é um número de telefone válido (9 digitos)
     * @param telefone String número a verificar
     * @return true - se telefone for valido, se não for dá popup erro
     */
    public boolean verificarTelefone(String telefone){
        String expressao ="[\\d]{9}";
        if(telefone.matches(expressao)){
            return true;
        }
        JOptionPane.showMessageDialog(painelPai, "Valor '"+telefone+"' não é válido no campo 'Telefone'\n" +
                        "O telefone deve ser composto por 9 algarismos",
                "Erro Dados", JOptionPane.WARNING_MESSAGE);
        return false;
    }

    /**
     * Verificar se dados referentes a um cartão são validos.
     * @param numero String numero do cartão
     * @param mes String mes validade do cartão
     * @param ano String ano validade do cartão
     * @param cvc String codigo de validação do cartão
     * @return true - se cartão for valido, se não for dá popup erro
     */
    public boolean verificarCartaoCredito(String numero, String mes, String ano, String cvc) {
        if(!verificarNumeroInteiro(numero, "Número Cartão")){
            return false;
        }
        if(numero.length()!=16){
            JOptionPane.showMessageDialog(painelPai,
                    "Número de cartão de crédito Inválido (deve ter 16 digitos).",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        if(!verificarNumeroInteiro(cvc, "CVC")){
            return false;
        }
        if(cvc.length()!=3){
            JOptionPane.showMessageDialog(painelPai,
                    "Código validação Cartão inválido (deve ter 3 digitos).",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;

        }
        if(!verificarNumeroInteiro(mes, "Mês")) {
            return false;
        }
        if(!verificarNumeroInteiro(ano, "Ano")){
            return false;
        }


        int anoInt=Integer.parseInt(ano);
        int mesInt=Integer.parseInt(mes)-1;//menos 1 é a correção necessária para o Gregorian Calendar
        //Verificar se o mes existe
        if(mesInt<0 || mesInt>11){
            JOptionPane.showMessageDialog(painelPai, "Mês inválido, " +
                            "o mês deve ser entre 1 [janeiro] e 12 [dezembro]",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        GregorianCalendar dataValidade = new GregorianCalendar(anoInt,mesInt,1,23,59);
        dataValidade.set(Calendar.DAY_OF_MONTH,dataValidade.getMaximum(Calendar.DAY_OF_MONTH));//Colocar sempre no ultimo dia do mes

        if(dataValidade.before(new GregorianCalendar())){
            JOptionPane.showMessageDialog(painelPai,
                    "A data de validade do cartão de crédito já expirou.",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        return true;
    }
}
