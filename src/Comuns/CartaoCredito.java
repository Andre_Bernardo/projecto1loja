package Comuns;

/**
 * Um tipo de pagamento
 */
public class CartaoCredito extends Pagamento {
    /**
     * Construtor para carregemento de dados
     * @param id int
     * @param valor double
     */
    public CartaoCredito (int id, double valor){
        super(id,valor);
    }

    /**
     * Construtor para novo pagamento
     * @param valor double
     */
    public CartaoCredito ( double valor){
        super(valor);
    }

    @Override
    public String tipoString() {
        return "Cartao Credito";
    }


    @Override
    public int tipo() {
        return 2;
    }
}
