package Comuns;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 * Listar pagamentos
 */
public class ListaPagamentos implements GestaoFicheiros {

    protected ArrayList<Pagamento> lista;
    protected String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaPagamentos(){
        lista=new ArrayList<>();
        localFicheiro=caminhoPagamentos;
    }

    /**
     * Construtor para procurar lista de pagamentos especifica
     * @param lista ArryList Pagamento
     */
    public ListaPagamentos(ArrayList<Pagamento> lista){
        this.lista=lista;
        localFicheiro=caminhoPagamentos;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaPagamentos(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<Pagamento> getLista() {
        return lista;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id=0;
            long referencia=0;
            double valor;
            char tipo;


            while (linha!=null) {
                linhaPartida = linha.split(divisorColunas);
                id = parseInt(linhaPartida[0]);
                valor = parseDouble(linhaPartida[1]);
                tipo = linhaPartida[2].charAt(0);
                referencia=Long.parseLong(linhaPartida[3]);

                switch (tipo) {
                    case '1': {
                        lista.add(new Multibanco(id, valor, referencia));
                    }
                    break;
                    case '2': {
                        lista.add(new CartaoCredito(id, valor));
                    }
                    break;
                    case '3': {
                        lista.add(new PayPall(id, valor));
                    }
                    break;
                }
                linha = conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(Pagamento pagamento: lista){
                long referencia=pagamento.tipo()==1?((Multibanco)pagamento).getReferencia():0;

                linha=pagamento.getId()+divisorColunas+
                        pagamento.getValor()+divisorColunas+
                        pagamento.tipo()+divisorColunas+
                        referencia;
                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }


    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            try {
                //Caso não exista cria um novo
                ficheiro.openFileWriter(localFicheiro);
                ficheiro.closeFileWriter();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    public void adicionarElemento(Object object) {
        lista.add((Pagamento) object);
    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[3];
        //Cabeçalho
        infoTabela[0]="id";
        infoTabela[1]="Valor";
        infoTabela[2]="Tipo de Pagamento";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][3];

        //Informação
        int i=0;
        for(Pagamento pagamento:lista){
            infoTabela[i]=pagamento.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public Object pesquisa(int id) {
        for(Pagamento pagamento:lista){
            if(pagamento.id==id){
                return pagamento;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(Pagamento pagamento:lista){
            count++;
        }
        return count;
    }

}
