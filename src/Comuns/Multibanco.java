package Comuns;

/**
 * Um tipo de pagamento
 *
 * Este guarda a referencia bancária do pagamento
 */
public class Multibanco extends Pagamento {

    protected static final int entidade = 12345;
    protected long referencia;


    /**
     * Construtor para carregemento de dados
     * @param id int
     * @param valor double
     * @param referencia long
     */
    public Multibanco (int id, double valor, long referencia){
        super(id,valor);
        this.referencia=referencia;
    }

    /**
     * Construtor para novo pagamento
     * @param valor double
     * @param referencia long
     */
    public Multibanco ( double valor, long referencia){
        super(valor);
        this.referencia=referencia;
    }

    /**
     * Deolve entidade do pagamento
     * @return int
     */
    public static int getEntidade() {
        return entidade;
    }

    /**
     * Devolve a referencia do pagamento
     * @return long
     */
    public long getReferencia() {
        return referencia;
    }

    @Override
    public String tipoString() {
        return "Multibanco";
    }

    @Override
    public int tipo() {
        return 1;
    }
}
