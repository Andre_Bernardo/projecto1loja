package Comuns;

/**
 * Pagamento é uma class abstraa
 * Todos os pagamentos tem um id e um valor
 */
public abstract class Pagamento {

    protected int id;
    private static int idgerado;
    protected double valor;
    private static String[] pagamentosPossiveis = {"Inválido",
            "Multibanco","Cartao Credito","Paypall"};

    public String pagamentoString(){
        return pagamentosPossiveis[tipo()];
    }

    public Pagamento(double valor){
        idgerado++;
        this.id=idgerado;
        this.valor=valor;
    }

    /**
     * Construtor para carregamento dados
     * @param id int
     * @param valor valorPagamento
     */
    public Pagamento(int id, double valor){
       if(id>idgerado){
           idgerado=id;
       }
        this.id=id;
        this.valor=valor;
    }

    /**
     * para escrita em tabela se for necessário
     * @return Object
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[2];
        linha[0]=getId();
        linha[1]=getValor();

        return linha;
    }

    public abstract String tipoString();
    public abstract int tipo();

    public int getId() {
        return id;
    }

    public double getValor() {
        return valor;
    }
}
