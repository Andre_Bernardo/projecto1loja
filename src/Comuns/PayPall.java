package Comuns;

/**
 * Um tipo de pagamento
 */
public class PayPall extends Pagamento {

    /**
     * Construtor para carregemento de dados
     * @param id int
     * @param valor double
     */
    public PayPall (int id, double valor){
        super(id,valor);
    }

    /**
     * Construtor para novo pagamento
     * @param valor double
     */
    public PayPall ( double valor){
        super(valor);
    }

    @Override
    public String tipoString() {
        return "PayPall";
    }

    @Override
    public int tipo() {
        return 3;
    }
}
