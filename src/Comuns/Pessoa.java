package Comuns;

import java.util.GregorianCalendar;

/**
 * Pessoa class que contem os dados das pessoas
 *
 * Esta class permite a alteração de dados, e password
 */
public abstract class Pessoa {
    private static int idGerado;

    protected int id;
    protected String priNome;
    protected String ultNome;
    protected GregorianCalendar dataNasc;
    protected String morada;
    protected String telefone;
    protected String email;
    protected String password;

    public Pessoa(){}

    /**
     * Construtor normal, criar um novo utilizador requer apenas nome, email, password e data nascimento (verificação de idade)
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param email String - email
     * @param password String - password
     */
    public Pessoa(String priNome, String ultNome, GregorianCalendar dataNasc, String email, String password) {
        idGerado++;
        this.id=idGerado;
        this.priNome = priNome;
        this.ultNome = ultNome;
        this.dataNasc = dataNasc;
        this.email = email;
        this.password = password;
        this.morada = "";
        this.telefone = "";
    }

    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int - id da pessoa
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     * @param password String - password
     */
    public Pessoa(int id, String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email, String password) {
        this.id=id;
        this.priNome = priNome;
        this.ultNome = ultNome;
        this.dataNasc = dataNasc;
        this.morada = morada;
        this.telefone = telefone;
        this.email = email;
        this.password = password;

        if(id>idGerado){
            idGerado=id;
        }
    }

    public abstract String tipoString();
    public abstract int tipo();

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[6];
        linha[0]=getPriNome();
        linha[1]=getUltNome();
        linha[2]=String.format("%04d-%02d-%02d",getAnoNasc(),getMesNasc()+1,getDiaNasc());
        linha[3]=getMorada();
        linha[4]=getTelefone();
        linha[5]=getEmail();
        return linha;
    }

    /**
     * Devole o ID da pessoa
     * @return String Devolve o ID da pessoa
     */
    public int getId(){
        return id;
    }

    /**
     * Devole o primeiro nome da pessoa
     * @return String Devolve o primeiro nome da pessoa
     */
    public String getPriNome() {
        return priNome;
    }

    /**
     * Devole o último nome da pessoa
     * @return String Devolve o último nome da pessoa
     */
    public String getUltNome() {
        return ultNome;
    }

    /**
     * Devole a Data Nascimento da pessoa
     * @return GregorianCalendar Devolve o Data Nascimento da pessoa
     */
    public GregorianCalendar getDataNasc() {
        return dataNasc;
    }

    /**
     * Devole a Dia do Nascimento da pessoa
     * @return int Devolve o Dia de Nascimento da pessoa
     */
    public int getDiaNasc(){
        return dataNasc.get(GregorianCalendar.DAY_OF_MONTH);
    }

    /**
     * Devole o Mês do Nascimento da pessoa
     * @return int Devolve o Mês de Nascimento da pessoa
     */
    public int getMesNasc(){
        return dataNasc.get(GregorianCalendar.MONTH);
    }

    /**
     * Devole o Ano do Nascimento da pessoa
     * @return int Devolve o Ano de Nascimento da pessoa
     */
    public int getAnoNasc(){
        return dataNasc.get(GregorianCalendar.YEAR);
    }

    /**
     * Devole a Morada da pessoa
     * @return String Devolve a Morada da pessoa
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Devole o telefone da pessoa
     * @return String Devolve o telefone da pessoa
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Devole o email da pessoa
     * @return String Devolve o email da pessoa
     */
    public String getEmail() {
        return email;
    }

    /**
     * Devole a password da pessoa
     * @return String Devolve a password da pessoa
     */
    public String getPassword() {
        return password;
    }

    public static int getIdGerado() {
        return idGerado;
    }

    public static void setIdGerado(int idGerado) {
        Pessoa.idGerado = idGerado;
    }

    /**
     * Edição de dados pessoais
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     */
    public void editarPerfil (String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email) {
        this.priNome = priNome;
        this.ultNome = ultNome;
        this.dataNasc = dataNasc;
        this.morada = morada;
        this.telefone = telefone;
        this.email = email;
    }

    /**
     * Altera a password do utilizador
     * @param password String
     */
    public void editarPassword(String password){
        this.password=password;
    }
}