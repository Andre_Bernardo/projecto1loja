package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import appCliente.Encomenda.Encomenda;
import appCliente.Encomenda.ListaEncomendas;
import appCliente.Encomenda.ListaVendas;
import appCliente.Pessoas.Cliente;
import appCliente.PrincipalCliente;
import appCliente.Produtos.ListaProdutos;
import appCliente.Produtos.Produto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;

public class TabelaCarrinho {
    private JPanel principal;
    private DefaultTableModel tableModel;

    private JLabel precoFinal;
    private JComboBox cmbPagamento;
    private JComboBox cmbEntrega;
    private JButton btnFinalizar;

    private VerificacaoDados verificacao;

    private Cliente utilizador;
    private ListaEncomendas listaEncomendas;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaCarrinho(Cliente utilizador, ListaEncomendas listaEncomendas){
        this.utilizador=utilizador;
        this.listaEncomendas=listaEncomendas;

        precoFinal = new JLabel();
        verificacao=VerificacaoDados.getInstancia();
        carregarInfo();

        JPanel painelcabecalho = new JPanel(new GridBagLayout());
        JPanel painelFinalizar = new JPanel();

        cmbPagamento=new JComboBox();
        cmbPagamento.addItem("Ecolher Pagamento");
        cmbPagamento.addItem("Referência Bancária");
        cmbPagamento.addItem("Cartão de Crédito");
        cmbPagamento.addItem("PayPal");
        cmbEntrega=new JComboBox();
        cmbEntrega.addItem("Modo Entrega");
        cmbEntrega.addItem("Levantar Loja");
        cmbEntrega.addItem("Envio para Morada");
        btnFinalizar=new JButton("Finalizar");
        btnFinalizar.setEnabled(false);

        painelFinalizar.add(precoFinal);
        painelFinalizar.add(cmbPagamento);
        painelFinalizar.add(cmbEntrega);
        painelFinalizar.add(btnFinalizar);

        JLabel lbldetalhes = new JLabel();
        lbldetalhes.setText("Duplo Clique no item para remover do carrinho");

        GridBagConstraints  posicaoFinalizar = new GridBagConstraints();
        posicaoFinalizar.anchor=GridBagConstraints.LINE_START;
        posicaoFinalizar.gridx = 1;
        posicaoFinalizar.gridy = 0;
        GridBagConstraints  posicaoLblDetalhes = new GridBagConstraints();
        posicaoLblDetalhes.anchor=GridBagConstraints.CENTER;
        posicaoLblDetalhes.gridx = 1;
        posicaoLblDetalhes.gridy = 1;

        painelcabecalho.add(painelFinalizar, posicaoFinalizar);
        painelcabecalho.add(lbldetalhes, posicaoLblDetalhes);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);
        TableColumn column6 = tabela.getColumnModel().getColumn(6);
        column6.setMaxWidth(70);
        TableColumn column7 = tabela.getColumnModel().getColumn(7);
        column7.setMaxWidth(70);
        TableColumn column8 = tabela.getColumnModel().getColumn(8);
        column8.setMaxWidth(70);
        TableColumn column9 = tabela.getColumnModel().getColumn(9);
        column9.setMaxWidth(80);
        TableColumn column10 = tabela.getColumnModel().getColumn(10);
        column10.setMaxWidth(70);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações

        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable tabela =(JTable)e.getSource();
                Point point =e.getPoint();
                int row =tabela.rowAtPoint(point);
                if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                    String identificador =(String)tabela.getValueAt(tabela.getSelectedRow(),0);
                    ListaProdutos listaProdutos = utilizador.verProdutos();
                    Produto produto = listaProdutos.pesquisa(identificador);
                    utilizador.removerItemCarrinho(listaEncomendas,produto);
                    JOptionPane.showMessageDialog(principal, "Item removido do carrinho.",
                            "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    actualizarInfo();
                }
            }
        });

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                actualizarInfo();
            }
        });

        btnFinalizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                finalizarCompra();
                btnFinalizar.setEnabled(false);
                cmbPagamento.setSelectedIndex(0);
                cmbEntrega.setSelectedIndex(0);
            }
        });

        cmbPagamento.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    verificarFinalizacao();
                }
            }
        });
        cmbEntrega.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    verificarFinalizacao();
                }
            }
        });
    }

    /**
     * Verificar se o botão de finalização pode estar disponivel.
     */
    private void verificarFinalizacao() {
        actualizarInfo();
        int nItens = tableModel.getRowCount();
        int pagamento = cmbPagamento.getSelectedIndex();
        int entrega = cmbEntrega.getSelectedIndex();

        if(pagamento>0 && entrega >0 && nItens>0){
            if(entrega==2 && PrincipalCliente.getUtilizador().getMorada().equals("")){
                btnFinalizar.setEnabled(false);
                btnFinalizar.setToolTipText("Só pode finalizar compra se: existir itens no carrinho, se tiver selecionado a forma de pagamento e a forma de entrega");
                JOptionPane.showMessageDialog(principal, "Morada não existente, por favor verifique o seu Perfil.",
                        "Erro Dados", JOptionPane.WARNING_MESSAGE);
            }else{
                btnFinalizar.setEnabled(true);
                btnFinalizar.setToolTipText("Passar para resumo e pagamento da encomenda");
            }
        }else {
            btnFinalizar.setEnabled(false);
            btnFinalizar.setToolTipText("Só pode finalizar compra se: existir itens no carrinho, se tiver selecionado a forma de pagamento e a forma de entrega");
        }
    }

    /**
     * Metodo para finalizar compra do carrinho
     */
    private void finalizarCompra() {
        int pagamento = cmbPagamento.getSelectedIndex();
        int entrega = cmbEntrega.getSelectedIndex();
        TabelaCarrinhoFinalizar finalizar = new TabelaCarrinhoFinalizar(pagamento,entrega);
        Container janela = principal.getParent().getParent().getParent(); //Pai do TabelaCarrinho é a Aba. Pai da Aba é a PaginaPrincipal. Pai PaginaPrincipal é a Janela
        janela.add(finalizar.adicionarJanela(),"Finalizar");
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.show(janela,"Finalizar");
    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        Encomenda carrinho = utilizador.procurarCarrinho(listaEncomendas);
        ListaVendas vendas = new ListaVendas(carrinho.getVendas());
        precoFinal.setText(String.format("Preço final c/IVA: %.2f€",carrinho.valorTotalCarrinho()));
        cabecalho=vendas.listarCabecalhoTabela();
        dados=vendas.listarDadosTabela();
    }

    /**
     * Actualizar informação na tabela
     */
    private void actualizarInfo(){
        carregarInfo();
        while (tableModel.getRowCount()>0){
            tableModel.removeRow(0);
        }
        for(int i=0; i<dados.length;i++){
            tableModel.addRow(dados[i]);
        }
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                if(getRowCount()>0){
                    return getValueAt(0, column).getClass();
                }
                return "".getClass();
            }
        };
        return auxiliar;
    }


    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }

}
