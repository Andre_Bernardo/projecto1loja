package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import Comuns.Multibanco;
import appCliente.Encomenda.Encomenda;
import appCliente.Pessoas.Cliente;
import appCliente.PrincipalCliente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TabelaCarrinhoFinalizar {
    private static String[] metodoPagamento = {"Inválido","Referência Bancária","Cartão de Crédito","Paypal"};
    private static String[] metodoEnvio = {"Inválido","Levantar Loja","Envio para Morada"};

    private JPanel principal;

    private long referenciaB=0;

    private JTextField txtNumeroCC;
    private JTextField txtCodigoCC;
    private JTextField txtMesCC;
    private JTextField txtAnoCC;

    private JTextField txtEmailPP;
    private JPasswordField txtPassPP;


    private VerificacaoDados verificacao;
    private Encomenda carrinho;
    private Cliente utilizador;
    private int pagamento;
    private boolean levantarLoja;

    public TabelaCarrinhoFinalizar(int pagamento, int entrega){
        Color corFundo= new Color(125, 186, 149);
        this.pagamento=pagamento;
        this.levantarLoja=(entrega==1);
        utilizador= (Cliente) PrincipalCliente.getUtilizador();
        carrinho=utilizador.procurarCarrinho(PrincipalCliente.getListaEncomendas());
        verificacao=VerificacaoDados.getInstancia();

        JLabel lblEncomenda = new JLabel(String.format("Encomenda Nº: %04d",carrinho.getId()));
        lblEncomenda.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        JLabel lblItens = new JLabel(String.format("Nº Itens :%4d",carrinho.numeroItens()));
        lblItens.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        JLabel lblModoEntrega = new JLabel();
        lblModoEntrega.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        if(entrega==2){
            lblModoEntrega.setText(String.format("Entrega: %s: %s",metodoEnvio[entrega],utilizador.getMorada()));
        }else {
            lblModoEntrega.setText(String.format("%s",metodoEnvio[entrega]));
        }
        lblModoEntrega.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        JLabel lblModoPagamento = new JLabel(String.format("Modo Pagamento: %s",metodoPagamento[pagamento]));
        lblModoPagamento.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        JPanel painelBotoes = new JPanel();
        painelBotoes.setBackground(corFundo);
        JButton btnPagar = new JButton("Pagar");
        JButton btnRetroceder = new JButton("Retroceder");
        painelBotoes.add(btnPagar);
        painelBotoes.add(btnRetroceder);


        Box caixaCentral = new Box(BoxLayout.Y_AXIS);
        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);
        caixaCentral.add(lblEncomenda);
        caixaCentral.add(lblItens);
        caixaCentral.add(lblModoEntrega);
        caixaCentral.add(lblModoPagamento);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(lblModoPagamento);

        JLabel lblPreco = new JLabel(String.format("Valor: %.2f€", carrinho.valorTotal()));
        lblPreco.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        if(pagamento==1){
            JLabel lblEntidade = new JLabel(String.format("Entidade: %05d", Multibanco.getEntidade()));
            lblEntidade.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            referenciaB=100000000 + (int) (Math.random()*(999999999-100000000));;
            JLabel lblRefenrecia = new JLabel(String.format("Referencia: %09d%04d", referenciaB,carrinho.getId()));
            lblRefenrecia.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            caixaCentral.add(lblEntidade);
            caixaCentral.add(lblRefenrecia);
            caixaCentral.add(lblPreco);
        }else if(pagamento==2){
            JPanel painelNumeroCC = new JPanel();
            JLabel lblNumeroCC= new JLabel("Número Cartão:");
            txtNumeroCC=new JTextField(14);
            painelNumeroCC.setBackground(corFundo);
            painelNumeroCC.add(lblNumeroCC);
            painelNumeroCC.add(txtNumeroCC);

            JPanel painelDadosCC=new JPanel();
            painelDadosCC.setBackground(corFundo);
            JLabel lblDataCC  =new JLabel("Data Validade:");
            lblDataCC.setToolTipText("No formato 'mm-aaaa'");
            txtMesCC=new JTextField(2);
            txtMesCC.setToolTipText("Mês - formato 'mm'");
            JLabel lblDataCC2  =new JLabel("-");
            txtAnoCC=new JTextField(4);
            txtAnoCC.setToolTipText("Ano - formato 'aaaa'");

            JLabel lblcodigoCC  =new JLabel("CVC:");
            lblcodigoCC.setToolTipText("Código de Validação do Cartão. Um código de 3 digitos existente no cartão");
            txtCodigoCC=new JTextField(3);

            painelDadosCC.add(lblDataCC);
            painelDadosCC.add(txtMesCC);
            painelDadosCC.add(lblDataCC2);
            painelDadosCC.add(txtAnoCC);
            painelDadosCC.add(lblcodigoCC);
            painelDadosCC.add(txtCodigoCC);
            caixaCentral.add(painelNumeroCC);
            caixaCentral.add(painelDadosCC);
            caixaCentral.add(lblPreco);

        }else if(pagamento==3){
            JPanel painelUser = new JPanel();
            painelUser.setBackground(corFundo);
            JLabel lblEmailPP = new JLabel("Email do Paypal:");
            txtEmailPP=new JTextField(20);
            painelUser.add(lblEmailPP);
            painelUser.add(txtEmailPP);

            JPanel painelPass = new JPanel();
            painelPass.setBackground(corFundo);
            JLabel lblPassPP = new JLabel("Password do Paypal:");
            txtPassPP=new JPasswordField(20);
            painelPass.add(lblPassPP);
            painelPass.add(txtPassPP);

            caixaCentral.add(painelUser);
            caixaCentral.add(painelPass);
            caixaCentral.add(lblPreco);

        }

        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);

        caixaCentral.add(Box.createVerticalGlue());

        principal=new JPanel(new GridBagLayout());
        principal.setBackground(corFundo);
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.add(caixaCentral,posicao);

        //Acções
        btnPagar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pagamentoValido()){
                    btnPagar.setEnabled(false);
                    utilizador.finalizarEncomenda(carrinho,levantarLoja,pagamento,referenciaB);
                    JOptionPane.showMessageDialog(principal, "Encomenda paga e acrescentada ao separador Encomendas.",
                            "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    retroceder();
                }
            }
        });

        btnRetroceder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retroceder();
            }
        });



    }

    /**
     * Verificação se dados do pagamento são válidos
     * @return true se for válido
     */
    private boolean pagamentoValido() {
        if(pagamento==1){
            return true;
        }else if(pagamento==2){
            String numeroCC = txtNumeroCC.getText();
            String mesCC = txtMesCC.getText();
            String anoCC = txtAnoCC.getText();
            String codigoCC = txtCodigoCC.getText();
            if(verificacao.verificarCartaoCredito(numeroCC,mesCC,anoCC,codigoCC)){
                return true;
            }

        }else if(pagamento==3){
            String email = txtEmailPP.getText();
            if(verificacao.verificarEmail(email)){
                return true;
            }
        }
        return false;
    }

    public JPanel adicionarJanela() {
        return principal;
    }

    private void retroceder(){
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.show(janela,"paginaPrincipal");
    }
}
