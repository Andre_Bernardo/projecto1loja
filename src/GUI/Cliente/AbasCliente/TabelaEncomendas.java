package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import appCliente.Encomenda.Encomenda;
import appCliente.Encomenda.ListaEncomendas;
import appCliente.Pessoas.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class TabelaEncomendas {
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;

    private  VerificacaoDados verificacao;
    private String[] cabecalho;
    private Object[][] dados;
    private ListaEncomendas listaEncomendas;
    private Cliente utilizador;

    public TabelaEncomendas(Cliente utilizador, ListaEncomendas listaEncomendas){
        this.utilizador=utilizador;
        this.listaEncomendas=listaEncomendas;

        verificacao=VerificacaoDados.getInstancia();
        carregarInfo();

        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(cabecalho);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JPanel painelPesquisa= new JPanel();
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);

        JPanel painelcabecalho = new JPanel(new GridBagLayout());
        JLabel lbldetalhes = new JLabel();
        lbldetalhes.setText("Duplo Click nas linhas da tabela para abrir os detalhes da Encomenda");

        GridBagConstraints  posicaoPesquisa = new GridBagConstraints();
        posicaoPesquisa.anchor=GridBagConstraints.LINE_START;
        posicaoPesquisa.gridx = 1;
        posicaoPesquisa.gridy = 0;
        GridBagConstraints  posicaoLblDetalhes = new GridBagConstraints();
        posicaoLblDetalhes.anchor=GridBagConstraints.CENTER;
        posicaoLblDetalhes.gridx = 1;
        posicaoLblDetalhes.gridy = 1;

        painelcabecalho.add(painelPesquisa,posicaoPesquisa);
        painelcabecalho.add(lbldetalhes, posicaoLblDetalhes);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações
        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });


        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable tabela =(JTable)e.getSource();
                Point point =e.getPoint();
                int row =tabela.rowAtPoint(point);
                if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                    int identificador =(Integer) tabela.getValueAt(tabela.getSelectedRow(),0);
                    abrirDetalhes(identificador);
                }
            }
        });

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });

        cartaTabela.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });


    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        ListaEncomendas listaUtilizador=utilizador.encomendasDoCliente();
        cabecalho=listaUtilizador.listarCabecalhoTabela();
        dados=listaUtilizador.listarDadosTabela();
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText().toLowerCase().strip();
        int indiceParamentro=-1;
        boolean encontado=false;
        ArrayList<Object[]> resultadosPesquisa = new ArrayList<>();

        for(int i=0; i<cabecalho.length && !encontado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontado=true;
            }
        }

        //Só actualizo a tabela se o indice for encontrado.
        if(encontado){

            //Condição para dar Reset a pesquisa
            if(pesquisa.equals("")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    tableModel.addRow(dados[i]);
                }
            //Condição pesquisa em String
            }else if(dados[0][indiceParamentro] instanceof String){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            //Condição pesquisa em Double
            }else if(dados[0][indiceParamentro] instanceof Double && verificacao.verificarNumeroDecimal(pesquisa, "Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]==Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            //Condição pesquisa em Inteiro
            }else if(dados[0][indiceParamentro] instanceof Integer && verificacao.verificarNumeroInteiro(pesquisa, "Pesquisa campo de Número Inteiro")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Integer)dados[i][indiceParamentro]==Integer.parseInt(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }


    /**
     * Abir janela para detalhe da encomenda
     * @param identificador String
     */
    private void abrirDetalhes(int identificador){
        Encomenda encomenda= (Encomenda) listaEncomendas.pesquisa(identificador);
        TabelaEncomendasDetalhe detalhes = new TabelaEncomendasDetalhe(utilizador,listaEncomendas,encomenda);

        principal.add(detalhes.adicionarComponente(), "Detalhe");
        CardLayout cartas = (CardLayout)principal.getLayout();
        cartas.show(principal,"Detalhe");
    }
}
