package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import appCliente.Encomenda.Encomenda;
import appCliente.Encomenda.EstadoEncomenda;
import appCliente.Encomenda.ListaEncomendas;
import appCliente.Encomenda.ListaVendas;
import appCliente.Pessoas.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.GregorianCalendar;

public class TabelaEncomendasDetalhe {
    private JPanel principal;
    private DefaultTableModel tableModel;

    private VerificacaoDados verificacao;

    private Encomenda encomenda;
    private Cliente utilizador;
    private ListaEncomendas listaEncomendas;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaEncomendasDetalhe(Cliente utilizador, ListaEncomendas listaEncomendas, Encomenda encomenda){
        this.utilizador=utilizador;
        this.listaEncomendas=listaEncomendas;
        this.encomenda=encomenda;

        verificacao=VerificacaoDados.getInstancia();
        carregarInfo();

        JPanel painelcabecalho = new JPanel(new GridBagLayout());

        JLabel lblDadosGerais = new JLabel(String.format("Encomenda Nº%04d realizada a %tF %tR",encomenda.getId(), encomenda.getEstado().getDataEncomenda(), encomenda.getEstado().getDataEncomenda()));
        JLabel lblDadosPagamento = new JLabel(String.format("Valor Total %.2f€; Metodo Pagamento: %s",encomenda.valorTotal(), encomenda.getPagamento().tipoString()));
        JLabel lblEstado = new JLabel(String.format("Estado: %s; Ultima Actualização: %tF %tR",encomenda.getEstado().estadoString(), encomenda.getEstado().ultimaActualizacao(),encomenda.getEstado().ultimaActualizacao()));



        GridBagConstraints  posicaoLblDadosGerais = new GridBagConstraints();
        posicaoLblDadosGerais.anchor=GridBagConstraints.LINE_START;
        posicaoLblDadosGerais.insets=new Insets(2,5,2,5);
        posicaoLblDadosGerais.gridx = 0;
        posicaoLblDadosGerais.gridy = 0;
        posicaoLblDadosGerais.ipadx=50;
        posicaoLblDadosGerais.ipady=5;

        GridBagConstraints  posicaoLblPagamento = new GridBagConstraints();
        posicaoLblPagamento.insets=new Insets(2,5,2,5);
        posicaoLblPagamento.anchor=GridBagConstraints.LINE_START;
        posicaoLblPagamento.gridx = 0;
        posicaoLblPagamento.gridy = 1;
        posicaoLblPagamento.ipadx=50;

        GridBagConstraints  posicaoLblEstado = new GridBagConstraints();
        posicaoLblEstado.insets=new Insets(2,5,2,5);
        posicaoLblEstado.anchor=GridBagConstraints.LINE_START;
        posicaoLblEstado.gridx = 0;
        posicaoLblEstado.gridy = 2;
        posicaoLblEstado.ipadx=50;
        posicaoLblPagamento.ipady=5;

        JPanel painelBotoes=new JPanel(new GridBagLayout());
        JButton btnRetroceder = new JButton("Retroceder");
        JButton btnRecebida= new JButton();
        JButton btnNaoRecebida= new JButton();

        GridBagConstraints posicaoBtnRetroceder = new GridBagConstraints();
        posicaoBtnRetroceder.fill=GridBagConstraints.HORIZONTAL;
        posicaoBtnRetroceder.insets=new Insets(2,5,2,5);
        posicaoBtnRetroceder.gridy=0;
        GridBagConstraints posicaoBtnRecebida = new GridBagConstraints();
        posicaoBtnRecebida.insets=new Insets(2,5,2,5);
        posicaoBtnRecebida.fill=GridBagConstraints.HORIZONTAL;
        posicaoBtnRecebida.gridy=1;

        GridBagConstraints posicaoBtnNaoRecebida = new GridBagConstraints();
        posicaoBtnNaoRecebida.insets=new Insets(2,5,2,5);
        posicaoBtnNaoRecebida.fill=GridBagConstraints.HORIZONTAL;
        posicaoBtnNaoRecebida.gridy=2;

        painelBotoes.add(btnRetroceder,posicaoBtnRetroceder);


        EstadoEncomenda estado= encomenda.getEstado();
        if(estado.getTipoEstado()==3 || estado.getTipoEstado()==4){
            btnRecebida.setText("Recebida");
            painelBotoes.add(btnRecebida,posicaoBtnRecebida);
        }

        if(estado.getTipoEstado()==3 && estado.getDataChegada().before(new GregorianCalendar())){
            btnNaoRecebida.setText("Não Recebida");
            painelBotoes.add(btnNaoRecebida,posicaoBtnNaoRecebida);
        }

        GridBagConstraints  posicaoPainelBotoes = new GridBagConstraints();
        posicaoPainelBotoes.anchor=GridBagConstraints.LINE_END;
        posicaoPainelBotoes.fill=GridBagConstraints.VERTICAL;
        posicaoPainelBotoes.gridheight=3;
        posicaoPainelBotoes.gridx = 1;




        painelcabecalho.add(lblDadosGerais,posicaoLblDadosGerais);
        painelcabecalho.add(lblDadosPagamento,posicaoLblPagamento);
        painelcabecalho.add(lblEstado,posicaoLblEstado);
        painelcabecalho.add(painelBotoes,posicaoPainelBotoes);


        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações
        principal.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                actualizarInfo();
            }
        });

        btnRetroceder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retroceder();
            }
        });

        btnRecebida.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(principal, "Tem a certeza que quer dar a encomenda como recebida?",
                        "Confirmação", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if(input==0){
                    if(utilizador.encomendaEntrege(encomenda)){
                        JOptionPane.showMessageDialog(principal, "Estado Encomenda actualizado.",
                                "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        retroceder();
                    }else{
                        JOptionPane.showMessageDialog(principal, "Erro na aquisição de dados.",
                                "Erro Sistema", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        btnNaoRecebida.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(principal, "Tem a certeza que quer dar a encomenda como não Recebida?",
                        "Confirmação", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if(input==0){
                    if(utilizador.encomendaNaoEntrege(encomenda)){
                        JOptionPane.showMessageDialog(principal, "Estado Encomenda actualizado.",
                                "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        retroceder();
                    }else{
                        JOptionPane.showMessageDialog(principal, "Erro na aquisição de dados.",
                                "Erro Sistema", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });



    }

    /**
     * Retroceder para a tabela de encomendas
     */
    private void retroceder() {
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.first(janela);
    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){

        Encomenda encomenda = (Encomenda) listaEncomendas.pesquisa(this.encomenda.getId());
        ListaVendas vendas = new ListaVendas(encomenda.getVendas());
        cabecalho=vendas.listarCabecalhoTabela();
        dados=vendas.listarDadosTabela();
    }

    private void actualizarInfo(){
        carregarInfo();
        while (tableModel.getRowCount()>0){
            tableModel.removeRow(0);
        }
        for(int i=0; i<dados.length;i++){
            tableModel.addRow(dados[i]);
        }
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                if(getRowCount()>0){
                    return getValueAt(0, column).getClass();
                }
                return "".getClass();
            }
        };
        return auxiliar;
    }


    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }

}
