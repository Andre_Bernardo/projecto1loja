package GUI.Cliente.AbasCliente;

import appCliente.Pessoas.Cliente;
import appCliente.Produtos.BaseUnidade;
import appCliente.Produtos.Departamento;
import appCliente.Produtos.ListaProdutos;
import appCliente.Produtos.Categoria;
import appCliente.Produtos.ListaHierarquias;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class TabelaEstatistica {
    Cliente utilizador;
    ListaProdutos listaProdutos;

    JPanel principal;

    ListaHierarquias listaHierarquias;

    JComboBox cmbDepartamento;
    JComboBox cmbCategoria;
    JComboBox cmbBaseUnidade;

    JTextField txtTotalGasto;
    JTextField txtTotalGastoDep;
    JTextField txtTotalGastoCat;
    JTextField txtTotalGastoUni;



    public TabelaEstatistica(Cliente utilizador){
        this.utilizador = utilizador;
        listaProdutos = utilizador.verProdutos();
        listaHierarquias = utilizador.verHierarquias();

        principal=new JPanel();

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Estatística do Cliente");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Total Gastos
        JLabel lblTotalGasto = new JLabel("Total de Gastos: ");
        lblTotalGasto.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        txtTotalGasto = new JTextField(5);
        txtTotalGasto.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        txtTotalGasto.setText(String.format("%.2f€",utilizador.gastouTotal()));
        txtTotalGasto.setEditable(false);
        JPanel painelTotalGasto= new JPanel();
        painelTotalGasto.add(lblTotalGasto);
        painelTotalGasto.add(txtTotalGasto);

        //SubTotaisGastos
        JPanel painelSubtotaisGastos = new JPanel(new GridBagLayout());

        JLabel lblGastos = new JLabel("Gastos");
        lblGastos.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.insets=new Insets(2,5,2,5);
        posicao.gridy=0;
        posicao.gridx=2;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(lblGastos,posicao);

        //Departamento
        posicao.gridy=1;
        posicao.gridx=0;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(new JLabel("Departamento"),posicao);

        cmbDepartamento = new JComboBox(listaHierarquias.departamentos());
        cmbDepartamento.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        cmbDepartamento.setSelectedIndex(0);
        posicao.gridy=1;
        posicao.gridx=1;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(cmbDepartamento,posicao);

        txtTotalGastoDep=new JTextField(5);
        txtTotalGastoDep.setEditable(false);
        posicao.gridy=1;
        posicao.gridx=2;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(txtTotalGastoDep,posicao);


        //Categoria
        posicao.gridy=2;
        posicao.gridx=0;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(new JLabel("Categoria"),posicao);

        cmbCategoria = new JComboBox(listaHierarquias.departamentos()[0].getComboBoxFilhos());
        cmbCategoria.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        cmbCategoria.setSelectedItem(0);
        posicao.gridy=2;
        posicao.gridx=1;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(cmbCategoria,posicao);

        txtTotalGastoCat=new JTextField(5);
        txtTotalGastoCat.setEditable(false);
        posicao.gridy=2;
        posicao.gridx=2;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(txtTotalGastoCat,posicao);


        //Base Unidade
        posicao.gridy=3;
        posicao.gridx=0;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(new JLabel("Base Unidade"),posicao);


        cmbBaseUnidade = new JComboBox(((Categoria)listaHierarquias.departamentos()[0].getComboBoxFilhos()[0]).getComboBoxFilhos());
        cmbBaseUnidade.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        cmbBaseUnidade.setSelectedItem(0);
        posicao.gridy=3;
        posicao.gridx=1;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(cmbBaseUnidade,posicao);

        txtTotalGastoUni=new JTextField(5);
        txtTotalGastoUni.setEditable(false);
        posicao.gridy=3;
        posicao.gridx=2;
        posicao.gridwidth=1;
        painelSubtotaisGastos.add(txtTotalGastoUni,posicao);


        //Caixa que inclui: Total Vendas, Por DEP, Por Categoria, Por UnidadeBase
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelTotalGasto);
        caixaCentral.add(painelSubtotaisGastos);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(Box.createVerticalGlue());



        //Caixa appCliente.Principal
        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicaoX = new GridBagConstraints();
        posicaoX.anchor=GridBagConstraints.CENTER;
        principal.setName("Estatistica");
        principal.add(caixaCentral,posicaoX);

        //Ações

        //no arranque
        actualizarProdutoDepartamento();
        actualizarProdutoCategoria();
        actualizarProdutoBaseUnidade();

        cmbDepartamento.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoDepartamento();
                actualizarProdutoCategoria();
                actualizarProdutoBaseUnidade();
            }
        });
        cmbCategoria.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoCategoria();
                actualizarProdutoBaseUnidade();
            }
        });
        cmbBaseUnidade.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoBaseUnidade();
            }
        });

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                actualizarDados();
            }
        });

    }

    /**
     * Painel que devolve o conteudo desta janela
     * @return Jpainel com o conteudo da janela
     */
    public JPanel adicionarComponente(){
        return principal;
    }

    /**
     * Actualizar os dados da hierarquia
     */
    private void actualizarDados(){
        txtTotalGasto.setText(String.format("%.2f€",utilizador.gastouTotal()));
        DefaultComboBoxModel novosDados = new DefaultComboBoxModel(listaHierarquias.departamentos());
        cmbDepartamento.setModel( novosDados );
        actualizarProdutoDepartamento();
        actualizarProdutoCategoria();
        actualizarProdutoBaseUnidade();
    }

    private void actualizarProdutoDepartamento(){
        Departamento departamentoSelecionado = ((Departamento)(cmbDepartamento.getSelectedItem()));
        txtTotalGastoDep.setText(String.format("%.2f€",((Cliente) utilizador).gastouTotalDep(departamentoSelecionado.getId())));

        DefaultComboBoxModel novosDados = new DefaultComboBoxModel(departamentoSelecionado.getComboBoxFilhos());
        cmbCategoria.setModel( novosDados );
    }

    private void actualizarProdutoCategoria(){
        Categoria categoriaSelecionada = ((Categoria)(cmbCategoria.getSelectedItem()));
        txtTotalGastoCat.setText(String.format("%.2f€",((Cliente) utilizador).gastouTotalCat(categoriaSelecionada.getId())));

        DefaultComboBoxModel novosDados = new DefaultComboBoxModel(categoriaSelecionada.getComboBoxFilhos());
        cmbBaseUnidade.setModel( novosDados );
    }

    private void actualizarProdutoBaseUnidade(){
        BaseUnidade baseUnidadeSelecionada = ((BaseUnidade)(cmbBaseUnidade.getSelectedItem()));
        txtTotalGastoUni.setText(String.format("%.2f€",((Cliente) utilizador).gastouTotalUni(baseUnidadeSelecionada.getId())));
    }

}
