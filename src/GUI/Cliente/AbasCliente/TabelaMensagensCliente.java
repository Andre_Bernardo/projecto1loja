package GUI.Cliente.AbasCliente;

import appCliente.Mensagem.ListaEnvioMensagens;
import appCliente.Mensagem.ListaMensagens;
import appCliente.Pessoas.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class TabelaMensagensCliente {
    private Cliente utilizador;
    private ListaEnvioMensagens envioMensagens;

    private JPanel principal;
    private DefaultTableModel tableModel;

    private String[] cabecalho;
    private Object[][] dados;


    public TabelaMensagensCliente(Cliente utilizador, ListaEnvioMensagens envioMensagens) {
        this.utilizador=utilizador;
        this.envioMensagens=envioMensagens;

        carregarInfo();

        principal = new JPanel();

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Mensagens da Loja");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setAutoCreateRowSorter(true);
        TableColumn coluna1 = tabela.getColumnModel().getColumn(0);
        coluna1.setMaxWidth(200);
        TableColumn coluna2 = tabela.getColumnModel().getColumn(1);
        coluna2.setMaxWidth(400);
        coluna2.setMinWidth(200);



        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(lblCabecalho, BorderLayout.NORTH);
        cartaTabela.add(new JPanel());
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        principal.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                actualizarInfo();
            }
        });
    }

    /**
     *
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){

        ListaMensagens listagem = utilizador.verMensagens(envioMensagens);
        cabecalho=listagem.listarCabecalhoTabela();
        dados=listagem.listarDadosTabela();
    }

    /**
     * Actualizar a informação das mensagens
     */
    private void actualizarInfo(){
        carregarInfo();
        while (tableModel.getRowCount()>0){
            tableModel.removeRow(0);
        }
        for(int i=0; i<dados.length;i++){
            tableModel.addRow(dados[i]);
        }
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Metodo que devolve a janela principal construida
     *
     * @return JPanel com a janela principal
     */

    public JPanel adicionarComponente() {
        return principal;
    }
}
