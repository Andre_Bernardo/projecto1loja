package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import appCliente.Pessoas.Cliente;
import appCliente.Produtos.ListaProdutos;
import appCliente.Produtos.Produto;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class TabelaProdutos {
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;
    private ListaProdutos listagem;

    private Cliente utilizador;
    private VerificacaoDados verificacao;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaProdutos(Cliente utilizador){
        verificacao=VerificacaoDados.getInstancia();
        this.utilizador=utilizador;

        carregarInfo();

        String[] camposPesquisa=new String[]{"Identificador", "Departamento","Categoria","Base Unidade","Nome","Marca",
                "Stock","Preço >","Preço <","Desconto >","Desconto <", "Preço Final >","Preço Final <"};

        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(camposPesquisa);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JPanel painelPesquisa= new JPanel();
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);

        if(utilizador.tipo()==2){
            JButton btnAdicionar=new JButton("Adicionar Produto");
            painelPesquisa.add(btnAdicionar);

            btnAdicionar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    abrirDetalhes(utilizador);
                }
            });
        }

        JPanel painelcabecalho = new JPanel(new GridBagLayout());
        JLabel lbldetalhes = new JLabel();
        lbldetalhes.setText("Duplo Click nas linhas da tabela para abrir os detalhes do Produto");

        GridBagConstraints  posicaoPesquisa = new GridBagConstraints();
        posicaoPesquisa.anchor=GridBagConstraints.LINE_START;
        posicaoPesquisa.gridx = 1;
        posicaoPesquisa.gridy = 0;
        GridBagConstraints  posicaoLblDetalhes = new GridBagConstraints();
        posicaoLblDetalhes.anchor=GridBagConstraints.CENTER;
        posicaoLblDetalhes.gridx = 1;
        posicaoLblDetalhes.gridy = 1;

        painelcabecalho.add(painelPesquisa,posicaoPesquisa);
        painelcabecalho.add(lbldetalhes, posicaoLblDetalhes);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);
        TableColumn column7 = tabela.getColumnModel().getColumn(7);
        column7.setMaxWidth(70);
        TableColumn column8 = tabela.getColumnModel().getColumn(8);
        column8.setMaxWidth(70);
        TableColumn column9 = tabela.getColumnModel().getColumn(9);
        column9.setMaxWidth(80);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações
        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });


        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Point point =e.getPoint();
                int row =tabela.rowAtPoint(point);
                if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                    String identificador =(String)tabela.getValueAt(tabela.getSelectedRow(),0);
                    abrirDetalhes(identificador, utilizador);
                }
            }
        });
        cartaTabela.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });


    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        listagem=utilizador.verProdutos();
        cabecalho=listagem.listarCabecalhoTabela();
        dados=listagem.listarDadosTabela();
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText().toLowerCase().strip();
        int indiceParamentro=-1;
        boolean encontrado=false;

        //Campos de pesquisa personalizados
        if(parametro.equals("Preço >") || parametro.equals("Preço <")){
            encontrado=true;
            indiceParamentro=7;
        }
        if(parametro.equals("Desconto >") || parametro.equals("Desconto <")){
            encontrado=true;
            indiceParamentro=8;
        }
        if(parametro.equals("Preço Final >") || parametro.equals("Preço Final <")){
            encontrado=true;
            indiceParamentro=9;
        }

        for(int i=0; i<cabecalho.length && !encontrado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontrado=true;
            }
        }

        //Só actualizo a tabela se o indice for encontrado.
        if(encontrado){
            //Condição para dar Reset a pesquisa
            if(pesquisa.equals("")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    tableModel.addRow(dados[i]);
                }

                //Condição Preço superior a
            }else if(parametro.equals("Preço >") && verificacao.verificarNumeroDecimal(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]>Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição Preço inferior a
            }else if(parametro.equals("Preço <") && verificacao.verificarNumeroDecimal(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]<Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }else if(parametro.equals("Desconto <") && verificacao.verificarNumeroInteiro(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]<Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }else if(parametro.equals("Desconto >") && verificacao.verificarNumeroInteiro(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]>Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }

                //Condição Preço Final Superior a
            }else if(parametro.equals("Preço Final >") && verificacao.verificarNumeroDecimal(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]>Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição Preço Final inferior a
            }else if(parametro.equals("Preço Final <") && verificacao.verificarNumeroDecimal(pesquisa,"Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]<Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição pesquisa em String
            }else if(dados[0][indiceParamentro] instanceof String){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição pesquisa em Double
            }else if(dados[0][indiceParamentro] instanceof Double && verificacao.verificarNumeroDecimal(pesquisa, "Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]==Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição pesquisa em Inteiro
            }else if(dados[0][indiceParamentro] instanceof Integer && verificacao.verificarNumeroInteiro(pesquisa, "Pesquisa campo de Número Inteiro")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Integer)dados[i][indiceParamentro]==Integer.parseInt(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }

    /**
     * Abrir a janela para visualizar (e/ou) editar um produto
     * @param identificador código de indentifcação do produto
     * @param utilizador utilizador com login
     */
    private void abrirDetalhes(String identificador, Cliente utilizador){
        ListaProdutos listaProdutos=new ListaProdutos();
        listaProdutos.downloadFicheiro();
        Produto produto = listaProdutos.pesquisa(identificador);
        if(produto!=null){
            TabelaProdutosDetalhes detalhes = new TabelaProdutosDetalhes(produto, utilizador);
            principal.add(detalhes.adicionarComponente(), "Detalhe");
            CardLayout cartas = (CardLayout)principal.getLayout();
            cartas.show(principal,"Detalhe");
        }else{
            JOptionPane.showMessageDialog(principal, "Hierarquia não encontrada! Identificador: "+identificador,
                    "Erro Leitura", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Abir janela para adicionar produto
     * @param utilizador utilizador com login
     */
    private void abrirDetalhes(Cliente utilizador){
        TabelaProdutosDetalhes detalhes = new TabelaProdutosDetalhes(null, utilizador);
        principal.add(detalhes.adicionarComponente(), "Detalhe");
        CardLayout cartas = (CardLayout)principal.getLayout();
        cartas.show(principal,"Detalhe");
    }
}
