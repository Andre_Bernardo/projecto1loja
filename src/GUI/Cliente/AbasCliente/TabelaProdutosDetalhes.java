package GUI.Cliente.AbasCliente;

import Auxiliar.VerificacaoDados;
import appCliente.Pessoas.Cliente;
import appCliente.PrincipalCliente;
import appCliente.Produtos.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TabelaProdutosDetalhes {
    private Cliente utilizador;
    private Produto produto;
    private ListaHierarquias listaHierarquias;
    private VerificacaoDados verificacao;



    private JPanel principal;

    private JTextField txtNome;
    private JTextField txtDescricao;
    private JTextField txtMarca;
    private JTextField txtStock;
    private JTextField txtUnidade;
    private JComboBox<Departamento> cmbDepartamento;
    private JComboBox<Categoria> cmbCategoria;
    private JComboBox<BaseUnidade> cmbBaseUnidade;
    private JTextField txtPreco;
    private JTextField txtIVA;
    private JTextField txtDesconto;
    private JLabel lblPrecoFinal;
    private JLabel lblpreco;
    private JSpinner sprQuantidade;




    public TabelaProdutosDetalhes(Produto produto, Cliente utilizador){

        verificacao=VerificacaoDados.getInstancia();
        this.produto=produto;
        this.utilizador=utilizador;

        ListaHierarquias listaHierarquias = new ListaHierarquias();
        listaHierarquias.downloadFicheiro();
        this.listaHierarquias=listaHierarquias;


        //Cabecalho
        JLabel lblCabecalho = new JLabel("Detalhe do Produto");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados Gerais
        JLabel lblDadosGerais = new JLabel();
        lblDadosGerais.setText(produto.getIdentificador()+" - "+produto.getNome());
        lblDadosGerais.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados nome
        JLabel lblNome = new JLabel("Nome:");
        txtNome = new JTextField(20);
        txtNome.setText(produto==null?"":produto.getNome());
        txtNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelNome = new JPanel();
        painelNome.add(lblNome);
        painelNome.add(txtNome);

        //Dados descricao
        JLabel lblDescricao = new JLabel("Descrição:");
        txtDescricao = new JTextField(40);
        txtDescricao.setText(produto==null?"":produto.getDescricao());
        txtDescricao.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelDescricao = new JPanel();
        painelDescricao.add(lblDescricao);
        painelDescricao.add(txtDescricao);

        //Dados marca
        JLabel lblMarca = new JLabel("Marca:");
        txtMarca = new JTextField(20);
        txtMarca.setText(produto==null?"":produto.getMarca());
        txtMarca.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelMarca = new JPanel();
        painelMarca.add(lblMarca);
        painelMarca.add(txtMarca);

        //Dados stock
        JLabel lblStock = new JLabel("Stock:");
        txtStock = new JTextField(5);
        if(produto.tipo()==1){
            txtStock.setText(Integer.toString(produto.getStock()));
        }else{
            txtStock.setText(Double.toString(((Granel)produto).getStockGranel()));
        }


        //Dados da unidade
        JLabel lblUnidade =  new JLabel("Unidade de Venda:");
        txtUnidade = new JTextField(produto==null?"uni":produto.getUnidade(),2);

        txtStock.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelStock = new JPanel();
        painelStock.add(lblMarca);
        painelStock.add(txtMarca);
        painelStock.add(lblStock);
        painelStock.add(txtStock);
        painelStock.add(lblUnidade);
        painelStock.add(txtUnidade);



        //Dados Departamento
        JLabel lblDepartamento = new JLabel("Departamento:");

        //Dados Categoria
        JLabel lblCategoria = new JLabel("Categoria:");


        //Dados BaseUnidade
        JLabel lblBaseUnidade = new JLabel("Base Unidade:");


        JPanel painelDepartamento= new JPanel();
        JTextField txtDepartamento = new JTextField();
        txtDepartamento.setText(produto.getDepartamento().toString());
        txtDepartamento.setEditable(false);

        JTextField txtCategoria= new JTextField();
        txtCategoria.setText(produto.getCategoria().toString());
        txtCategoria.setEditable(false);

        JTextField txtBaseUnidade = new JTextField();
        txtBaseUnidade.setText(produto.getUnidadeBase().toString());
        txtBaseUnidade.setEditable(false);

        painelDepartamento.add(lblDepartamento);
        painelDepartamento.add(txtDepartamento);
        painelDepartamento.add(lblCategoria);
        painelDepartamento.add(txtCategoria);
        painelDepartamento.add(lblBaseUnidade);
        painelDepartamento.add(txtBaseUnidade);

        //Dados preco base
        lblpreco = new JLabel(String.format("Preço €/%s:",produto==null?"uni":produto.getUnidade()));
        txtPreco = new JTextField(5);
        txtPreco.setText(Double.toString(produto==null?0:produto.getPreco()));
        txtPreco.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        //Dados iva
        JLabel lblIVA = new JLabel("IVA %:");
        txtIVA = new JTextField(3);
        txtIVA.setText(String.format("%.0f",produto==null?23:produto.getIva()*100));
        txtIVA.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        //Dados iva
        JLabel lblDesconto = new JLabel("Desconto %:");
        txtDesconto = new JTextField(3);
        txtDesconto.setText(String.format("%.0f",produto==null?0:produto.getDesconto()*100));
        txtDesconto.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        JPanel painelPreco = new JPanel();
        painelPreco.add(lblpreco);
        painelPreco.add(txtPreco);
        painelPreco.add(lblIVA);
        painelPreco.add(txtIVA);
        painelPreco.add(lblDesconto);
        painelPreco.add(txtDesconto);

        //Preço Final
        lblPrecoFinal = new JLabel(String.format("Preço final do produto por %s: %.2f€",
                produto==null?"uni":produto.getUnidade(), produto==null?0:produto.precoUnitarioVendaComDesconto()));
        lblPrecoFinal.setAlignmentX(JComponent.CENTER_ALIGNMENT);


        //Caixa Botoes:  Retroceder + adicionar carrinho
        JPanel painelBotoes = new JPanel();
        JButton btnRetro = new JButton("Retroceder");
        painelBotoes.add(btnRetro);



        //Caixa que central (que inclui todos os elementos)
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(lblDadosGerais);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelNome);
        caixaCentral.add(painelDescricao);
        caixaCentral.add(painelStock);
        caixaCentral.add(painelDepartamento);
        caixaCentral.add(painelPreco);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(lblPrecoFinal);
        caixaCentral.add(Box.createVerticalStrut(20));

        //Spiner da quantidade
        JLabel lblQuantidade = new JLabel("Quantidade:");
        sprQuantidade = new JSpinner(new SpinnerNumberModel(1,produto.tipo()==1?1:0.5,10,produto.tipo()==1?1:0.5));
        JLabel lblUnidadeEncomendar = new JLabel(produto.getUnidade()+" -");
        JLabel lblPrecoPrevisto = new JLabel(String.format("Custo: %.02f",produto.precoUnitarioVendaComDesconto()));
        JButton btnEncomendar = new JButton("Adicionar Carrinho");

        sprQuantidade.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                double quantidade = (double) sprQuantidade.getValue();

                if(produto.tipo()==1 && quantidade != Math.round(quantidade)){
                    JOptionPane.showMessageDialog(principal, "A quantidade deste produto só pode ser um número Inteiro\n",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }else{
                    lblPrecoPrevisto.setText(String.format("Custo: %.02f€",quantidade*produto.precoUnitarioVendaComDesconto()));

                }


            }
        });


        btnEncomendar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                encomendar();
            }
        });

        JPanel painelEncomendar = new JPanel();
        painelEncomendar.add(lblQuantidade);
        painelEncomendar.add(sprQuantidade);
        painelEncomendar.add(lblUnidadeEncomendar);
        painelEncomendar.add(lblPrecoPrevisto);
        painelEncomendar.add(btnEncomendar);

        caixaCentral.add(painelEncomendar);

        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());

        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.add(caixaCentral,posicao);

        txtNome.setEditable(false);
        txtDescricao.setEditable(false);
        txtMarca.setEditable(false);
        txtStock.setEditable(false);
        txtUnidade.setEditable(false);
        txtPreco.setEditable(false);
        txtIVA.setEditable(false);
        txtDesconto.setEditable(false);



        //acções
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retrocerder();
            }
        });

    }

    private void  retrocerder(){
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.first(janela);
    }

    private void encomendar() {
        Double quantidade = (double) sprQuantidade.getValue();

        //Verificar Se for um produto unitário a quantidade deve ser unitária.
        if(produto.tipo()==1 && quantidade==Math.round(quantidade)){
            utilizador.adicionarItemCarrinho(PrincipalCliente.getListaEncomendas(),produto,quantidade);
            JOptionPane.showMessageDialog(principal, "Item adicionado ao carrinho.",
                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
            retrocerder();
        }else if(produto.tipo()==2) {
            utilizador.adicionarItemCarrinho(PrincipalCliente.getListaEncomendas(),produto,quantidade);
            JOptionPane.showMessageDialog(principal, "Item adicionado ao carrinho.",
                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
            retrocerder();
        }
    }

    public JPanel adicionarComponente(){
        return principal;
    }

}
