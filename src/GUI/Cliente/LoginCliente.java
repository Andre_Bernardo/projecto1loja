package GUI.Cliente;

import Comuns.Pessoa;
import appCliente.Pessoas.ListaPessoas;
import appCliente.PrincipalCliente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginCliente {
    private JPanel principal;
    private JTextField txtUser;
    private JPasswordField txtPass;

    public LoginCliente(){
        Color corFundo= new Color(125, 186, 149);

        //Caixa Cabecalho
        JLabel lblicon = new JLabel();
        ImageIcon icon = new ImageIcon("resources/Icon.png");
        lblicon.setIcon(icon);
        lblicon.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        JLabel lblCabecalho = new JLabel("Login");
        JPanel cabecalho = new JPanel();
        cabecalho.add(lblCabecalho, BorderLayout.SOUTH);
        cabecalho.setBackground(corFundo);


        //Caixa UserName
        txtUser=new JTextField("email@exemplo.com",20);
        JLabel lblUser = new JLabel("Email:");
        lblUser.setLabelFor(txtUser);
        JPanel painelUser = new JPanel();
        painelUser.add(lblUser);
        painelUser.add(txtUser);
        painelUser.setBackground(corFundo);

        //Caixa Password
        txtPass=new JPasswordField(20);
        JLabel lblPass = new JLabel(" Pass:");
        lblUser.setLabelFor(txtPass);
        JPanel painelPass = new JPanel();
        painelPass.add(lblPass);
        painelPass.add(txtPass);
        painelPass.setBackground(corFundo);

        //Caixa Permitir ver pass introduzida
        JCheckBox cbPassVisivel = new JCheckBox("Pass Visível");
        cbPassVisivel.setBackground(corFundo);
        JPanel painelPassVisivel = new JPanel();
        painelPassVisivel.add(cbPassVisivel);
        painelPassVisivel.setBackground(corFundo);

        //Caixa Botoes
        JButton btnLogin = new JButton("Login");
        JButton btnSair = new JButton("Sair");
        JPanel painelBotoes = new JPanel(new GridLayout(1,5));
        JPanel vazio1=new JPanel();
        vazio1.setBackground(corFundo);
        painelBotoes.add(vazio1);
        painelBotoes.add(btnLogin);
        JPanel vazio2=new JPanel();
        vazio2.setBackground(corFundo);
        painelBotoes.add(vazio2);
        painelBotoes.add(btnSair);
        JPanel vazio3=new JPanel();
        vazio3.setBackground(corFundo);
        painelBotoes.add(vazio3);
        painelBotoes.setBackground(corFundo);


        //Caixa para registo
        JLabel lblRegisto = new JLabel("Anda não é Cliente?");
        JButton btnRegisto = new JButton("Registar");
        JPanel painelRegisto = new JPanel();
        painelRegisto.add(lblRegisto);
        painelRegisto.add(btnRegisto);
        painelRegisto.setBackground(corFundo);

        //Caixa que inclui: Username, Pass, ver Pass e botoes
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);

        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblicon);
        caixaCentral.add(cabecalho);
        caixaCentral.add(painelUser);
        caixaCentral.add(painelPass);
        caixaCentral.add(painelPassVisivel);
        caixaCentral.add(painelBotoes);
        caixaCentral.add(painelRegisto);
        caixaCentral.add(Box.createVerticalGlue());


        caixaCentral.setBackground(corFundo);

        //Caixa appCliente.Principal, esta é a que vai para o CARD da frame
        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("LoginPage");
        principal.add(caixaCentral,posicao);


        principal.setBackground(corFundo);



        //Acções
        cbPassVisivel.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange()==ItemEvent.SELECTED){
                    txtPass.setEchoChar((char)0);
                }else{
                    txtPass.setEchoChar('*');
                }
            }
        });

        btnSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        btnRegisto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.show(janela,"Registo");;
            }
        });

        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });
        txtPass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });

        txtUser.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (txtUser.getText().equals("email@exemplo.com")) {
                    txtUser.setText("");
                    txtUser.setForeground(Color.BLACK);
                }
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (txtUser.getText().isEmpty()) {
                    txtUser.setForeground(Color.GRAY);
                    txtUser.setText("email@exemplo.com");
                }
            }
        });

    }

    private void login(){
        ListaPessoas listagem=PrincipalCliente.getListaClientes();
        listagem.downloadFicheiro();

        String email = txtUser.getText().strip();
        String pass = String.valueOf(txtPass.getPassword()).strip();

        Pessoa utilizador;
        utilizador=listagem.login(email,pass);

        //Mensagem de erro
        if(utilizador==null){
            JOptionPane.showMessageDialog(principal.getParent(), "Utilizador ou Password Incorretos",
                    "Erro Login", JOptionPane.WARNING_MESSAGE);
        }else{
            txtUser.setText("");
        txtPass.setText("");
        PrincipalCliente.setUtilizador(utilizador);
        Container janela =principal.getParent();
        PaginaPrincipalCliente paginaPrincipal = new PaginaPrincipalCliente();
        janela.add(paginaPrincipal.adicionarJanela(),"paginaPrincipal");
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.show(janela,"paginaPrincipal");
    }
    }



    /**
     * Metodo que devolve a janela de login construida
     * @return JPanel com a janela de login
     */
    public JPanel adicionarJanela(){
        return principal;
    }

}
