package GUI.Cliente;

import GUI.Cliente.AbasCliente.*;
import appCliente.Pessoas.Cliente;
import appCliente.PrincipalCliente;
import GUI.Cliente.AbasCliente.TabelaEstatistica;

import javax.swing.*;
import java.awt.*;

public class PaginaPrincipalCliente {

    private JPanel principal;
    private TabelaProdutos tabelaProdutos;
    private TabelaCarrinho tabelaCarrinho;
    private TabelaEncomendas tabelaEncomendas;
    private TabelaMensagensCliente tabelaMensagensCliente;
    private TabelaEstatistica tabelaEstatistica;


    public PaginaPrincipalCliente(){
        Cliente utilizador= (Cliente)PrincipalCliente.getUtilizador();

        JPanel cabecalho= new CabecalhoCliente().adicionarComponente();

        JTabbedPane abas= new JTabbedPane();

        tabelaProdutos=new TabelaProdutos(utilizador);
        tabelaCarrinho= new TabelaCarrinho(utilizador, PrincipalCliente.getListaEncomendas());
        tabelaEncomendas=new TabelaEncomendas(utilizador, PrincipalCliente.getListaEncomendas());
        tabelaMensagensCliente=new TabelaMensagensCliente(utilizador, PrincipalCliente.getListaEnvioMensagens());//CONSTRUTOR COM UTILIZADOR NÃO É PRECISO??
        tabelaEstatistica= new TabelaEstatistica(utilizador);

        abas.add("Produtos",tabelaProdutos.adicionarComponente());
        abas.add("Carrinho",tabelaCarrinho.adicionarComponente());
        abas.add("Encomendas",tabelaEncomendas.adicionarComponente());
        abas.add("Mensagens",tabelaMensagensCliente.adicionarComponente());
        abas.add("Estatística",tabelaEstatistica.adicionarComponente());


        principal=new JPanel(new BorderLayout());
        principal.setName("PaginaPrincipal");
        principal.add(cabecalho, BorderLayout.NORTH);
        principal.add(abas, BorderLayout.CENTER);

        Color corFundo= new Color(125, 186, 149);
        principal.setBackground(corFundo);
        //acções

    }

    /**
     * Metodo que devolve a janela principal construida
     * @return JPanel com a janela principal
     */
    public JPanel adicionarJanela(){
        return principal;
    }
}
