package GUI.Cliente;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appCliente.Pessoas.ListaPessoas;
import appCliente.PrincipalCliente;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;

public class PerfilCliente {
    private Pessoa utilizador;
    private ListaPessoas listaPessoas;
    private VerificacaoDados verificacao;

    private JPanel principal;

    private JTextField txtPriNome;
    private JTextField txtUltNome;
    private JTextField txtDiaData;
    private JTextField txtMesData;
    private JTextField txtAnoData;
    private JTextField txtMorada;
    private JTextField txtTelefone;
    private JTextField txtEmail;
    private JPasswordField txtPass;
    private JPasswordField txtPassRep;//Quando se pretende mudar a pass deve-se repeti-la duas vezes por segurança e só avançar quando os dois campos são iguais.

    public PerfilCliente() {
        this.listaPessoas=PrincipalCliente.getListaClientes();
        this.utilizador = PrincipalCliente.getUtilizador();
        verificacao = VerificacaoDados.getInstancia();
        principal=new JPanel();
        Color corFundo= new Color(125, 186, 149);

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Detalhes do " + this.utilizador.tipoString());
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados PriNome com tentativas de alinhar à esquerda!
        JLabel lblPriNome = new JLabel("Primeiro Nome:");
        txtPriNome = new JTextField(this.utilizador.getPriNome(),20);
        txtPriNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelPriNome = new JPanel();
        painelPriNome.add(lblPriNome);
        painelPriNome.add(txtPriNome);
        painelPriNome.setBackground(corFundo);

        //Dados UltNome
        JLabel lblUltNome = new JLabel("Sobrenome:");
        txtUltNome = new JTextField(this.utilizador.getUltNome(),20);
        txtUltNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelUltNome = new JPanel();
        painelUltNome.add(lblUltNome);
        painelUltNome.add(txtUltNome);
        painelUltNome.setBackground(corFundo);

        //Dados Data Nascimento
        JLabel lblDataNasci = new JLabel("Data Nascimento:");
        lblDataNasci.setToolTipText("No formato 'dd-mm-aaaa'");
        txtDiaData = new JTextField(Integer.toString(this.utilizador.getDiaNasc()),2);
        txtDiaData.setToolTipText("Dia - formato 'dd'");
        JLabel lblSeparadorDiaMes = new JLabel("-");
        txtMesData = new JTextField(Integer.toString(this.utilizador.getMesNasc()+1),2);
        txtMesData.setToolTipText("Mês - formato 'mm'");
        JLabel lblSeparadorMesAno = new JLabel("-");
        txtAnoData = new JTextField(Integer.toString(this.utilizador.getAnoNasc()),4);
        txtAnoData.setToolTipText("Ano - formato 'aaaa'");
        JPanel painelDataNasci = new JPanel();
        painelDataNasci.add(lblDataNasci);
        painelDataNasci.add(txtDiaData);
        painelDataNasci.add(lblSeparadorDiaMes);
        painelDataNasci.add(txtMesData);
        painelDataNasci.add(lblSeparadorMesAno);
        painelDataNasci.add(txtAnoData);
        painelDataNasci.setBackground(corFundo);

        //Dados Morada
        JLabel lblMorada = new JLabel("Morada:");
        txtMorada = new JTextField(this.utilizador.getMorada(),20);
        JPanel painelMorada = new JPanel();
        painelMorada.add(lblMorada);
        painelMorada.add(txtMorada);
        painelMorada.setBackground(corFundo);

        //Dados Telefone
        JLabel lblTelefone = new JLabel("Telefone:");
        txtTelefone = new JTextField(this.utilizador.getTelefone(),20);
        JPanel painelTelefone = new JPanel();
        painelTelefone.add(lblTelefone);
        painelTelefone.add(txtTelefone);
        painelTelefone.setBackground(corFundo);

        //Dados email //não deve ser linear alterar o email!
        JLabel lblEmail = new JLabel("email:");
        txtEmail = new JTextField(this.utilizador.getEmail(),20);
        JPanel painelEmail = new JPanel();
        painelEmail.add(lblEmail);
        painelEmail.add(txtEmail);
        painelEmail.setBackground(corFundo);

        //Caixa Botoes: Alterar Dados
        JButton btnAltDados = new JButton("Alterar Dados");
        JPanel painelAltDados = new JPanel(new GridLayout(1,5));
        JPanel vazio1 = new JPanel();
        vazio1.setBackground(corFundo);
        painelAltDados.add(vazio1);
        painelAltDados.add(btnAltDados);
        JPanel vazio2 = new JPanel();
        vazio2.setBackground(corFundo);
        painelAltDados.add(vazio2);
        painelAltDados.setBackground(corFundo);

        //Dados Password
        JLabel lblPassword = new JLabel("Pass:");
        txtPass = new JPasswordField(20);
        JPanel painelPassword = new JPanel();
        painelPassword.add(lblPassword);
        painelPassword.add(txtPass);
        painelPassword.setBackground(corFundo);

        //Painel Password Reposicao
        JLabel lblPasswordRep = new JLabel("Repita Pass:");
        txtPassRep = new JPasswordField(20);
        JPanel painelPasswordRep = new JPanel();
        painelPasswordRep.add(lblPasswordRep);
        painelPasswordRep.add(txtPassRep);
        painelPasswordRep.setBackground(corFundo);

        //Caixa Botoes: AlterarPass + Retroceder
        JButton btnAltPass = new JButton("Alterar Pass");
        JButton btnRetro = new JButton("Retroceder");
        JPanel painelBotoes = new JPanel(new GridLayout(1,5));
        JPanel vazio3 = new JPanel();
        vazio3.setBackground(corFundo);
        painelBotoes.add(vazio3);
        painelBotoes.add(btnAltPass);
        JPanel vazio4 = new JPanel();
        vazio4.setBackground(corFundo);
        painelBotoes.add(vazio4);
        painelBotoes.add(btnRetro);
        JPanel vazio5 = new JPanel();
        vazio5.setBackground(corFundo);
        painelBotoes.add(vazio5);
        painelBotoes.setBackground(corFundo);

        //Caixa que inclui: Nome, Data Nascimento, Morada, Telefone, Email, Pass
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelPriNome);
        caixaCentral.add(painelUltNome);
        caixaCentral.add(painelDataNasci);
        caixaCentral.add(painelMorada);
        caixaCentral.add(painelTelefone);
        caixaCentral.add(painelEmail);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelAltDados);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelPassword);
        caixaCentral.add(painelPasswordRep);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());

        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("LoginPage");
        principal.add(caixaCentral,posicao);
        principal.setBackground(corFundo);

        //Ações
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.show(janela,"paginaPrincipal");
            }
        });

        btnAltDados.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actualizarDados();
            }
        });

        btnAltPass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actualizarPassword();

            }
        });

    }

    /**
     * Metodo que devolve a janela principal construida
     *
     * @return JPanel com a janela principal
     */
    public JPanel adicionarJanela() {
        return principal;
    }

    /**
     * Método que verifica os campos e actualiza os dados do utilizador (caso os campos sejam validos)
     */
    private void actualizarDados(){
        String priNome = txtPriNome.getText().strip();
        String ultNome = txtUltNome.getText().strip();
        String diaNasc = txtDiaData.getText().strip();
        String mesNasc = txtMesData.getText().strip();
        String anoNasc = txtAnoData.getText().strip();
        String morada = txtMorada.getText().strip();
        String telefone = txtTelefone.getText().strip();
        String email = txtEmail.getText().strip().toLowerCase();

        //1ª verificação se campos obrigatórios não estão vazios
        if(verificacao.campoPreenchido(priNome,"Primeiro Nome") && verificacao.campoPreenchido(ultNome,"Sobrenome") &&
                verificacao.campoPreenchido(diaNasc,"Dia Nascimento") && verificacao.campoPreenchido(mesNasc,"Mês Nascimento") && verificacao.campoPreenchido(anoNasc,"Ano Nascimento") &&
                verificacao.campoPreenchido(email,"Email")){
            //2º verificação validação de dados
            if(verificacao.verificarNome(priNome,"Primeiro Nome") && verificacao.verificarNome(ultNome,"Sobrenome") &&
            verificacao.verificarDataNascimento(anoNasc,mesNasc,diaNasc) && verificacao.verificarEmail(email) &&
                    (telefone.length()>0 && verificacao.verificarTelefone(telefone) || telefone.length()==0)
            ){

                listaPessoas.downloadFicheiro();

                //3º Verificação - Email Unico
                if(listaPessoas.emailUnico(email,utilizador.getId() )){
                    Pessoa pessoa=(Pessoa)listaPessoas.pesquisa(utilizador.getId());
                    pessoa.editarPerfil(priNome,ultNome,new GregorianCalendar(Integer.parseInt(anoNasc),Integer.parseInt(mesNasc)-1,Integer.parseInt(diaNasc)),morada,telefone,email);
                    listaPessoas.uploadFicheiro();
                    utilizador=pessoa;
                    PrincipalCliente.setUtilizador(pessoa);
                    JOptionPane.showMessageDialog(principal, "Dados actualizados com sucesso.",
                            "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);

                }else{
                    JOptionPane.showMessageDialog(principal, "O Email já está utilizado.",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
    }

    /**
     * Método que verifica os campos da password e actualiza os dados do utilizador (caso os campos sejam iguais)
     */
    private void actualizarPassword(){
        String password = String.valueOf(txtPass.getPassword());
        String passwordRep = String.valueOf(txtPassRep.getPassword());

        if(verificacao.campoPreenchido(password,"Password") && password.equals(passwordRep)){
            listaPessoas.downloadFicheiro();
            Pessoa pessoa=(Pessoa)listaPessoas.pesquisa(utilizador.getId());
            pessoa.editarPassword(password);
            listaPessoas.uploadFicheiro();
            JOptionPane.showMessageDialog(principal, "Password actualizada com sucesso.",
                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(principal, "Os valores dos campos 'Pass' e  'Repita a pass' não são iguais.",
                    "Erro Dados", JOptionPane.WARNING_MESSAGE);
        }

    }
}
