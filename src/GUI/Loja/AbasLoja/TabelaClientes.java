package GUI.Loja.AbasLoja;

import Comuns.Pessoa;
import appLoja.Pessoas.ListaPessoas;
import appLoja.Pessoas.UtilizadorLoja;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

public class TabelaClientes {
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;


    private UtilizadorLoja utilizador;
    private ListaPessoas listagem;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaClientes(UtilizadorLoja utilizador){
        this.utilizador=utilizador;
        carregarInfo();

        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(cabecalho);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JButton btnMensagem=new JButton("Enviar Mensagem");
        JPanel painelPesquisa= new JPanel();
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);
        if(((Pessoa)utilizador).tipo()==2){
            painelPesquisa.add(btnMensagem);
        }

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelPesquisa, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações
        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        btnMensagem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                TabelaMensagem mensagem = new TabelaMensagem(null);

                principal.add(mensagem.adicionarComponente(), "Mensagem");
                CardLayout cartas = (CardLayout)principal.getLayout();
                cartas.show(principal,"Mensagem");
            }
        });

        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });
        cartaTabela.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });
    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        listagem=utilizador.verClientes();
        cabecalho=listagem.listarCabecalhoTabela();
        dados=listagem.listarDadosTabela();
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return devolve DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText();
        int indiceParamentro=-1;
        boolean encontado=false;
        ArrayList<Object[]> resultadosPesquisa = new ArrayList<>();

        for(int i=0; i<cabecalho.length && !encontado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontado=true;
            }
        }

        //Só actualizo a tabela se o indice for encontrado.
        if(encontado){
            while (tableModel.getRowCount()>0){
                tableModel.removeRow(0);
            }

            for(int i=0; i<dados.length;i++){
                if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa.toLowerCase().strip())){
                    tableModel.addRow(dados[i]);
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }
}
