package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import appLoja.Encomendas.Encomenda;
import appLoja.Encomendas.ListaEncomendas;
import appLoja.Pessoas.Funcionario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class TabelaEncomendas {
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;

    private VerificacaoDados verificacao;
    private ListaEncomendas listaEncomendas;
    private String[] cabecalho;
    private Object[][] dados;
    private Funcionario utilizador;

    public TabelaEncomendas(Funcionario utilizador){
        this.utilizador=utilizador;
        verificacao=VerificacaoDados.getInstancia();
        carregarInfo();

        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(cabecalho);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JPanel painelPesquisa= new JPanel();
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);


        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelPesquisa, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        tabela.getTableHeader().setReorderingAllowed(false);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações
        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable tabela =(JTable)e.getSource();
                Point point =e.getPoint();
                int row =tabela.rowAtPoint(point);
                if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                    int identificador = (Integer) tabela.getValueAt(tabela.getSelectedRow(),0);
                    abrirPreparacao(identificador);
                }
            }
        });
        cartaTabela.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });
    }


    /**
     * Carregar a informação dos dados da tabelaaa
     */
    private void carregarInfo(){
        listaEncomendas=utilizador.verEncomendas();
        cabecalho=listaEncomendas.listarCabecalhoTabela();
        dados= listaEncomendas.listarDadosTabela();
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return devolve DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText().toLowerCase().strip();
        int indiceParamentro=-1;
        boolean encontado=false;
        ArrayList<Object[]> resultadosPesquisa = new ArrayList<>();

        for(int i=0; i<cabecalho.length && !encontado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontado=true;
            }
        }

        //Só actualizo a tabela se o indice for encontrado.
        if(encontado){

            //Condição para dar Reset a pesquisa
            if(pesquisa.equals("")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    tableModel.addRow(dados[i]);
                }
                //Condição pesquisa em String
            }else if(dados[0][indiceParamentro] instanceof String){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição pesquisa em Double
            }else if(dados[0][indiceParamentro] instanceof Double && verificacao.verificarNumeroDecimal(pesquisa, "Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]==Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
                //Condição pesquisa em Inteiro
            }else if(dados[0][indiceParamentro] instanceof Integer && verificacao.verificarNumeroInteiro(pesquisa, "Pesquisa campo de Número Inteiro")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Integer)dados[i][indiceParamentro]==Integer.parseInt(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }

    /**
     * Abrir a janela para visualizar (e/ou) editar um produto
     * '@param identificador' código de indentifcação do produto
     */
    private void abrirPreparacao(int identificador){
        Encomenda encomenda= (Encomenda) listaEncomendas.pesquisa(identificador);
        if(encomenda!=null){
            TabelaEncomendasPreparacao preparacao = new TabelaEncomendasPreparacao(encomenda, utilizador);
            principal.add(preparacao.adicionarComponente(), "Preparacao");
            CardLayout cartas = (CardLayout)principal.getLayout();
            cartas.show(principal,"Preparacao");
        }else{
            JOptionPane.showMessageDialog(principal, "Encomenda não encontrada! Numero Encomenda: "+identificador,
                    "Erro Leitura", JOptionPane.WARNING_MESSAGE);
        }
    }
}
