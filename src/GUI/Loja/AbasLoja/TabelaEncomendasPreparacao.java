package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import appLoja.Encomendas.Encomenda;
import appLoja.Encomendas.ListaEncomendas;
import appLoja.Encomendas.ListaVendas;
import appLoja.Pessoas.Funcionario;
import appLoja.Pessoas.Cliente;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TabelaEncomendasPreparacao {
    private Funcionario utilizador;
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtDiasEntrega;
    private JButton btnDespachar;

    private Encomenda encomenda;
    private ListaEncomendas listaEncomendas;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaEncomendasPreparacao(Encomenda encomenda, Funcionario utilizador){
        this.encomenda=encomenda;
        this.utilizador = utilizador;
        Color corEditavel = new Color(181, 212, 255);
        carregarInfo();

        JLabel lblDiasEntrega= new JLabel("Tempo Estimado Entrega");
        txtDiasEntrega=new JTextField(10);
        txtDiasEntrega.setText("2");
        if(encomenda.isLevantarLoja()==true){
            txtDiasEntrega.setText("0");
            txtDiasEntrega.setEditable(false);
        }
        JButton btnEnviarMensagem=new JButton("Enviar Mensagem");
        btnDespachar=new JButton("Despachar");
        JButton btnRetroceder = new JButton("Retroceder");
        JPanel painelDespachar= new JPanel();
        painelDespachar.add(lblDiasEntrega);
        painelDespachar.add(txtDiasEntrega);
        painelDespachar.add(btnEnviarMensagem);
        painelDespachar.add(btnDespachar);
        painelDespachar.add(btnRetroceder);


        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel){
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row,
                                             int column) {
                JComponent component = (JComponent) super.prepareRenderer(renderer, row, column);


                double quantidade = (double) this.getValueAt(row,3);
                double stock = (double) this.getValueAt(row,4);


                if( quantidade<=stock && column==5 && encomenda.getEstado().getTipoEstado()<3){
                    component.setBackground(corEditavel);
                }


                return component;
            }
        };
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelDespachar, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");
        verificarDespachar();


        //Acção no arranque
        if(encomenda.getEstado().getTipoEstado()==1){
            if(!((Funcionario)utilizador).prepararEncomenda(encomenda)){
                JOptionPane.showMessageDialog(principal, "Impossivel realizar actualização de estado",
                        "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }

        //ações

        btnEnviarMensagem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cliente cliente = encomenda.getCliente();
                TabelaMensagem mensagem = new TabelaMensagem(cliente);

                if(encomenda.getEstado().getTipoEstado()==1){
                    if(!((Funcionario)utilizador).prepararEncomenda(encomenda)){
                        JOptionPane.showMessageDialog(principal, "Impossivel realizar actualização de estado",
                                "Erro Sistema", JOptionPane.ERROR_MESSAGE);
                    }
                }

                principal.add(mensagem.adicionarComponente(), "Mensagem");
                CardLayout cartas = (CardLayout)principal.getLayout();
                cartas.show(principal,"Mensagem");
            }
        });

        btnDespachar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VerificacaoDados verificacaoDados = VerificacaoDados.getInstancia();
                String dias = txtDiasEntrega.getText();
                if(verificacaoDados.verificarNumeroInteiro(dias, "Dias entrega")){

                    int input = JOptionPane.showConfirmDialog(principal, "Tem a certeza que quer dar a encomenda como Despachada?",
                            "Confirmação", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if(input==0){
                        if(((Funcionario) utilizador).despacharEncomenda(encomenda, dias)){
                            JOptionPane.showMessageDialog(principal, "Encomenda Despachada",
                                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                            if (encomenda.isLevantarLoja()) {
                                ((Funcionario) utilizador).envioMensagem("Encomenda Despachada", "Encomenda " + encomenda.getId() + " pronta para levantar na loja.", encomenda.getCliente());
                            }
                            else{
                                ((Funcionario) utilizador).envioMensagem("Encomenda Despachada", "Encomenda " + encomenda.getId() + " enviada, tempo previsto para entrega "+ dias+ " dias.", encomenda.getCliente());
                            }
                            retroceder();
                        }else {
                            JOptionPane.showMessageDialog(principal, "Erro na aquisição de dados.",
                                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
                        }
                    }

                }
            }
        });

        btnRetroceder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retroceder();
            }
        });

        tableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                int linha= e.getFirstRow();
                int coluna=e.getColumn();

                String identificadorProduto= (String) tableModel.getValueAt(linha,0);
                int idProduto = Integer.parseInt(identificadorProduto.split(":")[3]);
                int idEncomenda=encomenda.getId();
                Boolean estado = (Boolean) tableModel.getValueAt(linha, coluna);
                verificarDespachar();

                if(!((Funcionario)utilizador).empacotarVenda(idProduto,idEncomenda,estado)){
                    JOptionPane.showMessageDialog(principal, "Impossivel de realizar empacotamento",
                            "Erro Sistema", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

    }

    /**
     * Retroceder para a tabela de encomendas
     */
    private void retroceder() {
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.first(janela);
    }

    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        listaEncomendas=utilizador.verEncomendas();
        Encomenda encomenda = (Encomenda) listaEncomendas.pesquisa(this.encomenda.getId());
        ListaVendas listaVendas = new ListaVendas(encomenda.getVendas());
        cabecalho=listaVendas.listarCabecalhoTabela();
        dados=listaVendas.listarDadosTabela();
    }

    /**
     * Verifica se pode despachar a encomenda
     */
    private void verificarDespachar(){

        if(encomenda.getEstado().getTipoEstado()>2){
            btnDespachar.setEnabled(false);
            return;
        }
        int numeroLinhas = tableModel.getRowCount();
        for(int i=0; i<numeroLinhas; i++){
            if(!(boolean)tableModel.getValueAt(i,5)){
                btnDespachar.setEnabled(false);
                return;
            }
        }
        btnDespachar.setEnabled(true);
    }

    /**
     * Gerar o modelo de dados que gera a tabela
     * @return devolve DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(encomenda.getEstado().getTipoEstado()>2){
                    return false;
                }

                double quantidade = (double) this.getValueAt(row,3);
                double stock = (double) this.getValueAt(row,4);


                if( quantidade<=stock && column==5){
                    return true;
                }
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }
}
