package GUI.Loja.AbasLoja;

import appLoja.Encomendas.ListaEncomendas;
import appLoja.Encomendas.ListaEstados;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.ListaPessoas;
import appLoja.Produtos.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class TabelaEstatistica {
    Gerente utilizador;

    JScrollPane principal;

    ListaHierarquias listaHierarquias;
    ListaProdutos listaProdutos;
    ListaPessoas listaClientes;
    ListaPessoas listaFuncionarios;
    ListaEncomendas listaEncomendas;
    ListaEstados listaEstados;

    JComboBox cmbDepartamento;
    JComboBox cmbCategoria;
    JComboBox cmbBaseUnidade;

    JTextField txtProdutoDepartamento;
    JTextField txtProdutoCategoria;
    JTextField txtProdutoBaseUnidade;

    JTextField txtStockDepartamento;
    JTextField txtStockCategoria;
    JTextField txtStockBaseUnidade;




    public TabelaEstatistica(Gerente utilizador){
        this.utilizador = utilizador;
        listaHierarquias=utilizador.verHierarquias();
        listaProdutos=utilizador.verProdutos();
        listaClientes=utilizador.verClientes();
        listaFuncionarios=utilizador.verFuncionarios();
        listaEncomendas=utilizador.verEncomendas();
        listaEstados=utilizador.verEstados();


        //Cabecalho
        JLabel lblCabecalho = new JLabel("Estatística da Loja");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Total Clientes
        Border contorno = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

        TitledBorder pessoasTitulo = BorderFactory.createTitledBorder(contorno, "Pessoas");
        pessoasTitulo.setTitleJustification(TitledBorder.CENTER);
        JPanel painelGerais = new JPanel(new GridBagLayout());
        painelGerais.setBorder(pessoasTitulo);
        GridBagConstraints posicaoDadsoGerais = new GridBagConstraints();
        posicaoDadsoGerais.insets=new Insets(2,5,2,5);
        posicaoDadsoGerais.ipadx=5;
        posicaoDadsoGerais.ipady=5;

        JLabel lblTotalClientes = new JLabel("Total Clientes:");
        posicaoDadsoGerais.gridy=0;
        posicaoDadsoGerais.gridx=0;
        posicaoDadsoGerais.gridwidth=1;
        posicaoDadsoGerais.gridheight=1;
        painelGerais.add(lblTotalClientes,posicaoDadsoGerais);

        JTextField txtTotalClientes = new JTextField(5);
        txtTotalClientes.setText(String.valueOf(listaClientes.getTotal()));
        txtTotalClientes.setEditable(false);
        posicaoDadsoGerais.gridy=0;
        posicaoDadsoGerais.gridx=1;
        posicaoDadsoGerais.gridwidth=1;
        posicaoDadsoGerais.gridheight=1;
        painelGerais.add(txtTotalClientes,posicaoDadsoGerais);

        JLabel lblTotalFuncionarios = new JLabel("Total Funcionários:");
        posicaoDadsoGerais.gridy=1;
        posicaoDadsoGerais.gridx=0;
        posicaoDadsoGerais.gridwidth=1;
        posicaoDadsoGerais.gridheight=1;
        painelGerais.add(lblTotalFuncionarios,posicaoDadsoGerais);

        JTextField txtTotalFuncionarios = new JTextField(5);
        txtTotalFuncionarios.setText(String.valueOf(listaFuncionarios.getTotal()));
        txtTotalFuncionarios.setEditable(false);
        posicaoDadsoGerais.gridy=1;
        posicaoDadsoGerais.gridx=1;
        posicaoDadsoGerais.gridwidth=1;
        posicaoDadsoGerais.gridheight=1;
        painelGerais.add(txtTotalFuncionarios,posicaoDadsoGerais);


        //SubTotaisProdutos
        TitledBorder produtosTitulo = BorderFactory.createTitledBorder(contorno, "Produtos");
        produtosTitulo.setTitleJustification(TitledBorder.CENTER);
        JPanel painelProdutos = new JPanel(new GridBagLayout());
        painelProdutos.setBorder(produtosTitulo);
        GridBagConstraints posicaoProduto = new GridBagConstraints();
        posicaoProduto.insets=new Insets(2,5,2,5);
        posicaoProduto.ipadx=5;
        posicaoProduto.ipady=5;


        JLabel lblTotal = new JLabel("Total");
        lblTotal.setAlignmentX(JComponent.LEFT_ALIGNMENT);
        posicaoProduto.gridy=1;
        posicaoProduto.gridx=1;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(lblTotal,posicaoProduto);


        JLabel lblItensProdutos = new JLabel("Itens");
        lblItensProdutos.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        posicaoProduto.gridy=0;
        posicaoProduto.gridx=2;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(lblItensProdutos,posicaoProduto);

        JLabel lblStockProdutos = new JLabel("Stocks");
        posicaoProduto.gridy=0;
        posicaoProduto.gridx=3;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(lblStockProdutos,posicaoProduto);

        JTextField txtTotalProdutos = new JTextField(5);
        txtTotalProdutos.setEditable(false);
        txtTotalProdutos.setText(String.format("%d",listaProdutos.getTotal()));
        posicaoProduto.gridy=1;
        posicaoProduto.gridx=2;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtTotalProdutos,posicaoProduto);

        JTextField txtTotalSotcks = new JTextField(5);
        txtTotalSotcks.setEditable(false);
        txtTotalSotcks.setText(String.format("%d",listaProdutos.getStocks()));
        posicaoProduto.gridy=1;
        posicaoProduto.gridx=3;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtTotalSotcks,posicaoProduto);

        //Departamento
        posicaoProduto.gridy=2;
        posicaoProduto.gridx=0;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(new JLabel("Departamento"),posicaoProduto);

        cmbDepartamento = new JComboBox(listaHierarquias.departamentos());
        cmbDepartamento.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        posicaoProduto.gridy=2;
        posicaoProduto.gridx=1;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(cmbDepartamento,posicaoProduto);

        txtProdutoDepartamento=new JTextField(5);
        txtProdutoDepartamento.setEditable(false);
        posicaoProduto.gridy=2;
        posicaoProduto.gridx=2;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtProdutoDepartamento,posicaoProduto);

        txtStockDepartamento=new JTextField(5);
        txtStockDepartamento.setEditable(false);
        posicaoProduto.gridy=2;
        posicaoProduto.gridx=3;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtStockDepartamento,posicaoProduto);

        //Categoria
        posicaoProduto.gridy=3;
        posicaoProduto.gridx=0;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(new JLabel("Categoria"),posicaoProduto);

        cmbCategoria = new JComboBox();
        cmbCategoria.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        posicaoProduto.gridy=3;
        posicaoProduto.gridx=1;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(cmbCategoria,posicaoProduto);

        txtProdutoCategoria=new JTextField(5);
        txtProdutoCategoria.setEditable(false);
        posicaoProduto.gridy=3;
        posicaoProduto.gridx=2;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtProdutoCategoria,posicaoProduto);

        txtStockCategoria=new JTextField(5);
        txtStockCategoria.setEditable(false);
        posicaoProduto.gridy=3;
        posicaoProduto.gridx=3;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtStockCategoria,posicaoProduto);

        //Base Unidade
        posicaoProduto.gridy=4;
        posicaoProduto.gridx=0;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(new JLabel("Base Unidade"),posicaoProduto);


        cmbBaseUnidade = new JComboBox();
        cmbBaseUnidade.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        posicaoProduto.gridy=4;
        posicaoProduto.gridx=1;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(cmbBaseUnidade,posicaoProduto);

        txtProdutoBaseUnidade=new JTextField(5);
        txtProdutoBaseUnidade.setEditable(false);
        posicaoProduto.gridy=4;
        posicaoProduto.gridx=2;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtProdutoBaseUnidade,posicaoProduto);

        txtStockBaseUnidade=new JTextField(5);
        txtStockBaseUnidade.setEditable(false);
        posicaoProduto.gridy=4;
        posicaoProduto.gridx=3;
        posicaoProduto.gridwidth=1;
        posicaoProduto.gridheight=1;
        painelProdutos.add(txtStockBaseUnidade,posicaoProduto);

        //Total Vendas
        String[] resumoVendas=utilizador.resumoVendasLoja(listaEncomendas);

        TitledBorder vendasTitulo = BorderFactory.createTitledBorder(contorno, "Vendas");
        vendasTitulo.setTitleJustification(TitledBorder.CENTER);

        JPanel painelVendas = new JPanel(new GridBagLayout());
        painelVendas.setBorder(vendasTitulo);
        GridBagConstraints posicaoVendas = new GridBagConstraints();
        posicaoVendas.insets=new Insets(2,5,2,5);
        posicaoVendas.ipadx=5;
        posicaoVendas.ipady=5;

        JLabel lblItensTotalVendas = new JLabel("Itens Vendidos:");
        posicaoVendas.gridy=0;
        posicaoVendas.gridx=0;
        posicaoVendas.gridwidth=1;
        posicaoVendas.gridheight=1;
        painelVendas.add(lblItensTotalVendas,posicaoVendas);

        JTextField txtItensTotalVendas = new JTextField(5);
        txtItensTotalVendas.setText(resumoVendas[0]);
        txtItensTotalVendas.setEditable(false);
        posicaoVendas.gridy=0;
        posicaoVendas.gridx=1;
        posicaoVendas.gridwidth=1;
        posicaoVendas.gridheight=1;
        painelVendas.add(txtItensTotalVendas,posicaoVendas);

        JLabel lblValorTotalVendas = new JLabel("Valor Total:");
        lblValorTotalVendas.setToolTipText("Inclui IVA");
        posicaoVendas.gridy=1;
        posicaoVendas.gridx=0;
        posicaoVendas.gridwidth=1;
        posicaoVendas.gridheight=1;
        painelVendas.add(lblValorTotalVendas,posicaoVendas);

        JTextField txtValorTotalVendas = new JTextField(10);
        txtValorTotalVendas.setToolTipText("Inclui IVA");
        txtValorTotalVendas.setText(resumoVendas[1]);
        txtValorTotalVendas.setEditable(false);
        posicaoVendas.gridy=1;
        posicaoVendas.gridx=1;
        posicaoVendas.gridwidth=1;
        posicaoVendas.gridheight=1;
        painelVendas.add(txtValorTotalVendas,posicaoVendas);

        //Encomendas
        String[] tempoPreparacao=utilizador.tempoPreparacao(listaEstados);
        String[] tempoDespachar=utilizador.tempoDespachar(listaEstados);

        TitledBorder encomendasTitulo = BorderFactory.createTitledBorder(contorno, "Encomendas");
        vendasTitulo.setTitleJustification(TitledBorder.CENTER);

        JPanel painelEncomendas = new JPanel(new GridBagLayout());
        painelEncomendas.setBorder(encomendasTitulo);
        GridBagConstraints posicaoEncomendas = new GridBagConstraints();
        posicaoVendas.insets=new Insets(2,5,2,5);
        posicaoVendas.ipadx=5;
        posicaoVendas.ipady=5;

        posicaoEncomendas.gridx=1;
        posicaoEncomendas.gridy=0;
        painelEncomendas.add(new JLabel("Média"),posicaoEncomendas);
        posicaoEncomendas.gridx=2;
        posicaoEncomendas.gridy=0;
        painelEncomendas.add(new JLabel("Desvio Padrão"),posicaoEncomendas);

        posicaoEncomendas.gridx=0;
        posicaoEncomendas.gridy=1;
        painelEncomendas.add(new JLabel("Tempo para Preparação"),posicaoEncomendas);

        JTextField txtPreparacaoMedia = new JTextField(5);
        txtPreparacaoMedia.setText(tempoPreparacao[0]);
        txtPreparacaoMedia.setEditable(false);
        posicaoEncomendas.gridx=1;
        posicaoEncomendas.gridy=1;
        painelEncomendas.add(txtPreparacaoMedia,posicaoEncomendas);

        JTextField txtPreparacaoDesvio = new JTextField(5);
        txtPreparacaoDesvio.setText(tempoPreparacao[1]);
        txtPreparacaoDesvio.setEditable(false);
        posicaoEncomendas.gridx=2;
        posicaoEncomendas.gridy=1;
        painelEncomendas.add(txtPreparacaoDesvio,posicaoEncomendas);

        posicaoEncomendas.gridx=0;
        posicaoEncomendas.gridy=2;
        painelEncomendas.add(new JLabel("Tempo para Despachar"),posicaoEncomendas);

        JTextField txtDespacharMedia = new JTextField(5);
        txtDespacharMedia.setText(tempoDespachar[0]);
        txtDespacharMedia.setEditable(false);
        posicaoEncomendas.gridx=1;
        posicaoEncomendas.gridy=2;
        painelEncomendas.add(txtDespacharMedia,posicaoEncomendas);

        JTextField txtDespacharDesvio = new JTextField(5);
        txtDespacharDesvio.setText(tempoDespachar[1]);
        txtDespacharDesvio.setEditable(false);
        posicaoEncomendas.gridx=2;
        posicaoEncomendas.gridy=2;
        painelEncomendas.add(txtDespacharDesvio,posicaoEncomendas);

        //Pagamentos
        String[][] valoresPagamentos=utilizador.metodosPagamentosUsados();

        TitledBorder pagamentosTitulo = BorderFactory.createTitledBorder(contorno, "Pagamentos");
        pagamentosTitulo.setTitleJustification(TitledBorder.CENTER);

        JPanel painelPagamentos = new JPanel(new GridBagLayout());
        painelPagamentos.setBorder(pagamentosTitulo);
        GridBagConstraints posicaoPagamentos = new GridBagConstraints();
        posicaoPagamentos.insets=new Insets(2,5,2,5);
        posicaoPagamentos.ipadx=5;
        posicaoPagamentos.ipady=5;

        posicaoPagamentos.gridx=1;
        posicaoPagamentos.gridy=0;
        painelPagamentos.add(new JLabel("Contagem"),posicaoPagamentos);
        posicaoPagamentos.gridx=2;
        posicaoPagamentos.gridy=0;
        painelPagamentos.add(new JLabel("Valor"),posicaoPagamentos);

        posicaoPagamentos.gridx=0;
        posicaoPagamentos.gridy=1;
        painelPagamentos.add(new JLabel("Referência Bancária"),posicaoPagamentos);

        JTextField txtContarMultibaco = new JTextField(5);
        txtContarMultibaco.setText(valoresPagamentos[0][0]);
        txtContarMultibaco.setEditable(false);
        posicaoPagamentos.gridx=1;
        posicaoPagamentos.gridy=1;
        painelPagamentos.add(txtContarMultibaco,posicaoPagamentos);

        JTextField txtValorMultibanco = new JTextField(5);
        txtValorMultibanco.setText(valoresPagamentos[0][1]);
        txtValorMultibanco.setEditable(false);
        posicaoPagamentos.gridx=2;
        posicaoPagamentos.gridy=1;
        painelPagamentos.add(txtValorMultibanco,posicaoPagamentos);

        posicaoPagamentos.gridx=0;
        posicaoPagamentos.gridy=2;
        painelPagamentos.add(new JLabel("Cartão Crédito"),posicaoPagamentos);

        JTextField txtContarCC = new JTextField(5);
        txtContarCC.setText(valoresPagamentos[1][0]);
        txtContarCC.setEditable(false);
        posicaoPagamentos.gridx=1;
        posicaoPagamentos.gridy=2;
        painelPagamentos.add(txtContarCC,posicaoPagamentos);

        JTextField txtValorCC = new JTextField(5);
        txtValorCC.setText(valoresPagamentos[1][1]);
        txtValorCC.setEditable(false);
        posicaoPagamentos.gridx=2;
        posicaoPagamentos.gridy=2;
        painelPagamentos.add(txtValorCC,posicaoPagamentos);

        posicaoPagamentos.gridx=0;
        posicaoPagamentos.gridy=3;
        painelPagamentos.add(new JLabel("PayPall"),posicaoPagamentos);

        JTextField txtContarPaypall = new JTextField(5);
        txtContarPaypall.setText(valoresPagamentos[2][0]);
        txtContarPaypall.setEditable(false);
        posicaoPagamentos.gridx=1;
        posicaoPagamentos.gridy=3;
        painelPagamentos.add(txtContarPaypall,posicaoPagamentos);

        JTextField txtValorPaypal = new JTextField(5);
        txtValorPaypal.setText(valoresPagamentos[2][1]);
        txtValorPaypal.setEditable(false);
        posicaoPagamentos.gridx=2;
        posicaoPagamentos.gridy=3;
        painelPagamentos.add(txtValorPaypal,posicaoPagamentos);

        //Caixa que inclui: Total Clientes,Funcionarios,Produtos,Vendas,
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(lblCabecalho);
        caixaCentral.add(Box.createVerticalStrut(10));
        caixaCentral.add(painelGerais);
        caixaCentral.add(Box.createVerticalStrut(10));
        caixaCentral.add(painelProdutos);
        caixaCentral.add(Box.createVerticalStrut(10));
        caixaCentral.add(painelVendas);
        caixaCentral.add(Box.createVerticalStrut(10));
        caixaCentral.add(painelEncomendas);
        caixaCentral.add(Box.createVerticalStrut(10));
        caixaCentral.add(painelPagamentos);
        caixaCentral.add(Box.createVerticalGlue());



        principal=new JScrollPane(caixaCentral);



        //Ações

        //no arranque
        actualizarProdutoDepartamento();
        actualizarProdutoCategoria();
        actualizarProdutoBaseUnidade();

        cmbDepartamento.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoDepartamento();
                actualizarProdutoCategoria();
                actualizarProdutoBaseUnidade();
            }
        });
        cmbCategoria.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoCategoria();
                actualizarProdutoBaseUnidade();
             }
        });
        cmbBaseUnidade.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                actualizarProdutoBaseUnidade();
            }
        });

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                actualizarDados();
            }
        });

    }

    /**
     * Painel que devolve o conteudo desta janela
     * @return Jpainel com o conteudo da janela
     */
    public JScrollPane adicionarComponente(){
        return principal;
    }

    /**
     * Actualizar os dados da hierarquia
     */
    private void actualizarDados(){
        DefaultComboBoxModel novosDados = new DefaultComboBoxModel(listaHierarquias.departamentos());
        cmbDepartamento.setModel( novosDados );
        actualizarProdutoDepartamento();
        actualizarProdutoCategoria();
        actualizarProdutoBaseUnidade();
    }

    private void actualizarProdutoDepartamento(){
        Departamento departamentoSelecionado = ((Departamento)(cmbDepartamento.getSelectedItem()));
        Object[] filhos =departamentoSelecionado==null?null:departamentoSelecionado.getComboBoxFilhos();
        int produtosDiferentes = departamentoSelecionado==null?0:departamentoSelecionado.produtosDiferentes(listaProdutos);
        int produtosStock = departamentoSelecionado==null?0:departamentoSelecionado.produtosStock(listaProdutos);
        txtProdutoDepartamento.setText(String.format("%d",produtosDiferentes));
        txtStockDepartamento.setText(String.format("%d",produtosStock));

        DefaultComboBoxModel novosDados;
        if(filhos==null){
            novosDados = new DefaultComboBoxModel();
        }else {
            novosDados = new DefaultComboBoxModel(filhos);
        }
        cmbCategoria.setModel( novosDados );
    }

    private void actualizarProdutoCategoria(){
        Categoria categoriaSelecionada = ((Categoria)(cmbCategoria.getSelectedItem()));
        Object[] filhos =categoriaSelecionada==null?null:categoriaSelecionada.getComboBoxFilhos();
        int produtosDiferentes = categoriaSelecionada==null?0:categoriaSelecionada.produtosDiferentes(listaProdutos);
        int produtosStock = categoriaSelecionada==null?0:categoriaSelecionada.produtosStock(listaProdutos);
        txtProdutoCategoria.setText(String.format("%d",produtosDiferentes));
        txtStockCategoria.setText(String.format("%d",produtosStock));

        DefaultComboBoxModel novosDados;
        if(filhos==null){
            novosDados = new DefaultComboBoxModel();
        }else {
            novosDados = new DefaultComboBoxModel(filhos);
        }
        cmbBaseUnidade.setModel( novosDados );
    }

    private void actualizarProdutoBaseUnidade(){
        BaseUnidade baseUnidadeSelecionada = ((BaseUnidade)(cmbBaseUnidade.getSelectedItem()));
        int produtosDiferentes = baseUnidadeSelecionada==null?0:baseUnidadeSelecionada.produtosDiferentes(listaProdutos);
        int produtosStock = baseUnidadeSelecionada==null?0:baseUnidadeSelecionada.produtosStock(listaProdutos);
        txtProdutoCategoria.setText(String.format("%d",produtosDiferentes));
        txtStockCategoria.setText(String.format("%d",produtosStock));
        txtProdutoBaseUnidade.setText(String.format("%d",produtosDiferentes));
        txtStockBaseUnidade.setText(String.format("%d",produtosStock));
    }
}
