package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appLoja.Pessoas.Funcionario;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.ListaPessoas;
import appLoja.PrincipalLoja;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;

public class TabelaFuncionariosAdicionar {
    private Pessoa utilizador;
    private VerificacaoDados verificacao;

    private JPanel principal;

    private JTextField txtPriNome;
    private JTextField txtUltNome;
    private JTextField txtDiaData;
    private JTextField txtMesData;
    private JTextField txtAnoData;
    private JTextField txtEmail;
    private JPasswordField txtPass;
    private JPasswordField txtPassRep;//Quando se pretende mudar a pass deve-se repeti-la duas vezes por segurança e só avançar quando os dois campos são iguais.


    TabelaFuncionariosAdicionar(){
        this.utilizador = PrincipalLoja.getUtilizador();
        principal=new JPanel();
        verificacao = VerificacaoDados.getInstancia();

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Adicionar Funcionário");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados PriNome com tentativas de alinhar à esquerda!
        JLabel lblPriNome = new JLabel("Primeiro Nome:");
        txtPriNome = new JTextField(20);
        txtPriNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelPriNome = new JPanel();
        painelPriNome.add(lblPriNome);
        painelPriNome.add(txtPriNome);

        //Dados UltNome
        JLabel lblUltNome = new JLabel("Sobrenome:");
        txtUltNome = new JTextField(20);
        txtUltNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelUltNome = new JPanel();
        painelUltNome.add(lblUltNome);
        painelUltNome.add(txtUltNome);

        //Dados Data Nascimento
        JLabel lblDataNasci = new JLabel("Data Nascimento:");
        lblDataNasci.setToolTipText("No formato 'dd-mm-aaaa'");
        txtDiaData = new JTextField(2);
        txtDiaData.setToolTipText("Dia - formato 'dd'");
        JLabel lblSeparadorDiaMes = new JLabel("-");
        txtMesData = new JTextField(2);
        txtMesData.setToolTipText("Mês - formato 'mm'");
        JLabel lblSeparadorMesAno = new JLabel("-");
        txtAnoData = new JTextField(4);
        txtAnoData.setToolTipText("Ano - formato 'aaaa'");
        JPanel painelDataNasci = new JPanel();
        painelDataNasci.add(lblDataNasci);
        painelDataNasci.add(txtDiaData);
        painelDataNasci.add(lblSeparadorDiaMes);
        painelDataNasci.add(txtMesData);
        painelDataNasci.add(lblSeparadorMesAno);
        painelDataNasci.add(txtAnoData);



        //Dados email //não deve ser linear alterar o email!
        JLabel lblEmail = new JLabel("email:");
        txtEmail = new JTextField(20);
        JPanel painelEmail = new JPanel();
        painelEmail.add(lblEmail);
        painelEmail.add(txtEmail);


        //Dados Password
        JLabel lblPassword = new JLabel("Pass:");
        txtPass = new JPasswordField(20);
        JPanel painelPassword = new JPanel();
        painelPassword.add(lblPassword);
        painelPassword.add(txtPass);

        //Painel Password Reposicao
        JLabel lblPasswordRep = new JLabel("Repita Pass:");
        txtPassRep = new JPasswordField(20);
        JPanel painelPasswordRep = new JPanel();
        painelPasswordRep.add(lblPasswordRep);
        painelPasswordRep.add(txtPassRep);

        //Caixa Botoes: AlterarPass + Retroceder
        JButton btnAdicionar = new JButton("Adicionar");
        JButton btnRetro = new JButton("Retroceder");
        JPanel painelBotoes = new JPanel(new GridLayout(1,5));
        painelBotoes.add(new JPanel());
        painelBotoes.add(btnAdicionar);
        painelBotoes.add(new JPanel());
        painelBotoes.add(btnRetro);
        painelBotoes.add(new JPanel());

        //Caixa que inclui: Nome, Data Nascimento, Morada, Telefone, Email, Pass
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelPriNome);
        caixaCentral.add(painelUltNome);
        caixaCentral.add(painelDataNasci);
        caixaCentral.add(painelEmail);
        caixaCentral.add(painelPassword);
        caixaCentral.add(painelPasswordRep);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());



        //Caixa appCliente.Principal
        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("LoginPage");
        principal.add(caixaCentral,posicao);

        //Ações

        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.first(janela);
            }
        });

          btnAdicionar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                novoFuncionario();


            }
        });


    }

    /**
     * Painel que devolve o conteudo desta janela
     * @return Jpainel com o conteudo da janela
     */
    public JPanel adicionarComponente(){
        return principal;
    }

    /**
     * Método que verifica os campos e adiciona funcionario (caso os campos sejam validos)
     */
    private void novoFuncionario(){
        String priNome = txtPriNome.getText().strip();
        String ultNome = txtUltNome.getText().strip();
        String diaNasc = txtDiaData.getText().strip();
        String mesNasc = txtMesData.getText().strip();
        String anoNasc = txtAnoData.getText().strip();
        String email = txtEmail.getText().strip().toLowerCase();
        String password = String.valueOf(txtPass.getPassword());
        String passwordRep = String.valueOf(txtPassRep.getPassword());

        //1ª verificação se campos obrigatórios não estão vazios
        if(verificacao.campoPreenchido(priNome,"Primeiro Nome") && verificacao.campoPreenchido(ultNome,"Sobrenome") &&
                verificacao.campoPreenchido(diaNasc,"Dia Nascimento") && verificacao.campoPreenchido(mesNasc,"Mês Nascimento") && verificacao.campoPreenchido(anoNasc,"Ano Nascimento") &&
                verificacao.campoPreenchido(email,"Email") && verificacao.campoPreenchido(password,"Password")){
            //2º verificação validação de dados
            if(verificacao.verificarNome(priNome,"Primeiro Nome") && verificacao.verificarNome(ultNome,"Sobrenome") &&
                    verificacao.verificarDataNascimento(anoNasc,mesNasc,diaNasc) && verificacao.verificarEmail(email)
            ){
              //3º verificação password
                if(password.equals(passwordRep)){
                    ListaPessoas listaPessoas;
                    listaPessoas = new ListaPessoas(1);
                    ListaPessoas listaPessoasAXU = new ListaPessoas(2);
                    listaPessoasAXU.downloadFicheiro();
                    listaPessoas.downloadFicheiro();

                    //4º verificacao email unico
                    if(listaPessoas.emailUnico(email,0 )){
                        Funcionario novoFuncionario= new Funcionario(priNome,ultNome,new GregorianCalendar(Integer.parseInt(anoNasc),Integer.parseInt(mesNasc)-1,Integer.parseInt(diaNasc)), email, password);

                        ((Gerente)utilizador).adicionarFuncionario(novoFuncionario);

                        JOptionPane.showMessageDialog(principal, "Funcionário adicionado com sucesso.",
                                "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        Container janela = principal.getParent();
                        CardLayout cartas = (CardLayout)janela.getLayout();
                        cartas.first(janela);
                    }else{
                        JOptionPane.showMessageDialog(principal, "O Email já está utilizado.",
                                "Erro Dados", JOptionPane.WARNING_MESSAGE);
                    }

                }else{
                    JOptionPane.showMessageDialog(principal, "Os valores dos campos 'Pass' e  'Repita a pass' não são iguais.",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }

            }
        }


    }
}
