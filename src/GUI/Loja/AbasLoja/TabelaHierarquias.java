package GUI.Loja.AbasLoja;

import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.UtilizadorLoja;
import appLoja.Produtos.HierarquiaProdutos;
import appLoja.Produtos.ListaHierarquias;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class TabelaHierarquias {
    private Gerente utilizador;


    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;

    private ListaHierarquias listagem;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaHierarquias(Gerente utilizador){
        this.utilizador=utilizador;
        carregarInfo();


        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(cabecalho);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JPanel painelPesquisa= new JPanel();
        JButton btnDepartamento=new JButton("Adicionar Departamento");
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);
        painelPesquisa.add(btnDepartamento);

        JPanel painelcabecalho = new JPanel(new GridBagLayout());
        JLabel lbldetalhes = new JLabel("Duplo Click nas linhas da tabela para abrir e editar os detalhes da Hiearquia");
        GridBagConstraints  posicaoPesquisa = new GridBagConstraints();
        posicaoPesquisa.anchor=GridBagConstraints.LINE_START;
        posicaoPesquisa.gridx = 1;
        posicaoPesquisa.gridy = 0;
        GridBagConstraints  posicaoLblDetalhes = new GridBagConstraints();
        posicaoLblDetalhes.anchor=GridBagConstraints.CENTER;
        posicaoLblDetalhes.gridx = 1;
        posicaoLblDetalhes.gridy = 1;


        painelcabecalho.add(painelPesquisa,posicaoPesquisa);
        painelcabecalho.add(lbldetalhes, posicaoLblDetalhes);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);
        TableColumn column = tabela.getColumnModel().getColumn(0);
        column.setMaxWidth(150);
        column.setPreferredWidth(150);
        column = tabela.getColumnModel().getColumn(1);
        column.setMaxWidth(200);
        column.setPreferredWidth(150);
        column = tabela.getColumnModel().getColumn(2);
        column.setMaxWidth(200);
        column.setPreferredWidth(150);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");


        //ações
        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });

        btnDepartamento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TabelaHierarquiasAdicionar adicionar = new TabelaHierarquiasAdicionar(null, utilizador);
                principal.add(adicionar.adicionarComponente(), "Adicionar");
                CardLayout cartas = (CardLayout)principal.getLayout();
                cartas.show(principal,"Adicionar");
            }
        });

        tabela.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable tabela =(JTable)e.getSource();
                Point point =e.getPoint();
                int row =tabela.rowAtPoint(point);
                if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                    String identificador =(String)tabela.getValueAt(tabela.getSelectedRow(),0);
                    abrirDetalhes(identificador);
                }
            }
        });

        cartaTabela.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });

    }


    /**
     * Metodo para abrir os detalhes para o identificador selecionado
     * @param identificador String - coódigo identificador da hierarquia
     */
    private void abrirDetalhes(String identificador){
        listagem.downloadFicheiro();
        HierarquiaProdutos hierarquiaProdutos = listagem.pesquisa(identificador);
        if(hierarquiaProdutos!=null){
            TabelaHierarquiasDetalhes detalhes = new TabelaHierarquiasDetalhes(hierarquiaProdutos,utilizador);
            principal.add(detalhes.adicionarComponente(), "Detalhe");
            CardLayout cartas = (CardLayout)principal.getLayout();
            cartas.show(principal,"Detalhe");
        }else{
            JOptionPane.showMessageDialog(principal, "Hierarquia não encontrada! Identificador: "+identificador,
                    "Erro Leitura", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        listagem=((UtilizadorLoja)utilizador).verHierarquias();
        cabecalho=listagem.listarCabecalhoTabela();
        dados=listagem.listarDadosTabela();
    }

    /**
     * Gerar o modelo de dados que gera a tabela
     * @return devolve DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText();
        int indiceParamentro=-1;
        boolean encontado=false;
        ArrayList<Object[]> resultadosPesquisa = new ArrayList<>();

        for(int i=0; i<cabecalho.length && !encontado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontado=true;
            }
        }
        //Só actualizo a tabela se o indice for encontrado.
        if(encontado){
            while (tableModel.getRowCount()>0){
                tableModel.removeRow(0);
            }


            for(int i=0; i<dados.length;i++){
                if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa.toLowerCase().strip())){
                    tableModel.addRow(dados[i]);
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }

}
