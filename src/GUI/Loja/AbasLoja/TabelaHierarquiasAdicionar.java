package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appLoja.Pessoas.Gerente;
import appLoja.Produtos.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TabelaHierarquiasAdicionar {
    private Pessoa utilizador;
    private HierarquiaProdutos parent;
    private VerificacaoDados verificacao;

    private JPanel principal;

    private JTextField txtNome;
    private JTextField txtDescricao;

    public TabelaHierarquiasAdicionar(HierarquiaProdutos hierarquiaProdutos, Pessoa utilizador){
        verificacao=VerificacaoDados.getInstancia();
        this.parent =hierarquiaProdutos;
        this.utilizador=utilizador;

        //Cabecalho
        String tipoAdicionar;
        if(parent==null){
            tipoAdicionar="Departamento";
        }else if(parent.tipo()==1){
            tipoAdicionar="Categoria";
        }else if(parent.tipo() ==2){
            tipoAdicionar="Unidade Base";
        }else{
            tipoAdicionar="ERRO";
        }
        JLabel lblCabecalho = new JLabel("Adicionar da Hierarquia - "+tipoAdicionar);
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);


         //Dados Pai (é adicionado apenas quando o pai existe, por isso só é gerado quando é adicionado a caixa).


        //Dados nome
        JLabel lblNome = new JLabel("Nome:");
        txtNome = new JTextField(20);
        txtNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelNome = new JPanel();
        painelNome.add(lblNome);
        painelNome.add(txtNome);

        //Dados descricao
        JLabel lblDescricao = new JLabel("Descrição:");
        txtDescricao = new JTextField(20);
        txtDescricao.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelDescricao = new JPanel();
        painelDescricao.add(lblDescricao);
        painelDescricao.add(txtDescricao);




        //Caixa Botoes: AlterarPass + Retroceder + adicionar subHiearquia
        JButton btnAdicionar = new JButton("Adicionar");
        JButton btnRetro = new JButton("Retroceder");

        JPanel painelBotoes = new JPanel();

        painelBotoes.add(btnAdicionar);
        painelBotoes.add(btnRetro);



        //Caixa que inclui: Tudo
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);

        //Dados Gerais
        if(parent!=null){
            JLabel lblDadosGerais;
            lblDadosGerais = new JLabel("Pai: "+hierarquiaProdutos.getIdentificador()+" - "+hierarquiaProdutos.tipoString()+" - "+hierarquiaProdutos.getNome());
            lblDadosGerais.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            caixaCentral.add(lblDadosGerais);
        }

        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelNome);
        caixaCentral.add(painelDescricao);

        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());

        //Caixa appCliente.Principal
        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("LoginPage");
        principal.add(caixaCentral,posicao);


        //acções
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.first(janela);
            }
        });

        btnAdicionar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if( novaHierarquia()){
                    Container janela = principal.getParent();
                    CardLayout cartas = (CardLayout)janela.getLayout();
                    cartas.first(janela);
                }
            }
        });
    }

    /**
     * Adiciona uma nova hierarquia a lista, Devovle true se foi adicionado, devolve false caso não tenha sido possivel adicionar
     * @return true se adicionou com sucesso
     */
    private boolean novaHierarquia(){
        String nome = txtNome.getText();
        String descricao = txtDescricao.getText();
        //Validar Dados
        if(verificacao.campoPreenchido(nome,"Nome")&&verificacao.txtValido(nome) && verificacao.campoPreenchido(descricao,"Descrição")&&verificacao.txtValido(descricao)){
            ListaHierarquias listaHierarquias = new ListaHierarquias();
            listaHierarquias.downloadFicheiro();
            HierarquiaProdutos novahierarquiaProdutos;
            if(parent==null){
                novahierarquiaProdutos=new Departamento(nome,descricao);
            }else if(parent.tipo()==1){
                novahierarquiaProdutos=new Categoria(nome,descricao,parent);
            }else  if(parent.tipo()==2){
                novahierarquiaProdutos=new BaseUnidade(nome,descricao,parent);
            }else {
                JOptionPane.showMessageDialog(principal.getParent(), "A hierarquia 'pai' não pode ser a Unidade Base ou ocorreu um erro na leitura do ficheiro",
                        "Erro Dados", JOptionPane.WARNING_MESSAGE);
                return false;
            }
            if(!((Gerente)utilizador).novaHierarquia(novahierarquiaProdutos)){
                if(novahierarquiaProdutos.tipo()>1){
                    JOptionPane.showMessageDialog(principal, "Já existe uma "+novahierarquiaProdutos.tipoString()+" com o nome '"+nome +"' na  "+novahierarquiaProdutos.getParent().tipoString()+" '"+novahierarquiaProdutos.getParent().getNome()+"'.\n",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }else{
                    JOptionPane.showMessageDialog(principal, "Já existe um Departamento com o nome '"+nome +"'\n",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }
                return false;
            }

            JOptionPane.showMessageDialog(principal, "Hierarquia adicionada com sucesso.",
                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        return false;
    }

    public JPanel adicionarComponente(){
        return principal;
    }
}
