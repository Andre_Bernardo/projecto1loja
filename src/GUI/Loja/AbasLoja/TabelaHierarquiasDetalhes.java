package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appLoja.Pessoas.Gerente;
import appLoja.PrincipalLoja;
import appLoja.Produtos.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;


public class TabelaHierarquiasDetalhes {
    private Pessoa utilizador;
    private HierarquiaProdutos hierarquiaProdutos;
    private ListaHierarquias listagem;
    private VerificacaoDados verificacao;

    private JPanel principal;

    private JTextField txtNome;
    private JTextField txtDescricao;

    public TabelaHierarquiasDetalhes(HierarquiaProdutos hierarquiaProdutos, Gerente utilizador){
        verificacao =VerificacaoDados.getInstancia();
        ListaProdutos listaProdutos=PrincipalLoja.getListaProdutos();
        listaProdutos.downloadFicheiro();
        this.hierarquiaProdutos=hierarquiaProdutos;
        this.listagem=utilizador.verHierarquias();
        this.utilizador=utilizador;


        //Cabecalho
        JLabel lblCabecalho = new JLabel("Detalhe da Hierarquia");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados Gerais
        JLabel lblDadosGerais = new JLabel(hierarquiaProdutos.getIdentificador()+" - "+hierarquiaProdutos.tipoString());
        lblDadosGerais.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados nome
        JLabel lblNome = new JLabel("Nome:");
        txtNome = new JTextField(20);
        txtNome.setText(hierarquiaProdutos.getNome());
        txtNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelNome = new JPanel();
        painelNome.add(lblNome);
        painelNome.add(txtNome);

        //Dados descricao
        JLabel lblDescricao = new JLabel("Descrição:");
        txtDescricao = new JTextField(20);
        txtDescricao.setText(hierarquiaProdutos.getDescricao());
        txtDescricao.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelDescricao = new JPanel();
        painelDescricao.add(lblDescricao);
        painelDescricao.add(txtDescricao);


        //Caixa Botoes: AlterarPass + Retroceder + adicionar subHiearquia
        JButton btnGuardar = new JButton("Guardar");
        JButton btnEliminar = new JButton("Eliminar");
        JButton btnRetro = new JButton("Retroceder");
        JButton btnAdicionar = new JButton();


        if(hierarquiaProdutos.tipo()==1){
            btnAdicionar.setText("Adicionar Categoria");
            btnAdicionar.setToolTipText("Adiciona uma Categoria a este Detpartamento");
        }
        if(hierarquiaProdutos.tipo()==2){
            btnAdicionar.setText("Adicionar Unidade");
            btnAdicionar.setToolTipText("Adiciona uma Unidade Base a esta Categoria");
        }

        JPanel painelBotoes = new JPanel();

        if(hierarquiaProdutos.tipo()==3){
            painelBotoes.add(btnGuardar);
            painelBotoes.add(btnEliminar);
            painelBotoes.add(btnRetro);
        }else{
            painelBotoes.add(btnGuardar);
            painelBotoes.add(btnEliminar);
            painelBotoes.add(btnAdicionar);
            painelBotoes.add(btnRetro);
        }




        //Caixa que inclui: Nome, Data Nascimento, Morada, Telefone, Email, Pass
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(lblDadosGerais);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelNome);
        caixaCentral.add(painelDescricao);




        //Tabela com os filhos da hierarquia
        if(hierarquiaProdutos.tipo()==1){
            caixaCentral.add(Box.createVerticalStrut(20));
            Departamento departamento=(Departamento)hierarquiaProdutos;
            JLabel lblFilhos = new JLabel();
            lblFilhos.setAlignmentX(JComponent.CENTER_ALIGNMENT);

            if(departamento.getLista().size()==0){
                lblFilhos.setText("A hierarquia não tem filhos");
                caixaCentral.add(lblFilhos);
            }else{
                lblFilhos.setText("Filhos da hierarquia");
                ListaHierarquias listaFilhos = departamento.getListaFilhos();
                JTable tabela=new JTable(gerarTabela(listaFilhos.listarDadosTabela(),listaFilhos.listarCabecalhoTabela()));
                tabela.setPreferredScrollableViewportSize(new Dimension(1100,80));
                tabela.scrollRectToVisible(new Rectangle(1100,80));
                tabela.setVisible(true);
                tabela.setAutoCreateRowSorter(true);
                JScrollPane scrollPane=new JScrollPane(tabela);
                caixaCentral.add(lblFilhos);
                caixaCentral.add(scrollPane);

                tabela.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        JTable tabela =(JTable)e.getSource();
                        Point point =e.getPoint();
                        int row =tabela.rowAtPoint(point);
                        if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                            String identificador =(String)tabela.getValueAt(tabela.getSelectedRow(),0);
                            abrirDetalhes(identificador);
                        }
                    }
                });
            }
        }else if(hierarquiaProdutos.tipo()==2){
            caixaCentral.add(Box.createVerticalStrut(20));
            Categoria categoria=(Categoria)hierarquiaProdutos;
            JLabel lblFilhos = new JLabel();
            lblFilhos.setAlignmentX(JComponent.CENTER_ALIGNMENT);

            if(categoria.getLista().size()==0){
                 lblFilhos.setText("A hierarquia não tem filhos");
                caixaCentral.add(lblFilhos);
            }else{
                lblFilhos.setText("Filhos da hierarquia");
                ListaHierarquias listaFilhos = categoria.getListaFilhos();
                JTable tabela=new JTable(gerarTabela(listaFilhos.listarDadosTabela(),listaFilhos.listarCabecalhoTabela()));
                tabela.setPreferredScrollableViewportSize(new Dimension(1100,80));
                tabela.scrollRectToVisible(new Rectangle(1100,80));
                tabela.setVisible(true);
                tabela.setAutoCreateRowSorter(true);
                JScrollPane scrollPane=new JScrollPane(tabela);
                caixaCentral.add(lblFilhos);
                caixaCentral.add(scrollPane);

                tabela.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        JTable tabela =(JTable)e.getSource();
                        Point point =e.getPoint();
                        int row =tabela.rowAtPoint(point);
                        if(e.getClickCount()==2 && tabela.getSelectedRow() != -1 && row !=-1){
                            String identificador =(String)tabela.getValueAt(tabela.getSelectedRow(),0);
                            abrirDetalhes(identificador);
                        }
                    }
                });
            }
        }else{
            caixaCentral.add(Box.createVerticalStrut(20));
            BaseUnidade baseUnidade=(BaseUnidade) hierarquiaProdutos;
            ArrayList<Produto> lista =listaProdutos.pertenceBaseUnidade(baseUnidade.getId());
            JLabel lblFilhos = new JLabel();
            lblFilhos.setAlignmentX(JComponent.CENTER_ALIGNMENT);

            if(lista.size()==0){
                lblFilhos.setText("A hierarquia não tem produtos");
                caixaCentral.add(lblFilhos);
            }else{
                lblFilhos.setText("Produtos da hierarquia");
                ListaProdutos listaFilhos = new ListaProdutos(lista);
                JTable tabela=new JTable(gerarTabela(listaFilhos.listarDadosTabela(),listaFilhos.listarCabecalhoTabela()));
                tabela.setPreferredScrollableViewportSize(new Dimension(1100,80));
                tabela.scrollRectToVisible(new Rectangle(1100,80));
                tabela.setVisible(true);
                tabela.setAutoCreateRowSorter(true);
                JScrollPane scrollPane=new JScrollPane(tabela);
                caixaCentral.add(lblFilhos);
                caixaCentral.add(scrollPane);
            }
        }
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());


        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("Detalhes de Hierarquias");
        principal.add(caixaCentral,posicao);



        //acções
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retroceder();
            }
        });

        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actualizarDados();

            }
        });



        btnEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(principal, "Tem a certeza que quer eliminar?",
                        "Confirmação", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (input == 0) {
                    if (((Gerente) utilizador).eliminiarHierarquia(hierarquiaProdutos)) {
                        JOptionPane.showMessageDialog(principal, "Hiearquia Eliminado com sucesso.",
                                "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        retroceder();
                    } else {
                        JOptionPane.showMessageDialog(principal, "Impossível eliminar o Hiearquia.\n" +
                                        "Hierarquia tem descendentes.",
                                "Erro de Sistema", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });



        if(hierarquiaProdutos.tipo()!=3){
            btnAdicionar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TabelaHierarquiasAdicionar adicionar = new TabelaHierarquiasAdicionar(hierarquiaProdutos, utilizador);
                    Container janela = principal.getParent();
                    janela.add(adicionar.adicionarComponente(), "Adicionar");
                    CardLayout cartas = (CardLayout)janela.getLayout();
                    cartas.show(janela,"Adicionar");
                }
            });
        }
    }

    /**
     * Metodo para abrir os detalhes para o identificador selecionado
     * @param identificador String - coódigo identificador da hierarquia
     */
    private void abrirDetalhes(String identificador){
        HierarquiaProdutos hierarquiaProdutos = listagem.pesquisa(identificador);
        if(hierarquiaProdutos!=null){
            TabelaHierarquiasDetalhes detalhes = new TabelaHierarquiasDetalhes(hierarquiaProdutos,(Gerente)utilizador);

            principal.getParent().add(detalhes.adicionarComponente(), "Detalhe");
            CardLayout cartas = (CardLayout)principal.getParent().getLayout();
            cartas.show(principal.getParent(),"Detalhe");
        }else{
            JOptionPane.showMessageDialog(principal, "Hierarquia não encontrada! Identificador: "+identificador,
                    "Erro Leitura", JOptionPane.WARNING_MESSAGE);
        }
    }


    /**
     * Actualizar e os dados da hierarquia apresentada
     */
    private void actualizarDados(){
        String nome = txtNome.getText();
        String descricao = txtDescricao.getText();
        //Validar Dados
        if(verificacao.campoPreenchido(nome,"Nome")&& verificacao.txtValido(nome) && verificacao.txtValido(descricao)){
            if(!((Gerente)utilizador).editarHierarquia(nome,descricao, hierarquiaProdutos)){
                if(hierarquiaProdutos.tipo()>1){
                    JOptionPane.showMessageDialog(principal, "Já existe uma "+hierarquiaProdutos.tipoString()+" com o nome '"+nome +"' na "+hierarquiaProdutos.getParent().tipoString()+" '"+hierarquiaProdutos.getParent().getNome()+"'.\n",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                    Container janela = principal.getParent();
                    CardLayout cartas = (CardLayout)janela.getLayout();
                }else{
                    JOptionPane.showMessageDialog(principal, "Já existe um Departamento com o nome '"+nome +"'.\n",
                            "Erro Dados", JOptionPane.WARNING_MESSAGE);
                }
            }else {
                JOptionPane.showMessageDialog(principal, "Hierarquia editada com sucesso.",
                        "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                retroceder();
            }

        }
    }

    private void  retroceder(){
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.first(janela);
    }
    private DefaultTableModel gerarTabela(Object[][] dados, String[] cabecalho) {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;
    }

    public JPanel adicionarComponente(){
        return principal;
    }
}
