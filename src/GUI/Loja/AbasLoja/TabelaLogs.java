package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import appLoja.Logs.ListaLogs;
import appLoja.Pessoas.Gerente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

public class TabelaLogs {
    private JPanel principal;
    private DefaultTableModel tableModel;
    private JTextField txtPesquisa;
    private JComboBox cmbCampo;

    private Gerente utilizador;
    private VerificacaoDados verificacao;
    private ListaLogs listagem;
    private String[] cabecalho;
    private Object[][] dados;

    public TabelaLogs(Gerente utilizador){
        verificacao=VerificacaoDados.getInstancia();
        this.utilizador=utilizador;
        carregarInfo();

        JLabel lblPesquisa= new JLabel("Campo");
        cmbCampo=new JComboBox(cabecalho);
        txtPesquisa=new JTextField(10);
        JButton btnPesquisa=new JButton("Pesquisar");
        JPanel painelPesquisa= new JPanel();
        painelPesquisa.add(lblPesquisa);
        painelPesquisa.add(cmbCampo);
        painelPesquisa.add(txtPesquisa);
        painelPesquisa.add(btnPesquisa);

        JPanel painelcabecalho = new JPanel(new GridBagLayout());
        GridBagConstraints  posicaoPesquisa = new GridBagConstraints();
        posicaoPesquisa.anchor=GridBagConstraints.LINE_START;
        posicaoPesquisa.gridx = 1;
        posicaoPesquisa.gridy = 0;

        painelcabecalho.add(painelPesquisa,posicaoPesquisa);

        tableModel= gerarTabela();
        JTable tabela=new JTable(tableModel);
        TableColumn column = tabela.getColumnModel().getColumn(3);
        column.setMinWidth(800);



        tabela.setPreferredScrollableViewportSize(new Dimension(325,80));
        tabela.scrollRectToVisible(new Rectangle(325,80));
        tabela.setVisible(true);
        tabela.setAutoCreateRowSorter(true);

        JScrollPane scrollPane=new JScrollPane(tabela);

        JPanel cartaTabela =new JPanel(new BorderLayout());
        cartaTabela.add(painelcabecalho, BorderLayout.NORTH);
        cartaTabela.add(scrollPane,BorderLayout.CENTER);

        principal = new JPanel(new CardLayout());
        principal.add(cartaTabela, "Tabela");

        //ações

        principal.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                pesquisaTabela();
            }
        });

        btnPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });


        txtPesquisa.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pesquisaTabela();
            }
        });
    }


    /**
     * Carregar a informação dos dados da tabela
     */
    private void carregarInfo(){
        listagem=utilizador.verLogs();
        cabecalho=listagem.listarCabecalhoTabela();
        dados=listagem.listarDadosTabela();
    }


    /**
     * Gerar o modelo de dados que gera a tabela
     * @return DefaultTableModel
     */
    private DefaultTableModel gerarTabela() {
        DefaultTableModel auxiliar = new DefaultTableModel(dados,cabecalho){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            @Override
            public Class getColumnClass(int column) {
                return getValueAt(0, column).getClass();
            }
        };
        return auxiliar;

    }

    /**
     * Actualiza os dados da DefaultTableModel de acordo com na pesquisa do utilizador
     */
    private void pesquisaTabela() {
        carregarInfo();
        String parametro=cmbCampo.getSelectedItem().toString();
        String pesquisa = txtPesquisa.getText().toLowerCase().strip();
        int indiceParamentro=-1;
        boolean encontado=false;

        for(int i=0; i<cabecalho.length && !encontado; i++){
            if(cabecalho[i].contains(parametro)){
                indiceParamentro=i;
                encontado=true;
            }
        }

        //Só actualizo a tabela se o indice for encontrado.
        if(encontado){

            //Condição para dar Reset a pesquisa
            if(pesquisa.equals("")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    tableModel.addRow(dados[i]);
                }
            //Condição pesquisa em String
            }else if(dados[0][indiceParamentro] instanceof String){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if(((String)dados[i][indiceParamentro]).toLowerCase().contains(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            //Condição pesquisa em Double
            }else if(dados[0][indiceParamentro] instanceof Double && verificacao.verificarNumeroDecimal(pesquisa, "Pesquisa campo de Número Decimal")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Double)dados[i][indiceParamentro]==Double.parseDouble(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            //Condição pesquisa em Inteiro
            }else if(dados[0][indiceParamentro] instanceof Integer && verificacao.verificarNumeroInteiro(pesquisa, "Pesquisa campo de Número Inteiro")){
                while (tableModel.getRowCount()>0){
                    tableModel.removeRow(0);
                }
                for(int i=0; i<dados.length;i++){
                    if((Integer)dados[i][indiceParamentro]==Integer.parseInt(pesquisa)){
                        tableModel.addRow(dados[i]);
                    }
                }
            }
        }
    }

    /**
     * Devolve o painel Principal (que é a tabela com a caixa de pesquisa)
     * @return devolve JPanel principal
     */
    public JPanel adicionarComponente(){
        return principal;
    }


}
