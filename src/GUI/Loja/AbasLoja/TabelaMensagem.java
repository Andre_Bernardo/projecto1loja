package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appLoja.Pessoas.Funcionario;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.Cliente;
import appLoja.PrincipalLoja;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TabelaMensagem {
    private Pessoa utilizador;
    private VerificacaoDados verificacao;

    private JPanel principal;

    private JTextField txtAssunto;
    private JTextField txtMensagem;
    private Cliente cliente;


    public TabelaMensagem(Cliente cliente) {
        verificacao=VerificacaoDados.getInstancia();
        this.utilizador = PrincipalLoja.getUtilizador();
        this.cliente=cliente;
        principal = new JPanel();

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Envio de Mensagens");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Assunto da Mensagem
        JLabel lblAssunto = new JLabel("Assunto:");
        txtAssunto = new JTextField();
        txtAssunto.setColumns(40);
        JPanel painelAssunto = new JPanel();
        painelAssunto.add(lblAssunto);
        painelAssunto.add(txtAssunto);

        //Texto da Mensagem
        JLabel lblMensagem = new JLabel("Texto:");
        txtMensagem = new JTextField();
        txtMensagem.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        txtMensagem.setColumns(40);
        txtMensagem.setText("Escreva aqui a sua mensagem.");
        JPanel painelMensagem = new JPanel();
        painelMensagem.add(lblMensagem);
        painelMensagem.add(txtMensagem);

        //Painel Botões Enviar + Retroceder
        JButton btnEnviar = new JButton("Enviar");
        JButton btnRetro = new JButton("Retroceder");
        JPanel painelBotoes = new JPanel(new GridLayout(1,5));
        painelBotoes.add(new JPanel());
        painelBotoes.add(btnEnviar);
        painelBotoes.add(new JPanel());
        painelBotoes.add(btnRetro);
        painelBotoes.add(new JPanel());

        //Caixa que inclui: Assunto, Texto e Botoes
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);

        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelAssunto);
        caixaCentral.add(painelMensagem);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());

        //Caixa appCliente.Principal
        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.setName("Envio de Mensagens");
        principal.add(caixaCentral,posicao);


        //Ações
        //Resultado após carregar no botão retroceder!
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.first(janela);
            }
        });

        //Resultado após carregar no botão Enviar!
        btnEnviar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(utilizador.tipo()==1){
                    if(novaMensagemCliente(cliente)) {
                    JOptionPane.showMessageDialog(principal, "Mensagem Enviada.",
                            "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                else{
                    if(novaMensagem()) {
                        JOptionPane.showMessageDialog(principal, "Mensagem Enviada.",
                                "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                Container janela = principal.getParent();
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.first(janela);

            }
        });
    }

    /**
     * Método que envia a mensagem para todos cliente!
     */
    private boolean novaMensagem() {
        String texto_assunto = txtAssunto.getText().strip();
        String texto_mensagem = txtMensagem.getText().strip();

        if(verificacao.campoPreenchido(texto_assunto,"Assunto")&&verificacao.campoPreenchido(texto_mensagem,"Mensagem") &&
                verificacao.txtValido(texto_assunto)&&verificacao.txtValido(texto_mensagem)){

            ((Gerente) utilizador).envioMensagem(texto_assunto, texto_mensagem);

            return true;
        }

        return false;


    }

    private boolean novaMensagemCliente(Cliente cliente) {
        String texto_assunto = txtAssunto.getText().strip();
        String texto_mensagem = txtMensagem.getText().strip();

        if(verificacao.campoPreenchido(texto_assunto,"Assunto")&&verificacao.campoPreenchido(texto_mensagem,"Mensagem") &&
                verificacao.txtValido(texto_assunto)&&verificacao.txtValido(texto_mensagem)){

            ((Funcionario) utilizador).envioMensagem(texto_assunto, texto_mensagem, cliente);

            return true;
        }

        return false;


    }

    /*Método que envia a mensagem para cliente especifico!
    private void novaMensagem() {*/

    /**
     * Metodo que devolve a janela principal construida
     *
     * @return JPanel com a janela principal
     */

    public JPanel adicionarComponente() {
        return principal;
    }
}
