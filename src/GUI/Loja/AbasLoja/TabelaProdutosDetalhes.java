package GUI.Loja.AbasLoja;

import Auxiliar.VerificacaoDados;
import Comuns.Pessoa;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.UtilizadorLoja;
import appLoja.Produtos.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class TabelaProdutosDetalhes {
    private UtilizadorLoja utilizador;
    private Produto produto;
    private ListaHierarquias listaHierarquias;
    private VerificacaoDados verificacao;
    private ListaProdutos listaProdutos;


    private JPanel principal;

    private JTextField txtNome;
    private JTextField txtDescricao;
    private JTextField txtMarca;
    private JTextField txtStock;
    private JTextField txtUnidade;
    private JComboBox<Departamento> cmbDepartamento;
    private JComboBox<Categoria> cmbCategoria;
    private JComboBox<BaseUnidade> cmbBaseUnidade;
    private JTextField txtPreco;
    private JTextField txtIVA;
    private JTextField txtDesconto;
    private JLabel lblPrecoFinal;
    private JLabel lblpreco;

    public TabelaProdutosDetalhes(Produto produto, UtilizadorLoja utilizador){

        verificacao=VerificacaoDados.getInstancia();
        this.produto=produto;
        this.utilizador=utilizador;
        this.listaProdutos=utilizador.verProdutos();
        this.listaHierarquias=utilizador.verHierarquias();

        //Cabecalho
        JLabel lblCabecalho = new JLabel("Detalhe do Produto");
        lblCabecalho.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados Gerais
        JLabel lblDadosGerais = new JLabel();
        if(produto==null){
            lblDadosGerais.setText("Novo Produto:");
        }else {
            lblDadosGerais.setText(produto.getIdentificador()+" - "+produto.getNome());
        }
        lblDadosGerais.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        //Dados nome
        JLabel lblNome = new JLabel("Nome:");
        txtNome = new JTextField(20);
        txtNome.setText(produto==null?"":produto.getNome());
        txtNome.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelNome = new JPanel();
        painelNome.add(lblNome);
        painelNome.add(txtNome);

        //Dados descricao
        JLabel lblDescricao = new JLabel("Descrição:");
        txtDescricao = new JTextField(40);
        txtDescricao.setText(produto==null?"":produto.getDescricao());
        txtDescricao.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelDescricao = new JPanel();
        painelDescricao.add(lblDescricao);
        painelDescricao.add(txtDescricao);

        //Dados marca
        JLabel lblMarca = new JLabel("Marca:");
        txtMarca = new JTextField(20);
        txtMarca.setText(produto==null?"":produto.getMarca());
        txtMarca.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelMarca = new JPanel();
        painelMarca.add(lblMarca);
        painelMarca.add(txtMarca);

        //Dados stock
        JLabel lblStock = new JLabel("Stock:");
        txtStock = new JTextField(5);
        if(produto!=null){
            if(produto.tipo()==1){
                txtStock.setText(Integer.toString(produto.getStock()));
            }else{
                txtStock.setText(Double.toString(((Granel)produto).getStockGranel()));
            }
        }else{
            txtStock.setText("0");
        }

        //Dados da unidade
        JLabel lblUnidade =  new JLabel("Unidade de Venda:");
        txtUnidade = new JTextField(produto==null?"uni":produto.getUnidade(),2);

        txtStock.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
        JPanel painelStock = new JPanel();
        painelStock.add(lblMarca);
        painelStock.add(txtMarca);
        painelStock.add(lblStock);
        painelStock.add(txtStock);
        painelStock.add(lblUnidade);
        painelStock.add(txtUnidade);



        //Dados Departamento
        JLabel lblDepartamento = new JLabel("Departamento:");



        //Dados Categoria
        JLabel lblCategoria = new JLabel("Categoria:");


        //Dados BaseUnidade
        JLabel lblBaseUnidade = new JLabel("Base Unidade:");


        JPanel painelDepartamento= new JPanel();


        //Apenas o Gerente vê as cmbBox para alterar o produto.
        if(((Pessoa)utilizador).tipo()==2){
            cmbDepartamento = new JComboBox(listaHierarquias.departamentos());
            cmbDepartamento.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

            cmbCategoria = new JComboBox();
            cmbCategoria.setAlignmentX(JComponent.RIGHT_ALIGNMENT);


            cmbBaseUnidade = new JComboBox();
            cmbBaseUnidade.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

            if(produto!=null){
                cmbDepartamento.setSelectedIndex(indiceDepartamento());
                actualizarDepartamento();
                cmbCategoria.setSelectedItem(produto.getCategoria());
                actualizarCategoria();
                cmbBaseUnidade.setSelectedItem(produto.getUnidadeBase());
            }else{
                actualizarDepartamento();
                actualizarCategoria();

            }

            painelDepartamento.add(lblDepartamento);
            painelDepartamento.add(cmbDepartamento);
            painelDepartamento.add(lblCategoria);
            painelDepartamento.add(cmbCategoria);
            painelDepartamento.add(lblBaseUnidade);
            painelDepartamento.add(cmbBaseUnidade);


            cmbDepartamento.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    actualizarCategoria();
                    actualizarDepartamento();
                }
            });
            cmbCategoria.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    actualizarCategoria();
                    actualizarDepartamento();
                }
            });
        }else {
            JTextField txtDepartamento = new JTextField();
            txtDepartamento.setText(produto.getDepartamento().toString());
            txtDepartamento.setEditable(false);

            JTextField txtCategoria= new JTextField();
            txtCategoria.setText(produto.getCategoria().toString());
            txtCategoria.setEditable(false);

            JTextField txtBaseUnidade = new JTextField();
            txtBaseUnidade.setText(produto.getUnidadeBase().toString());
            txtBaseUnidade.setEditable(false);

            painelDepartamento.add(lblDepartamento);
            painelDepartamento.add(txtDepartamento);
            painelDepartamento.add(lblCategoria);
            painelDepartamento.add(txtCategoria);
            painelDepartamento.add(lblBaseUnidade);
            painelDepartamento.add(txtBaseUnidade);
        }

        //Dados preco base
        lblpreco = new JLabel(String.format("Preço €/%s:",produto==null?"uni":produto.getUnidade()));
        txtPreco = new JTextField(5);
        txtPreco.setText(Double.toString(produto==null?0:produto.getPreco()));
        txtPreco.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        //Dados iva
        JLabel lblIVA = new JLabel("IVA %:");
        txtIVA = new JTextField(3);
        txtIVA.setText(String.format("%.0f",produto==null?23:produto.getIva()*100));
        txtIVA.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        //Dados iva
        JLabel lblDesconto = new JLabel("Desconto %:");
        txtDesconto = new JTextField(3);
        txtDesconto.setText(String.format("%.0f",produto==null?0:produto.getDesconto()*100));
        txtDesconto.setAlignmentX(JComponent.RIGHT_ALIGNMENT);

        JPanel painelPreco = new JPanel();
        painelPreco.add(lblpreco);
        painelPreco.add(txtPreco);
        painelPreco.add(lblIVA);
        painelPreco.add(txtIVA);
        painelPreco.add(lblDesconto);
        painelPreco.add(txtDesconto);

        //Preço Final
        lblPrecoFinal = new JLabel(String.format("Preço final do produto por %s: %.2f€",
                produto==null?"uni":produto.getUnidade(), produto==null?0:produto.precoUnitarioVendaComDesconto()));
        lblPrecoFinal.setAlignmentX(JComponent.CENTER_ALIGNMENT);


        //Caixa Botoes: AlterarPass + Retroceder + adicionar subHiearquia
        JPanel painelBotoes = new JPanel();
        if(((Pessoa)utilizador).tipo()==2){
            JButton btnGuardar = new JButton();
            if(produto==null){
                btnGuardar.setText("Adicionar");
                painelBotoes.add(btnGuardar);
            }else{
                btnGuardar.setText("Guardar");
                painelBotoes.add(btnGuardar);


                JButton btnEliminar = new JButton("Eliminar");
                painelBotoes.add(btnEliminar);
                btnEliminar.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int input = JOptionPane.showConfirmDialog(principal, "Tem a certeza que quer eliminar?",
                                "Confirmação",   JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                        if(input==0){
                            if(((Gerente)utilizador).eliminarProduto(produto)){
                                JOptionPane.showMessageDialog(principal, "Produto Eliminado com sucesso.",
                                        "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                                retroceder();
                            }else{
                                JOptionPane.showMessageDialog(principal, "Impossível eliminar o produto.\n" +
                                                "Produto faz parte de uma encomenda.",
                                        "Erro de Sistema", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                });
            }

            btnGuardar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    if(produto==null){
                        if(inserirDados()){
                            JOptionPane.showMessageDialog(principal, "Produto Inserido com sucesso.",
                                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                            retroceder();
                        }
                    }else{
                        if(actualizarDados()){
                            JOptionPane.showMessageDialog(principal, "Produto editado com sucesso.",
                                    "Operação realizada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                            retroceder();
                        }
                    }
                }
            });
        }
        JButton btnRetro = new JButton("Retroceder");
        painelBotoes.add(btnRetro);

        //Caixa que cnetral (que inclui todos os elementos)
        Box caixaCentral = new Box(BoxLayout.Y_AXIS);


        caixaCentral.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        caixaCentral.setAlignmentY(JComponent.CENTER_ALIGNMENT);

        caixaCentral.add(Box.createVerticalGlue());
        caixaCentral.add(lblCabecalho);
        caixaCentral.add(lblDadosGerais);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelNome);
        caixaCentral.add(painelDescricao);
        caixaCentral.add(painelStock);
        caixaCentral.add(painelDepartamento);
        caixaCentral.add(painelPreco);
        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(lblPrecoFinal);

        caixaCentral.add(Box.createVerticalStrut(20));
        caixaCentral.add(painelBotoes);
        caixaCentral.add(Box.createVerticalGlue());

        principal=new JPanel(new GridBagLayout());
        GridBagConstraints posicao = new GridBagConstraints();
        posicao.anchor=GridBagConstraints.CENTER;
        principal.add(caixaCentral,posicao);

        //Desativar Campos
        if(((Pessoa)utilizador).tipo()!=2){
            txtNome.setEditable(false);
            txtDescricao.setEditable(false);
            txtMarca.setEditable(false);
            txtStock.setEditable(false);
            txtUnidade.setEditable(false);
            txtPreco.setEditable(false);
            txtIVA.setEditable(false);
            txtDesconto.setEditable(false);
        }


        //acções
        btnRetro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retroceder();
            }
        });



        txtPreco.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                actualizarUnidades();
            }
        });
        txtIVA.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                actualizarUnidades();
            }
        });
        txtDesconto.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                actualizarUnidades();
            }
        });
        txtUnidade.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                actualizarUnidades();
            }


        });
    }

    private void retroceder(){
        Container janela = principal.getParent();
        CardLayout cartas = (CardLayout)janela.getLayout();
        cartas.first(janela);
    }

    private double actualizarPrecoFinal(){
        String preco = txtPreco.getText();
        String iva= txtIVA.getText();
        String desconto = txtDesconto.getText();

        if(verificacao.verificarNumeroDecimal(preco,"Preço")
            && verificacao.verificarNumeroInteiro(iva,"IVA [%]")
            && verificacao.verificarNumeroInteiro(desconto,"Desconto [%]")
        ){
            double valorPreco=Double.parseDouble(preco);
            double valorIva=Double.parseDouble(iva)/100.0;
            double valorDesconto=Double.parseDouble(desconto)/100.0;

            double precoFinal= valorPreco*(1+valorIva-valorDesconto);
            precoFinal=Math.round(precoFinal*100.0);
            precoFinal=precoFinal/100.0;
            return precoFinal;
        }
        return 0.0;
    }
    private void actualizarUnidades() {
        String unidades =txtUnidade.getText();
        lblpreco.setText("Preço €/"+unidades+":");
        lblPrecoFinal.setText(String.format("Preço final do produto por %s: %.2f€",
                unidades,actualizarPrecoFinal()));
    }

    /**
     * Devolve true ou falso dependendo se os inputs são válidos
     * @return true se inputs forem válidos
     */
    private boolean validarDados(){
        String nome = txtNome.getText();
        String descricao = txtDescricao.getText();
        String marca = txtMarca.getText();
        String stock= txtStock.getText();
        String unidade= txtUnidade.getText();
        Departamento departamento = (Departamento) cmbDepartamento.getSelectedItem();
        Categoria categoria = (Categoria) cmbCategoria.getSelectedItem();
        BaseUnidade baseUnidade = (BaseUnidade) cmbBaseUnidade.getSelectedItem();
        String preco = txtPreco.getText();
        String iva= txtIVA.getText();
        String desconto = txtDesconto.getText();

        return (verificacao.campoPreenchido(nome,"Nome")&&verificacao.txtValido(nome)
                && verificacao.txtValido(descricao)
                && verificacao.txtValido(marca)
                && ((((produto==null && unidade.equals("uni") ) || produto.tipo()==1) && verificacao.verificarNumeroInteiro(stock,"Stock"))|| ( verificacao.verificarNumeroDecimal(stock,"Stock - Produto a Granel")))
                && verificacao.txtValido(unidade)
                && verificacao.verificarHierarquia(departamento,"Departamento")
                && verificacao.verificarHierarquia(categoria,"Categoria")
                && verificacao.verificarHierarquia(baseUnidade,"Base de Unidade")
                && verificacao.campoPreenchido(preco,"Preço") && verificacao.verificarNumeroDecimal(preco,"Preço")
                && verificacao.campoPreenchido(iva,"IVA [%]") && verificacao.verificarNumeroInteiro(iva,"IVA [%]")
                && verificacao.campoPreenchido(desconto,"Desconto [%]") && verificacao.verificarNumeroInteiro(desconto,"Desconto [%]")
        );
    }

    /**
     * Metodo para actualizar dados (alterar produto)
     * @return true - se foi alteração realizada com sucesso
     */
    private boolean actualizarDados(){
        //Validar Dados
        if(validarDados()){
            String nome = txtNome.getText();
            String descricao = txtDescricao.getText();
            String marca = txtMarca.getText();
            String stock= txtStock.getText();
            String unidade= txtUnidade.getText();
            BaseUnidade baseUnidade = (BaseUnidade) cmbBaseUnidade.getSelectedItem();
            String preco = txtPreco.getText();
            String iva= txtIVA.getText();
            String desconto = txtDesconto.getText();

            if(!((Gerente)utilizador).editarProduto(nome,descricao,marca,stock,unidade,baseUnidade,preco,iva,desconto,
                    produto)){
                JOptionPane.showMessageDialog(principal, "Já existe um produto com o nome '"+nome +"' e marca '"+marca+"'.\n",
                        "Erro Dados", JOptionPane.WARNING_MESSAGE);
                return false;
            };

            return true;
        }
        return false;
    }

    /**
     * Metodo para inserir dados (novo produto)
     * @return true - se foi inserido com sucesso
     */
    private boolean inserirDados(){
        if(validarDados()){
            String nome = txtNome.getText();
            String descricao = txtDescricao.getText();
            String marca = txtMarca.getText();
            String stock= txtStock.getText();
            String unidade= txtUnidade.getText();
            BaseUnidade baseUnidade = (BaseUnidade) cmbBaseUnidade.getSelectedItem();
            String preco = txtPreco.getText();
            String iva= txtIVA.getText();
            String desconto = txtDesconto.getText();

            ListaProdutos listaProdutos = new ListaProdutos();
            listaProdutos.downloadFicheiro();
            Produto novoProduto;
            if(unidade.equals("uni")){
                novoProduto=new Produto(nome,descricao,baseUnidade,marca,unidade,Integer.parseInt(stock),
                        Double.parseDouble(preco),Double.parseDouble(iva)/100.0,Double.parseDouble(desconto)/100.0);
            }else{
                novoProduto=new Granel(nome,descricao,baseUnidade,marca,unidade,Double.parseDouble(stock),
                        Double.parseDouble(preco),Double.parseDouble(iva)/100.0,Double.parseDouble(desconto)/100.0);
            }
            if(!(((Gerente)utilizador).novoProduto(novoProduto))){
                JOptionPane.showMessageDialog(principal, "Já existe um produto com o nome '"+nome +"' e marca '"+marca+"'.\n",
                        "Erro Dados", JOptionPane.WARNING_MESSAGE);
                return false;
            }
            return true;
        }
        return false;
    }

    public JPanel adicionarComponente(){
        return principal;
    }


    private int indiceDepartamento(){
        Departamento[] departamentos = listaHierarquias.departamentos();
        for(int i=0; i<departamentos.length;i++){
            if(departamentos[i].getId()==produto.getDepartamento().getId()){
                return i;
            }
        }
        return 0;
    }

    private void actualizarCategoria(){
        Categoria categoriaSelecionada = ((Categoria)(cmbCategoria.getSelectedItem()));
        Object[] filhos =categoriaSelecionada==null?null:categoriaSelecionada.getComboBoxFilhos();
        DefaultComboBoxModel novosDados;
        if(filhos==null){
            novosDados = new DefaultComboBoxModel();
        }else {
            novosDados = new DefaultComboBoxModel(filhos);
        }
        cmbBaseUnidade.setModel( novosDados );
    }

    private void actualizarDepartamento(){
        Departamento departamentoSelecionado = ((Departamento)(cmbDepartamento.getSelectedItem()));
        Object[] filhos =departamentoSelecionado==null?null:departamentoSelecionado.getComboBoxFilhos();

        DefaultComboBoxModel novosDados;
        if(filhos==null){
            novosDados = new DefaultComboBoxModel();
        }else {
            novosDados = new DefaultComboBoxModel(filhos);
        }
        cmbCategoria.setModel( novosDados );
    }
}
