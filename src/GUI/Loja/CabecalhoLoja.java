package GUI.Loja;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CabecalhoLoja {
    private JPanel principal;


    public CabecalhoLoja(){

        JButton btnLogout = new JButton("Logout");
        btnLogout.setToolTipText("Fechar Sessão do Utilizador");
        JButton btnPerfil = new JButton("Perfil");
        btnPerfil.setToolTipText("Visualizar e alterar dados do Utilizador");

        JLabel lblIcon = new JLabel(new ImageIcon("resources/IconCompridoV2.png"));
        lblIcon.setBackground(Color.BLUE);


        principal=new JPanel();
        principal.add(lblIcon);
        principal.add(btnPerfil);
        principal.add(btnLogout);
        Color corFundo= new Color(173,185,202);
        principal.setBackground(corFundo);


        //acçoes
        btnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container janela = principal.getParent().getParent(); //Pai do Cabeçalho é a PaginaPrincipal
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.show(janela,"Login");
            }
        });
        btnPerfil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PerfilLoja paginaPerfil = new PerfilLoja();
                Container janela = principal.getParent().getParent(); //Pai do Cabeçalho é a PaginaPrincipal
                janela.add(paginaPerfil.adicionarJanela(),"Perfil");
                CardLayout cartas = (CardLayout)janela.getLayout();
                cartas.show(janela,"Perfil");
            }
        });

    }

    /**
     * Metodo que devolve o cabecalho construido
     * @return JPanel com o cabecalho
     */
    public JPanel adicionarComponente(){
        return principal;
    }
}
