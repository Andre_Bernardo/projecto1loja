package GUI.Loja;

import Comuns.Pessoa;
import GUI.Loja.AbasLoja.*;
import appLoja.Pessoas.Funcionario;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.UtilizadorLoja;
import appLoja.PrincipalLoja;

import javax.swing.*;
import java.awt.*;

public class PaginaPrincipalLoja {

    private JPanel principal;
    private TabelaFuncionarios tabelaFuncionarios;
    private TabelaClientes tabelaClientes;
    private TabelaHierarquias tabelaHierarquias;
    private TabelaProdutos tabelaProdutos;
    private TabelaEstatistica tabelaEstatistica;
    private TabelaEncomendas tabelaEncomendas;
    private TabelaLogs tabelaLogs;

    public PaginaPrincipalLoja(){
        Pessoa utilizador=PrincipalLoja.getUtilizador();

        JPanel cabecalho= new CabecalhoLoja().adicionarComponente();

        JTabbedPane abas= new JTabbedPane();

        tabelaClientes =new TabelaClientes((UtilizadorLoja)utilizador);
        tabelaProdutos=new TabelaProdutos((UtilizadorLoja)utilizador);
               abas.add("Produtos",tabelaProdutos.adicionarComponente());
        abas.add("Clientes",tabelaClientes.adicionarComponente());


        //Estas abas só estçao disponíveis ao funcionário
        if(utilizador.tipo()==1){
            tabelaEncomendas=new TabelaEncomendas((Funcionario) utilizador);
            abas.add("Encomendas",tabelaEncomendas.adicionarComponente());
        }

        //Estas abas só estão disoníveis ao Gestor
        if(utilizador.tipo()==2){
            tabelaFuncionarios =new TabelaFuncionarios((Gerente)utilizador);
            abas.add("Funcionários", tabelaFuncionarios.adicionarComponente());
            tabelaHierarquias=new TabelaHierarquias((Gerente)utilizador);
            abas.add("Hierarquias Produtos",tabelaHierarquias.adicionarComponente());
            tabelaEstatistica=new TabelaEstatistica((Gerente)utilizador);
            abas.add("Estatistica",tabelaEstatistica.adicionarComponente());
            tabelaLogs=new TabelaLogs((Gerente)utilizador);
            abas.add("Logs",tabelaLogs.adicionarComponente());
        }

        principal=new JPanel(new BorderLayout());
        principal.setName("PaginaPrincipal");
        principal.add(cabecalho, BorderLayout.NORTH);
        principal.add(abas, BorderLayout.CENTER);

        Color corFundo= new Color(173,185,202);
        principal.setBackground(corFundo);
    }

    /**
     * Metodo que devolve a janela principal construida
     * @return JPanel com a janela principal
     */
    public JPanel adicionarJanela(){
        return principal;
    }
}
