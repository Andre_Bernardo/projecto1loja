package appCliente.Encomenda;

import Comuns.Pagamento;
import appCliente.Pessoas.Cliente;
import appCliente.Produtos.Produto;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Class Encomenda
 * Tem a informação em relação a uma encondeda, permite calcular preços, ver itens, estado e pagamento
 */
public class Encomenda {

    private int id;
    private static int idgerado;
    private Cliente cliente;
    private ArrayList<VendaProduto> vendas;
    private Pagamento pagamento;
    private boolean levantarLoja;
    private EstadoEncomenda estado;


    /**
     * Construtor para gerar um carrinho referente a um cliente
     * @param cliente Cliente
     */
    Encomenda(Cliente cliente){
        idgerado++;
        this.id=idgerado;
        this.cliente=cliente;
        this.vendas=new ArrayList<>();
        this.estado=new EstadoEncomenda();
    };

    /**
     * Construtor para nova encomenda
     * @param cliente Cliente
     * @param pagamento Pagamento
     * @param estado Estado
     */
    public Encomenda(Cliente cliente, Pagamento pagamento, EstadoEncomenda estado){
        idgerado++;
        this.id=idgerado;
        this.cliente=cliente;
        this.vendas=new ArrayList<>();
        this.pagamento=pagamento;
        this.estado=estado;
    }

    /**
     * Construtor para carregar dados já existentes
     * @param id int
     * @param cliente Cliente
     * @param pagamento Pagamento
     * @param levantarLoja boolean
     * @param estado EstadoEncomenda
     */
    public Encomenda(int id, Cliente cliente, Pagamento pagamento, boolean levantarLoja, EstadoEncomenda estado){
        this.id=id;
        if (id>idgerado){
            idgerado=id;
        }
        this.cliente=cliente;
        this.pagamento=pagamento;
        this.levantarLoja=levantarLoja;
        this.estado=estado;
        this.vendas=new ArrayList<>();
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[8];
        linha[0]=getId();
        linha[1]=String.format("%tF %tR",getEstado().getDataEncomenda(),getEstado().getDataEncomenda());
        linha[2]=getEstado().estadoString();
        linha[3]=String.format("%tF %tR",getEstado().ultimaActualizacao(),getEstado().ultimaActualizacao());
        linha[4]=numeroItens();
        linha[5]=valorTotal();
        linha[6]=pagamento.tipoString();
        linha[7]=levantarLoja?"Levantar Loja":"Enviar";
        return linha;
    }

    /**
     * Metodo para adicionar as vendas a uma encomenda no arranque
     * @param vendas ArrayList vendas
     */
    public void setVendas(ArrayList<VendaProduto> vendas) {
        this.vendas = vendas;
    }

    public int getId() {
        return id;
    }

    public static int getIdgerado() {
        return idgerado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public ArrayList<VendaProduto> getVendas() {
        return vendas;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public int pagamentoID(){
        if(pagamento==null){
            return 0;
        }
        return pagamento.getId();
    }

    public boolean isLevantarLoja() {
        return levantarLoja;
    }

    public EstadoEncomenda getEstado() {
        return estado;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public void setLevantarLoja(boolean levantarLoja) {
        this.levantarLoja = levantarLoja;
    }

    /**
     * Procura na encomenda se já exite um determinado produto e devovle a respetiva venda.
     * Devolve null se não encontrar.
     * @param produto Produto
     * @return VendaProduto ou null.
     */
    public VendaProduto procurarProdutoEncomenda(Produto produto){
        for(VendaProduto venda: vendas){
            if(venda.getProduto().getId()==produto.getId()){
                return venda;
            }
        }
        return null;
    }

    /**
     * Adicionar venda a encomenda, 1º verifica se venda existe e adiciona ou actualiza a venda
     * @param produto Produto
     * @param quantidade int
     * @param listaVendas ListaVendas
     */
    public void adicionarVenda(Produto produto, double quantidade, ListaVendas listaVendas){
        listaVendas.downloadFicheiro();
        VendaProduto venda= procurarProdutoEncomenda(produto);
        if(venda==null){
            venda=new VendaProduto(this.getId(),quantidade,produto,produto.getPreco(),produto.getIva(),produto.getDesconto(),false);
            vendas.add(venda);
            listaVendas.adicionarElemento(venda);
        }else {
            venda.setQuantidade(quantidade);
            listaVendas.pesquisa(this.getId(),produto.getId()).setQuantidade(quantidade);

        }
        listaVendas.uploadFicheiro();
    }

    /**
     * Remover venda de um determinado produto
     * @param listaVendas listaVendas
     * @param produto produto para remover
     */
    public void removerVenda(Produto produto, ListaVendas listaVendas){
        VendaProduto venda= procurarProdutoEncomenda(produto);
        listaVendas.downloadFicheiro();
        if(venda!=null){
            vendas.remove(venda);
            listaVendas.getLista().remove(listaVendas.pesquisa(this.getId(),produto.getId()));
        }
        listaVendas.uploadFicheiro();
    }

    /**
     * Devolve o número de itens na encomenda (Se produto = Granel conta apenas por 1 independentemente da quantidade)
     * @return int
     */
    public int numeroItens(){
        int nItens=0;
        for(VendaProduto venda:vendas){
            if(venda.getProduto().tipo()==1){
                nItens+=venda.getQuantidade();
            }else{
                nItens++;
            }
        }
        return nItens;
    }

    /**
     * Custo total da encomenda, calculado com base nos valores guardados em histórico
     * @return double
     */
    public double valorTotal(){
        double valor=0;
        for(VendaProduto venda:vendas){
            valor+=venda.precoVenda();
        }
        return valor;
    }

    /**
     * Custo total da encomenda, calculado com base nos valores associados ao produto
     * @return double
     */
    public double valorTotalCarrinho(){
        double valor=0;
        for(VendaProduto venda:vendas){
            valor+=venda.precoCarrinho();
        }
        return valor;
    }

    /**
     * Actualizar os valores da venda, para guardar no histórico
     * @param listaVendas ListaVendas
     */
    public void actualizarHistoricoCarrinho(ListaVendas listaVendas){
        listaVendas.downloadFicheiro();
        VendaProduto vendaLista;
        for(VendaProduto venda: vendas){
            vendaLista=listaVendas.pesquisa(this.getId(),venda.getProduto().getId());
            vendaLista.setPreco(venda.getProduto().getPreco());
            vendaLista.setDesconto(venda.getProduto().getDesconto());
            vendaLista.setIva(venda.getProduto().getIva());
        }
        listaVendas.uploadFicheiro();
    }


    /**
     * Ordenar por estado
     */
    public static Comparator<Encomenda> ordenarEstado = new Comparator<Encomenda>() {
        @Override
        public int compare(Encomenda o1, Encomenda o2) {
            if(o1.getEstado().getTipoEstado()==o2.getEstado().getTipoEstado()){
                return o1.getEstado().getDataEncomenda().compareTo(o2.getEstado().getDataEncomenda());
            }
            return Integer.compare(o1.getEstado().getTipoEstado(),o2.getEstado().getTipoEstado());
        }
    };
}
