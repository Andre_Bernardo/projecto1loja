package appCliente.Encomenda;

import java.util.GregorianCalendar;

/**
 * Class Estado Encomenda
 * Esta class além de deter a informação relativa ao estado guarda as datas em que os vários estágios ocorreram
 */
public class EstadoEncomenda {

    private static String[] estadoPossiveis = {"Carrinho",
            "Encomenda Realizada",
            "Em Preparação",
            "Enviada", "Pronto a levantar",
            "Não Recebida","Recebida"};
    private static int idGerado;
    private int id;
    private GregorianCalendar dataEncomenda;
    private GregorianCalendar dataPreparacao;
    private GregorianCalendar dataExpedicao;
    private GregorianCalendar dataChegada;
    private int estado;

    /**
     * Construtor Vazio
     */
    EstadoEncomenda(){
        idGerado++;
        estado=0;
        id=idGerado;
        dataEncomenda=new GregorianCalendar();
        dataPreparacao=new GregorianCalendar();
        dataExpedicao=new GregorianCalendar();
        dataChegada=new GregorianCalendar();
    }

    /**
     * Construtor para carregamento de dados
     * @param id (id)
     * @param dataEncomenda GregorianCalendar
     * @param dataPreparacao GregorianCalendar
     * @param dataExpedicao GregorianCalendar
     * @param dataChegada GregorianCalendar
     */
    EstadoEncomenda(int id, GregorianCalendar dataEncomenda, GregorianCalendar dataPreparacao, GregorianCalendar dataExpedicao, GregorianCalendar dataChegada, int estado){
        if(id>idGerado){
            idGerado=id;
        }
        this.id=id;
        this.dataEncomenda=dataEncomenda;
        this.dataPreparacao=dataPreparacao;
        this.dataExpedicao=dataExpedicao;
        this.dataChegada=dataChegada;
        this.estado=estado;
    }

    public Object[] linhaTabela(){
        Object[] linha = new Object[6];
        linha[0]=getId();
        linha[1]=getDataEncomenda();
        linha[3]=getDataPreparacao();
        linha[4]=getDataChegada();
        linha[5]= getTipoEstado();
        return linha;
    }

    public GregorianCalendar getDataEncomenda() {
        return dataEncomenda;
    }

    public GregorianCalendar getDataPreparacao() {
        return dataPreparacao;
    }

    public GregorianCalendar getDataExpedicao() {
        return dataExpedicao;
    }

    public GregorianCalendar getDataChegada() {
        return dataChegada;
    }

    public int getTipoEstado() {
        return estado;
    }

    public void setDataEncomenda(GregorianCalendar dataEncomenda) {
        this.dataEncomenda = dataEncomenda;
    }

    public void setDataPreparacao(GregorianCalendar dataPreparacao) {
        this.dataPreparacao = dataPreparacao;
    }

    public void setDataExpedicao(GregorianCalendar dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }

    public void setDataChegada(GregorianCalendar dataChegada) {
        this.dataChegada = dataChegada;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public String estadoString(){
        return estadoPossiveis[estado];
    }

    /**
     * Devolve a ultima actualização do estado
     * @return GregorianCalendar
     */
    public GregorianCalendar ultimaActualizacao(){
        if(estado==1){
            return dataEncomenda;
        }
        if(estado==2){
            return dataPreparacao;
        }
        if(estado==3){
            return dataExpedicao;
        }
        if(estado==4){
            return dataExpedicao;
        }
        if(estado==5){
            return dataChegada;
        }
        if(estado==5){
            return dataChegada;
        }

        return new GregorianCalendar();
    }

}
