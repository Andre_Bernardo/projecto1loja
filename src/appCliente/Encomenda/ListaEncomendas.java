package appCliente.Encomenda;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import Comuns.ListaPagamentos;
import Comuns.Pagamento;
import appCliente.Pessoas.Cliente;
import appCliente.Pessoas.ListaPessoas;
import appCliente.PrincipalCliente;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

/**
 * Listar as encomendas
 */
public class ListaEncomendas implements GestaoFicheiros {

    private ArrayList<Encomenda> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaEncomendas(){
        lista=new ArrayList<>();
        localFicheiro=caminhoEncomendas;
    }

    /**
     * Construtor para procurar lista de encomendas especificas
     * @param lista Arraylist Encomendas
     */
    public ListaEncomendas(ArrayList<Encomenda> lista){
        this.lista=lista;
        localFicheiro=caminhoEncomendas;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaEncomendas(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<Encomenda> getLista() {
        return lista;
    }


    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            lista.clear();

            int id=0;
            int id_Cliente;
            Cliente cliente;
            int id_Pagamento;
            Pagamento pagamento;
            boolean levantarLoja;
            int id_Estado;
            EstadoEncomenda estado;

            Encomenda encomenda;

            ListaPessoas pessoas = PrincipalCliente.getListaClientes();
            pessoas.downloadFicheiro();

            ListaPagamentos pagamentos = PrincipalCliente.getListaPagamentos();
            pagamentos.downloadFicheiro();

            ListaEstados estadosEncomenda = PrincipalCliente.getListaEstados();
            estadosEncomenda.downloadFicheiro();

            ListaVendas listaVendas = new PrincipalCliente().getListaVendas();
            listaVendas.downloadFicheiro();

            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id= parseInt(linhaPartida[0]);
                id_Cliente = parseInt(linhaPartida[1]);
                cliente = (Cliente) pessoas.pesquisa(id_Cliente);
                id_Pagamento = parseInt(linhaPartida[2]);
                pagamento = (Pagamento) pagamentos.pesquisa(id_Pagamento);
                if(linhaPartida[3].equals("true")){levantarLoja=true;}
                else{levantarLoja=false;};
                id_Estado = parseInt(linhaPartida[4]);
                estado = (EstadoEncomenda) estadosEncomenda.pesquisa(id_Estado);

                encomenda=new Encomenda(id,cliente,pagamento,levantarLoja,estado);
                lista.add(encomenda);

                //Adicionar Vendas
                encomenda.setVendas(listaVendas.procurarVendas(encomenda.getId()));


                linha=conteudo.readLine();
            }
            ordenarEncomendas();
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }


    @Override
    public void uploadFicheiro() {
        ordenarEncomendas();
        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(Encomenda encomenda: lista){
                linha=encomenda.getId()+divisorColunas+
                        encomenda.getCliente().getId()+divisorColunas+
                        encomenda.pagamentoID()+divisorColunas+
                        encomenda.isLevantarLoja()+divisorColunas+
                        encomenda.getEstado().getId();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Encomenda) object);
    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[8];
        //Cabeçalho
        infoTabela[0]="Nº Encomenda";
        infoTabela[1]="Data Encomenda";
        infoTabela[2]="Estado";
        infoTabela[3]="Ultima Actualização";
        infoTabela[4]="Nº Itens";
        infoTabela[5]="Valor Total";
        infoTabela[6]="Pagamento";
        infoTabela[7]="Modo Entrega";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][3];

        //Informação
        int i=0;
        for(Encomenda encomenda:lista){
            infoTabela[i]=encomenda.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public Object pesquisa(int id) {
        for(Encomenda encomenda:lista){
            if(encomenda.getId()==id){
                return encomenda;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        return lista.size();
    }

    /**
     * Devolve as encomendas realizadas (estado mairo que 0) pelo cliente
     * @param cliente cliente
     * @return ListaEncomendas
     */
    public ListaEncomendas encomendasCliente(Cliente cliente){
        ArrayList<Encomenda> encomendas = new ArrayList<>();
        for(Encomenda encomenda: lista){
            if(encomenda.getCliente().getId()==cliente.getId() && encomenda.getEstado().getTipoEstado()!=0){
                encomendas.add(encomenda);
            }
        }
        return new ListaEncomendas(encomendas);
    }

    /**
     * Carrinho do cliente (estado igual a 0) do cliente
     * @param cliente Cliente
     * @return Encomenda
     */
    public Encomenda carrinhoCliente(Cliente cliente){
        for(Encomenda encomenda: lista){
            if(encomenda.getCliente().getId()==cliente.getId() && encomenda.getEstado().getTipoEstado()==0){
                return encomenda;
            }
        }
        Encomenda carrinho =  new Encomenda(cliente);
        lista.add(carrinho);
        uploadFicheiro();
        ListaEstados estados = new ListaEstados();
        estados.downloadFicheiro();
        estados.adicionarElemento(carrinho.getEstado());
        estados.uploadFicheiro();

        return carrinho;
    }

    /**
     * ORdenar dados
     */
    private void ordenarEncomendas(){
        lista.sort(Encomenda.ordenarEstado);
    }
}
