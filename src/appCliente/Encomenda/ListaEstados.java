package appCliente.Encomenda;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

/**
 * Listar as estados das encomendas
 */
public class ListaEstados implements GestaoFicheiros {

    private ArrayList<EstadoEncomenda> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaEstados(){
        lista=new ArrayList<>();
        localFicheiro=caminhoEstados;
    }

    /**
     * Construtor para procurar lista de estados especificos
     * @param lista ArrrayList EstadosEncomenda
     */
    public ListaEstados(ArrayList<EstadoEncomenda> lista){
        this.lista=lista;
        localFicheiro=caminhoEstados;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaEstados(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<EstadoEncomenda> getLista() {
        return lista;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id=0;

            String[] dataDiaEncomenda;
            String[] dataHoraEncomenda;
            GregorianCalendar dataEncomenda;

            String[] dataDiaPreparacao;
            String[] dataHoraPreparacao;
            GregorianCalendar dataPreparacao;

            String[] dataDiaExpedicao;
            String[] dataHoraExpedicao;
            GregorianCalendar dataExpedicao;

            String[] dataDiaChegada;
            String[] dataHoraChegada;
            GregorianCalendar dataChegada;
            int estado;


            //char tipo;//PARA QUÊ? Necessário para distinguir
            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id= parseInt(linhaPartida[0]);

                dataDiaEncomenda=linhaPartida[1].split("-");
                dataHoraEncomenda=linhaPartida[2].split(":");
                //Ano, Mes, Dia, Hora, Minuto
                dataEncomenda =new GregorianCalendar(parseInt(dataDiaEncomenda[0]),parseInt(dataDiaEncomenda[1])-1,parseInt(dataDiaEncomenda[2]),
                        parseInt(dataHoraEncomenda[0]), parseInt(dataHoraEncomenda[1]));

                dataDiaPreparacao=linhaPartida[3].split("-");
                dataHoraPreparacao=linhaPartida[4].split(":");
                //Ano, Mes, Dia, Hora, Minuto
                dataPreparacao =new GregorianCalendar(parseInt(dataDiaPreparacao[0]),parseInt(dataDiaPreparacao[1])-1,parseInt(dataDiaPreparacao[2]),
                        parseInt(dataHoraPreparacao[0]), parseInt(dataHoraPreparacao[1]));

                dataDiaExpedicao=linhaPartida[5].split("-");
                dataHoraExpedicao=linhaPartida[6].split(":");
                //Ano, Mes, Dia, Hora, Minuto
                dataExpedicao =new GregorianCalendar(parseInt(dataDiaExpedicao[0]),parseInt(dataDiaExpedicao[1])-1,parseInt(dataDiaExpedicao[2]),
                        parseInt(dataHoraExpedicao[0]), parseInt(dataHoraExpedicao[1]));

                dataDiaChegada=linhaPartida[7].split("-");
                dataHoraChegada=linhaPartida[8].split(":");
                //Ano, Mes, Dia, Hora, Minuto
                dataChegada =new GregorianCalendar(parseInt(dataDiaChegada[0]),parseInt(dataDiaChegada[1])-1,parseInt(dataDiaChegada[2]),
                        parseInt(dataHoraChegada[0]), parseInt(dataHoraChegada[1]));
                estado= parseInt(linhaPartida[9]);


                lista.add(new EstadoEncomenda(id,dataEncomenda,dataPreparacao,dataExpedicao,dataChegada,estado));

                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(EstadoEncomenda estadoEncomenda: lista){
                linha=estadoEncomenda.getId()+divisorColunas+
                        String.format("%tF",estadoEncomenda.getDataEncomenda())+divisorColunas+
                        String.format("%tR",estadoEncomenda.getDataEncomenda())+divisorColunas+
                        String.format("%tF",estadoEncomenda.getDataPreparacao())+divisorColunas+
                        String.format("%tR",estadoEncomenda.getDataPreparacao())+divisorColunas+
                        String.format("%tF",estadoEncomenda.getDataExpedicao())+divisorColunas+
                        String.format("%tR",estadoEncomenda.getDataExpedicao())+divisorColunas+
                        String.format("%tF",estadoEncomenda.getDataChegada())+divisorColunas+
                        String.format("%tR",estadoEncomenda.getDataChegada())+divisorColunas+
                        estadoEncomenda.getTipoEstado();


                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((EstadoEncomenda) object);
    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[6];
        //Cabeçalho
        infoTabela[0]="iD";
        infoTabela[1]="Data Encomenda";
        infoTabela[2]="Data Preparacao";
        infoTabela[3]="Data Expedicao";
        infoTabela[4]="Data Chegada";
        infoTabela[5]="Estado";
        return infoTabela;
    }

    /**
     * NECESSARIO?
     * Devolve array de Objects que são os dados da tabela.
     * @return Devolve um array de objectos com os dados da tabela
     */
    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][3];

        //Informação
        int i=0;
        for(EstadoEncomenda estadoEncomenda:lista){
            infoTabela[i]=estadoEncomenda.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public Object pesquisa(int id) {
        for(EstadoEncomenda estadoEncomenda:lista){
            if(estadoEncomenda.getId()==id){
                return estadoEncomenda;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        return lista.size();
    }


}
