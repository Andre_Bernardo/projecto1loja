package appCliente.Encomenda;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import appCliente.PrincipalCliente;
import appCliente.Produtos.ListaProdutos;
import appCliente.Produtos.Produto;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 * Listar as vendas
 */
public class ListaVendas implements GestaoFicheiros {

    private ArrayList<VendaProduto> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaVendas(){
        lista=new ArrayList<>();
        localFicheiro=caminhoVendas;
    }

    /**
     * Construtor para procurar lista de vendas especificas
     * @param lista ArrayList vendas
     */
    public ListaVendas(ArrayList<VendaProduto> lista){
        this.lista=lista;
        localFicheiro="auxiliarVendas.txt";
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaVendas(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<VendaProduto> getLista() {
        return lista;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id_Encomenda;
            double quantidade;
            int id_Produto;
            Produto produto;
            double preco;
            double iva;
            double desconto;
            boolean empacotado;

            lista.clear();

            ListaProdutos produtos = PrincipalCliente.getListaProdutos();
            produtos.downloadFicheiro();

            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id_Encomenda = parseInt(linhaPartida[0]);
                quantidade = parseDouble(linhaPartida[1]);
                id_Produto = parseInt(linhaPartida[2]);
                produto = (Produto)produtos.pesquisa(id_Produto);
                preco = parseDouble (linhaPartida[3]);
                iva = parseDouble (linhaPartida[4])/100.0;
                desconto = parseDouble (linhaPartida[5])/100.0;

                if(linhaPartida[6].equals("true")){
                    empacotado=true;}
                else{
                    empacotado=false;
                };

                lista.add(new VendaProduto(id_Encomenda,quantidade,produto,preco,iva,desconto,empacotado));

                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;

            for(VendaProduto venda: lista){
                linha=venda.getIdEncomenda()+divisorColunas+
                        venda.getQuantidade()+divisorColunas+
                        venda.getProduto().getId()+divisorColunas+
                        venda.getPreco()+divisorColunas+
                        (venda.getIva()*100.0)+divisorColunas+
                        (venda.getDesconto()*100.0)+divisorColunas+
                        venda.isEmpacotado();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((VendaProduto) object);
    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[11];
        //Cabeçalho
        infoTabela[0]="Identificado Produto";
        infoTabela[1]="Departamento";
        infoTabela[2]="Categoria";
        infoTabela[3]="Base Unidade";
        infoTabela[4]="Nome";
        infoTabela[5]="Marca";
        infoTabela[6]="Quantidade";
        infoTabela[7]="Preço/Uni";
        infoTabela[8]="IVA[%]";
        infoTabela[9]="Desconto[%]";
        infoTabela[10]="Preço Final";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][11];

        //Informação
        int i=0;
        for(VendaProduto venda:lista){
            infoTabela[i]=venda.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    /**
     * Uma entrada nesta lista é definida pelo ID da encomenda e ID do produto, por isto este metodo não faz sentido.
     * @param id int
     * @return Null
     */
    @Override
    public Object pesquisa(int id) {
        return null;
    }

    /**
     * Realizar uma pesquisa na listagem a través do ID encomenda e ID produto
     * @param idEncomenda int
     * @param idProduto int
     * @return VendaProduto
     */
    public VendaProduto pesquisa(int idEncomenda, int idProduto) {
        for(VendaProduto venda: lista){
            if(venda.getIdEncomenda()==idEncomenda && venda.getProduto().getId()== idProduto){
                return venda;
            }
        }
        return null;
    }

    /**
     * Conta quantos itens estão guardados no ficheiro .txt
     * @return quantidade total
     */
    @Override
    public int getTotal(){
        downloadFicheiro();
        return lista.size();
    }

    /**
     * Identificar a vendas associadas a uma encomenda
     * @param id int (id da encomenda em procura)
     * @return ArrayList (VendaProdutos)
     */
    public ArrayList<VendaProduto> procurarVendas(int id) {
        ArrayList<VendaProduto> vendas = new ArrayList<>();
        for(VendaProduto venda : lista){
            if(venda.getIdEncomenda()==id){
                vendas.add(venda);
            }
        }
        return vendas;
    }
}
