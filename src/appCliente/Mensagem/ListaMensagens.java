package appCliente.Mensagem;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

/**
 * Listar as Mensagens
 */
public class ListaMensagens implements GestaoFicheiros {

    private ArrayList<Mensagem> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaMensagens(){
        lista=new ArrayList<>();
        localFicheiro=caminhoMensagens;
    }

    /**
     * Construtor para procurar lista de mensagens especifica, util para abir apenas as mensagens para um determinado destinanário
     * @param lista arrayList mensagens
     */
    public ListaMensagens(ArrayList<Mensagem> lista){
        this.lista=lista;
        localFicheiro="auxiliarMensagens.txt";
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaMensagens(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<Mensagem> getLista() {
        return lista;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id=0;
            String[] dataAux;
            GregorianCalendar data;
            String assunto;
            String texto;


            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                /*if(linhaPartida[0]==""){

                }*/
                id= parseInt(linhaPartida[0]);
                dataAux=linhaPartida[2].split("-");
                data =new GregorianCalendar(parseInt(dataAux[0]), parseInt(dataAux[1])-1, parseInt(dataAux[2]));
                assunto=linhaPartida[3];
                texto=linhaPartida[4];

                lista.add(new Mensagem(id,assunto,texto,data));//CRIAR OUTRO CONSTRUTOR COM TODOS OS ELEMENTOS?

                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    /**
     * GUARDAR INFO
     * Descarregar informação da lista para o ficheiro
     */
    @Override
    public void uploadFicheiro() {
        System.out.println("Cliente não pode guardar ficheiro");
    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Mensagem)object);
    }


    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[3];
        //Cabeçalho
        infoTabela[0]="Data";
        infoTabela[1]="Assunto";
        infoTabela[2]="Mensagem";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][3];

        //Informação
        int i=0;
        for(Mensagem mensagem:lista){
            infoTabela[i]=mensagem.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    public Object pesquisa(int id) {
        for(Mensagem mensagem:lista){
            if(mensagem.getId()==id){
                return mensagem;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(Mensagem mensagem:lista){
            count++;
        }
        return count;
    }

}
