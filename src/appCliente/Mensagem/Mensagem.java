package appCliente.Mensagem;

import java.util.GregorianCalendar;


/**
 * Mensagem enviada para o cliente, contem uma data, assunto e texto
 */
public class Mensagem {

    private int id;
    private static int idgerado;
    private GregorianCalendar data;
    private String assunto;
    private String texto;


    /**
     * Construtor para carregamento dados já existentes
     * @param id int
     * @param assunto String assunto
     * @param texto String texto
     * @param data GregorianCalendar, data da mensagem
     */
    public Mensagem(int id,String assunto, String texto, GregorianCalendar data){
        this.id=id;
        if (id>idgerado){
            idgerado=id;
        }
        this.data=data;
        this.assunto=assunto;
        this.texto=texto;
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[3];
        linha[0]=String.format("%tF",data);
        linha[1]=getAssunto();
        linha[2]=getTexto();
        return linha;
    }

    public int getId() {
        return id;
    }

    public String getTexto() {
        return texto;
    }

    public String getAssunto() {
        return assunto;
    }

    public GregorianCalendar getData() {
        return data;
    }

}
