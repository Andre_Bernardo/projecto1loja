package appCliente.Pessoas;

import Comuns.*;
import appCliente.Encomenda.*;
import appCliente.Mensagem.ListaEnvioMensagens;
import appCliente.Mensagem.ListaMensagens;
import appCliente.PrincipalCliente;
import appCliente.Produtos.ListaHierarquias;
import appCliente.Produtos.ListaProdutos;
import appCliente.Produtos.Produto;

import java.util.GregorianCalendar;

/**
 * Cliente é a entidade utilizadora da aplicação, aqui encontram-se todas as ações que pode executar.
 */
public class Cliente extends Pessoa {
    /**
     * Construtor normal, criar um novo utilizador requer apenas nome, email, password e data nascimento (verificação de idade)
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param email String - email
     * @param password String - password
     */
    public Cliente(String priNome, String ultNome, GregorianCalendar dataNasc, String email, String password) {
        super(priNome, ultNome, dataNasc, email, password);
    }
    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int - id da pessoa
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     * @param password String - password
     */
    public Cliente(int id, String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email, String password) {
        super(id, priNome, ultNome, dataNasc, morada, telefone, email, password);
    }

    @Override
    public String tipoString() {
        return "Cliente";
    }

    @Override
    public int tipo() {
        return 3;
    }

    /**
     * Ver mensagens destinadas a um cliente.
     * @param envioMensagens ListaEnvioMensagens
     * @return ListaMensagens
     */
    public ListaMensagens verMensagens(ListaEnvioMensagens envioMensagens){
        envioMensagens.downloadFicheiro();
        return new ListaMensagens(envioMensagens.mensagensCliente(id));
    }

    /**
     * Procurar Carrinho do Cliente
     * @param listaEncomendas ListaEncomendas
     * @return Encomenda carrinho do cliente
     */
    public Encomenda procurarCarrinho(ListaEncomendas listaEncomendas){
        listaEncomendas.downloadFicheiro();
        return listaEncomendas.carrinhoCliente(this);
    }

    /**
     * Adicionar um item ao carrinho do cliente
     * @param listaEncomendas ListaEncomendas
     * @param produto Produto
     * @param quantidade Qunatidade
     */
    public void adicionarItemCarrinho(ListaEncomendas listaEncomendas, Produto produto, Double quantidade){
        Encomenda carrinho = procurarCarrinho(listaEncomendas);
        carrinho.adicionarVenda(produto,quantidade, PrincipalCliente.getListaVendas());
    }

    /**
     * Remover um item ao carrinho do cliente
     * @param listaEncomendas ListaEncomendas
     * @param produto Produto
     */
    public void removerItemCarrinho(ListaEncomendas listaEncomendas , Produto produto){

        Encomenda carrinho = procurarCarrinho(listaEncomendas);
        carrinho.removerVenda(produto, PrincipalCliente.getListaVendas());
    }

    /**
     * Ao finalizar uma encomenda é necessário actualizar a Encomenda, adicionar um pagamento a Encomenda, Actualizar os dados
     * para histórico da encomenda e actualizar o estado da encomenda
     * @param carrinho encomenda
     * @param levantarLoja boolean
     * @param tipoPagamento int
     * @param referenciaBancaria long
     */
    public void finalizarEncomenda(Encomenda carrinho,boolean levantarLoja, int tipoPagamento, long referenciaBancaria){
        ListaEncomendas listaEncomendas = PrincipalCliente.getListaEncomendas();
        listaEncomendas.downloadFicheiro();
        Encomenda encomenda =(Encomenda) listaEncomendas.pesquisa(carrinho.getId());

        Pagamento pagamento;
        if(tipoPagamento==1){
            pagamento=new Multibanco(encomenda.valorTotalCarrinho(),referenciaBancaria);
        }else if(tipoPagamento==2){
            pagamento=new CartaoCredito(encomenda.valorTotalCarrinho());
        }else{
            pagamento=new PayPall(encomenda.valorTotalCarrinho());
        }
        ListaPagamentos listaPagamentos = PrincipalCliente.getListaPagamentos();
        listaPagamentos.downloadFicheiro();
        listaPagamentos.adicionarElemento(pagamento);
        listaPagamentos.uploadFicheiro();

        encomenda.setLevantarLoja(levantarLoja);
        encomenda.setPagamento(pagamento);
        listaEncomendas.uploadFicheiro();

        carrinho.actualizarHistoricoCarrinho(PrincipalCliente.getListaVendas());

        ListaEstados listaEstado = PrincipalCliente.getListaEstados();
        listaEstado.downloadFicheiro();
        EstadoEncomenda estado = (EstadoEncomenda) listaEstado.pesquisa(carrinho.getEstado().getId());
        estado.setDataEncomenda(new GregorianCalendar());
        estado.setEstado(1);
        listaEstado.uploadFicheiro();

    }

    /**
     * Altera o estado da encomenda para recebida
     * @param encomenda Encomenda
     * @return true se for possivel actualizar, false se não encontrar estado
     */
    public boolean encomendaEntrege(Encomenda encomenda) {
        ListaEstados listaEstados = PrincipalCliente.getListaEstados();
        listaEstados.downloadFicheiro();
        EstadoEncomenda estadoActualizado= (EstadoEncomenda) listaEstados.pesquisa(encomenda.getEstado().getId());
        if(estadoActualizado!=null){
            estadoActualizado.setEstado(6);
            estadoActualizado.setDataChegada(new GregorianCalendar());
            listaEstados.uploadFicheiro();
            return true;
        }
        return false;
    }

    /**
     * Altera o estado da encomenda para recebida
     * @param encomenda Encomenda
     * @return true se for possivel actualizar, false se não encontrar estado OU data de chegada ainda não expirou
     */
    public boolean encomendaNaoEntrege(Encomenda encomenda) {
        ListaEstados listaEstados = PrincipalCliente.getListaEstados();
        listaEstados.downloadFicheiro();
        EstadoEncomenda estadoActualizado= (EstadoEncomenda) listaEstados.pesquisa(encomenda.getEstado().getId());
        if(estadoActualizado!=null && estadoActualizado.getDataChegada().before(new GregorianCalendar())){
            estadoActualizado.setEstado(5);
            estadoActualizado.setDataChegada(new GregorianCalendar());
            listaEstados.uploadFicheiro();
            return true;
        }
        return false;
    }

    /**
     * Devolve uma Lista de encomendas efecuadas pelo cliente
     * @return ListaEncomendas
     */
    public ListaEncomendas encomendasDoCliente(){
        ListaEncomendas listaEncomendas = PrincipalCliente.getListaEncomendas();
        listaEncomendas.downloadFicheiro();
        return listaEncomendas.encomendasCliente(this);
    }

    /**
     * Valor total gasto em todas as encomedas
     * @return double
     */
    public double gastouTotal (){
        double total=0;
        ListaEncomendas listaEncomendas = encomendasDoCliente();
        for(Encomenda encomenda: listaEncomendas.getLista()){
           for(VendaProduto venda: encomenda.getVendas()){
               total += venda.getQuantidade()*venda.precoVenda();
           }
        }
        return total;
    }

    /**
     * Valor total gasto num departamento específico
     * @param departamento int idDepartamento
     * @return double
     */
    public double gastouTotalDep(int departamento){
        double total=0;
        ListaEncomendas listaEncomendas = encomendasDoCliente();
        for(Encomenda encomenda: listaEncomendas.getLista()){
            for(VendaProduto venda: encomenda.getVendas()){
                if(venda.getProduto().getDepartamento().getId()==departamento){
                    total += venda.getQuantidade()*venda.precoVenda();
                }
            }
        }
        return total;
    }

    /**
     * Valor total gasto numa categoria específica
     * @param categoria int idCategoria
     * @return double
     */
    public double gastouTotalCat(int categoria){
        double total=0;
        ListaEncomendas listaEncomendas = encomendasDoCliente();
        for(Encomenda encomenda: listaEncomendas.getLista()){
            for(VendaProduto venda: encomenda.getVendas()){
                if(venda.getProduto().getCategoria().getId()==categoria){
                    total += venda.getQuantidade()*venda.precoVenda();
                }
            }
        }
        return total;
    }

    /**
     * Valor total gasto numa baseUnidade específica
     * @param baseUnidade int ID da baseUnidade
     * @return double
     */
    public double gastouTotalUni(int baseUnidade){
        double total=0;
        ListaEncomendas listaEncomendas = encomendasDoCliente();
        for(Encomenda encomenda: listaEncomendas.getLista()){
            for(VendaProduto venda: encomenda.getVendas()){
                if(venda.getProduto().getUnidadeBase().getId()==baseUnidade){
                    total += venda.getQuantidade()*venda.precoVenda();
                }
            }
        }
        return total;
    }

    /**
     * Devolve a Lista de produtos
     * @return ListaProdutos
     */
    public ListaProdutos verProdutos(){
        ListaProdutos lista = PrincipalCliente.getListaProdutos();
        lista.downloadFicheiro();
        return lista;
    }

    public ListaHierarquias verHierarquias(){
        ListaHierarquias lista = PrincipalCliente.getListaHierarquias();
        lista.downloadFicheiro();
        return lista;
    }
}
