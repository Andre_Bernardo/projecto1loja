package appCliente.Pessoas;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import Comuns.Pessoa;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

/**
 * Listar as Pessoas
 */
public class ListaPessoas implements GestaoFicheiros {


    protected ArrayList<Pessoa> lista;
    protected String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaPessoas(){
        lista=new ArrayList<>();
    }

    /**
     * Construtor para gerara a lista Pessoas
     * 1- Lista Funcionários
     * 2- Lista Clientes
     * @param tipo int
     */
    public ListaPessoas(int tipo){
        lista=new ArrayList<>();
        if(tipo==1){
            localFicheiro=caminhoFuncionarios;
        }else{
            localFicheiro=caminhoCliente;
        }
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (se difenrete do camnhinho da interface
     */
    public ListaPessoas(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    public ArrayList<Pessoa> getLista() {
        return lista;
    }

    /**
     * Verifica se o email é unico na lista de pessoa
     * @param email String - email a testar
     * @param id int id da pessoa a testar
     * @return true se o email for unico
     */
    public boolean emailUnico(String email, int id){
        for(Pessoa pessoa:lista){
            if(pessoa.getEmail().equals(email) && pessoa.getId()!=id){
                return false;
            }
        }
        return true;
    }

    /**
     * Pesquisa de pessoa pelo respetivo ID.
     * Devolve a pessoa que tem o ID igual ao argumento dado ou devolve NULL caso não encontre correspondencia.
     * @param id int
     * @return Devolve (Pessoa)Object ou NULL
     */
    @Override
    public Object pesquisa(int id) {
        for(Pessoa pessoa:lista){
            if(pessoa.getId()==id){
                return pessoa;
            }
        }
        return null;
    }

    /**
     *  Carregar informação do ficheiro para a lista//COLOCAR PARA O FICHEIRO TXT
     */
    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            lista.clear();

            int id;
            String priNome;
            String ultNome;
            String[] dataNascAux;
            GregorianCalendar dataNasc;
            String morada;
            String telefone;
            String email;
            String password;

            char tipo;
            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                tipo=linhaPartida[8].charAt(0);
                id= parseInt(linhaPartida[0]);
                priNome=linhaPartida[1];
                ultNome=linhaPartida[2];
                dataNascAux=linhaPartida[3].split("-");
                dataNasc=new GregorianCalendar(parseInt(dataNascAux[0]), parseInt(dataNascAux[1])-1, parseInt(dataNascAux[2]));
                morada=linhaPartida[4];
                telefone=linhaPartida[5];
                email=linhaPartida[6];
                password=linhaPartida[7];

                if(tipo=='3'){
                    lista.add(new Cliente(id, priNome, ultNome, dataNasc,morada,telefone,email,password));
                }else{
                    if(id> Comuns.Pessoa.getIdGerado()){
                        Comuns.Pessoa.setIdGerado(id);
                    }
                }


                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    /**
     * Descarrefar informação da lista para o ficheiro//CARREGAR DO FICHEIRO TXT
     */
    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(Pessoa pessoa: lista){
                linha=pessoa.getId()+divisorColunas+
                        pessoa.getPriNome()+divisorColunas+
                        pessoa.getUltNome()+divisorColunas+
                        String.format("%04d-%02d-%02d",pessoa.getAnoNasc(),pessoa.getMesNasc()+1,pessoa.getDiaNasc())+divisorColunas+
                        pessoa.getMorada()+divisorColunas+
                        pessoa.getTelefone()+divisorColunas+
                        pessoa.getEmail()+divisorColunas+
                        pessoa.getPassword()+divisorColunas+
                        pessoa.tipo();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }


    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }

    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[6];
        //Cabeçalho
        infoTabela[0]="Nome";
        infoTabela[1]="Aplido";
        infoTabela[2]="Data Nascimento";
        infoTabela[3]="Morada";
        infoTabela[4]="Telefone";
        infoTabela[5]="Email";
        return infoTabela;
    }

    /**
     * Devolve array de Objects que são os dados da tabela.
     *
     * @return Devolve um array de objectos com os dados da tabela
     */
    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][6];

        //Informação
        int i=0;
        for(Pessoa pessoa:lista){
            infoTabela[i]=pessoa.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Pessoa)object);
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    public Pessoa login(String email, String password){
        for(Pessoa pessoa: lista){
            if(pessoa.getEmail().toLowerCase().equals(email.toLowerCase()) && pessoa.getPassword().equals(password)){
                return pessoa;
            }
        }
        return null;
    }

    /**
     * Conta quantos itens estão guardados no ficheiro .txt
     * @return quantidade total
     */
    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(Pessoa pessoa:lista){
            count++;
        }
        return count;
    }
}
