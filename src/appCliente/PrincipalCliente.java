package appCliente;

import Auxiliar.GestaoFicheiros;
import Auxiliar.VerificacaoDados;
import Comuns.ListaPagamentos;
import Comuns.Pessoa;
import GUI.Cliente.LoginCliente;
import GUI.Cliente.NovoCliente;
import appCliente.Encomenda.ListaEncomendas;
import appCliente.Encomenda.ListaEstados;
import appCliente.Encomenda.ListaVendas;
import appCliente.Mensagem.ListaEnvioMensagens;
import appCliente.Mensagem.ListaMensagens;
import appCliente.Pessoas.ListaPessoas;
import appCliente.Produtos.ListaHierarquias;
import appCliente.Produtos.ListaProdutos;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;

public class PrincipalCliente {
    /**
     * Se não for assim após actualizar os dados é necessário REINICIAR o programa
     */
    private static Pessoa utilizador;
    private static ListaPessoas listaFuncionarios;
    private static ListaPessoas listaClientes;

    private static ListaHierarquias listaHierarquias;
    private static ListaProdutos listaProdutos;

    private static ListaEnvioMensagens listaEnvioMensagens;
    private static ListaMensagens listaMensagens;

    private static ListaEstados listaEstados;
    private static ListaEncomendas listaEncomendas;
    private static ListaVendas listaVendas;
    private static ListaPagamentos listaPagamentos;

    private static VerificacaoDados verificar;


    public static void main(String[] args) {
        listaFuncionarios=new ListaPessoas(1);
        boolean existeFuncionarios = listaFuncionarios.verificaFicheiro();
        listaClientes=new ListaPessoas(2);
        boolean existeClientes = listaClientes.verificaFicheiro();
        listaHierarquias=new ListaHierarquias();
        boolean existeHieraquias = listaHierarquias.verificaFicheiro();
        listaProdutos=new ListaProdutos();
        boolean existeProdutos=listaProdutos.verificaFicheiro();

        listaEstados=new ListaEstados();
        boolean existeEstados = listaEstados.verificaFicheiro();
        listaPagamentos=new ListaPagamentos();
        boolean existePagamentos = listaPagamentos.verificaFicheiro();
        listaVendas=new ListaVendas();
        boolean existeVendas = listaVendas.verificaFicheiro();
        listaEncomendas=new ListaEncomendas();
        boolean existeEncomendas = listaEncomendas.verificaFicheiro();

        listaEnvioMensagens=new ListaEnvioMensagens();
        boolean existeEnvioMensagens = listaEnvioMensagens.verificaFicheiro();
        listaMensagens= new ListaMensagens();
        boolean existeMensagens = listaMensagens.verificaFicheiro();


        Locale.setDefault(Locale.US);
        ImageIcon icon = new ImageIcon("resources/Icon.png");

        //Gerar os "cards"
        LoginCliente login = new LoginCliente();
        NovoCliente novoCliente = new NovoCliente();

        //Inicio Janela
        JFrame janela = new JFrame("Loja EletroJava");
        janela.setIconImage(icon.getImage());
        janela.setLayout(new CardLayout());
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        verificar=VerificacaoDados.getInstancia();
        verificar.setPainelPai(janela.getContentPane());

        janela.add(login.adicionarJanela(), "Login");
        janela.add(novoCliente.adicionarJanela(), "Registo");
        janela.setSize(1200,700);
        janela.setVisible(true);

        if(!existeFuncionarios || !existeClientes || ! existeEncomendas||!existeEnvioMensagens||!existeMensagens
                ||!existeEstados||!existeVendas||!existeHieraquias||!existeProdutos||!existePagamentos){
            String mensagemErro="Impossivel de abrir programa, documentos em falta:\n";
            if(!existeFuncionarios){
                mensagemErro+= ">'"+ GestaoFicheiros.caminhoFuncionarios+"';\n";
            }
            if(!existeClientes){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoCliente+"';\n";
            }
            if(!existeMensagens){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoMensagens+"';\n";
            }
            if(!existeEnvioMensagens){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEnvioMensagens+"';\n";
            }
            if(!existeHieraquias){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoHierarquias+"';\n";
            }
            if(!existeProdutos){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoProdutos+"';\n";
            }
            if(!existeEncomendas){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEncomendas+"';\n";
            }
            if(!existeVendas){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoVendas+"';\n";
            }
            if(!existeEstados){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEstados+"';\n";
            }
            if(!existePagamentos){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoPagamentos+"';\n";
            }
            JOptionPane.showMessageDialog(janela, mensagemErro,
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);

        }

    }
    public static void setUtilizador(Pessoa utilizador) {
        PrincipalCliente.utilizador = utilizador;
    }

    public static Pessoa getUtilizador() {
        return utilizador;
    }

    public static ListaPessoas getListaFuncionarios() {
        return listaFuncionarios;
    }

    public static ListaPessoas getListaClientes() {
        return listaClientes;
    }

    public static ListaHierarquias getListaHierarquias() {
        return listaHierarquias;
    }

    public static ListaProdutos getListaProdutos() {
        return listaProdutos;
    }

    public static ListaEncomendas getListaEncomendas() {
        return listaEncomendas;
    }

    public static ListaEnvioMensagens getListaEnvioMensagens() {
        return listaEnvioMensagens;
    }

    public static ListaPagamentos getListaPagamentos() {
        return listaPagamentos;
    }

    public static ListaVendas getListaVendas() {
        return listaVendas;
    }

    public static ListaEstados getListaEstados() {
        return listaEstados;
    }
}
