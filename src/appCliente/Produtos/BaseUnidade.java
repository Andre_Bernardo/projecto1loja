package appCliente.Produtos;

/**
 * Class que representa o nivel mais baixo na hiearquia de produtos
 */
public class BaseUnidade extends HierarquiaProdutos {

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaPrdoduto - pai da baseUnidade
     */
    public BaseUnidade(String nome, String descricao, HierarquiaProdutos parent) {
        super(nome, descricao, parent);
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaPrdoduto - pai da baseUnidade
     */
    public BaseUnidade(int id, String nome, String descricao, HierarquiaProdutos parent) {
        super(id, nome, descricao, parent);
    }

    /**
     * Devolve o tipo de hierarquia
     * @return 1-Departamento, 2-Categoria, 3-UnidadeBase
     */
    @Override
    public int tipo (){
        return 3;
    }

    /**
     * Devolve o tipo por extenso (texto)
     * @return 'Departamento','Categoria','UnidadeBase'
     */
    @Override
    public String tipoString() {
        return "Base Unidade";
    }


}
