package appCliente.Produtos;

import java.util.ArrayList;

/**
 * Categoria contem um lista de baseUnidade associado
 */
public class Categoria extends HierarquiaProdutos {

    protected ArrayList<BaseUnidade> lista;

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaPrdoduto - pai da categoria
     */
    public Categoria(String nome, String descricao, HierarquiaProdutos parent) {
        super(nome, descricao, parent);
        this.lista = new ArrayList<>();
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaPrdoduto - pai da categoria
     */
    public Categoria(int id, String nome, String descricao, HierarquiaProdutos parent) {
        super(id, nome, descricao, parent);
        this.lista = new ArrayList<>();
    }

    /**
     * Devolve o tipo de hierarquia
     * @return 1-Departamento, 2-Categoria, 3-UnidadeBase
     */
    @Override
    public int tipo (){
        return 2;
    }

    /**
     * Devolve o tipo por extenso (texto)
     * @return 'Departamento','Categoria','UnidadeBase'
     */
    @Override
    public String tipoString() {
        return "Categoria";
    }

    /**
     * Adiciona um elemento à listagem, serve para adicionar um filho
     * @param elemento HierarquiasProdutos - o que se quiser adicionar
     */
    public void adicionarElemento(BaseUnidade elemento){
        lista.add(elemento);
    }

    /**
     * Devolve um array list com os filhos da categoria
     * @return ArrayList UnidadeBase
     */
    public ArrayList<BaseUnidade> getLista() {
        return lista;
    }

    /**
     * Devolve um array list com os filhos do Departamento mas como Object
     * @return Object[] de Categorias
     */
    public Object[] getComboBoxFilhos(){
        ArrayList<HierarquiaProdutos> categorias = new ArrayList<>();
        for(HierarquiaProdutos hierarquiaProdutos:lista){
            categorias.add(hierarquiaProdutos);
        }
        return categorias.toArray();
    }

}
