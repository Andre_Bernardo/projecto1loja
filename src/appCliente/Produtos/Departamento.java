package appCliente.Produtos;

import java.util.ArrayList;

/**
 * Departamento contem um lista de categorias associado
 */
public class Departamento extends HierarquiaProdutos {

    protected ArrayList<Categoria> lista;

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     */
    public Departamento(String nome, String descricao) {
        super(nome, descricao, null);
        this.lista = new ArrayList<>();
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     */
    public Departamento(int id, String nome, String descricao) {
        super(id, nome, descricao, null);
        this.lista = new ArrayList<>();
    }

    /**
     * Devolve o tipo de hierarquia
     * @return 1-Departamento, 2-Categoria, 3-UnidadeBase
     */
    @Override
    public int tipo (){
        return 1;
    }

    /**
     * Devolve o tipo por extenso (texto)
     * @return 'Departamento','Categoria','UnidadeBase'
     */
    @Override
    public String tipoString() {
        return "Departamento";
    }

    /**
     * Adiciona um elemento à listagem, serve para adicionar um filho
     * @param elemento HierarquiasProdutos - o que se quiser adicionar
     */
    public void adicionarElemento(Categoria elemento){
        lista.add(elemento);
    }


    /**
     * Devolve um array list com os filhos do Departamento
     * @return ArrayList Categoria
     */
    public ArrayList<Categoria> getLista() {
        return lista;
    }


    /**
     * Devolve um array list com os filhos do Departamento mas como Object
     * @return Object[] de Categorias
     */
    public Object[] getComboBoxFilhos(){
        return lista.toArray();
    }


}
