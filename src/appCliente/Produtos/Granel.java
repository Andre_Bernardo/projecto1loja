package appCliente.Produtos;


/**
 * Granel extende a funcionalidades do produto, permitindo o programa ter produtos que não sejam unitários
 */
public class Granel extends Produto {

    protected String unidade;
    protected double stock;

    /**
     * Construtor normal
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock double (em Granel, o stock tem casas décimais)
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Granel(String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, double stock, double preco, double iva, double desconto) {
        super(nome, descricao, unidadeBase, marca, unidade, 0, preco, iva, desconto);
        this.stock=stock;
        this.unidade=unidade;
    }

    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock double (em Granel, o stock tem casas décimais)
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Granel(int id, String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, double stock, double preco, double iva, double desconto) {
        super(id, nome, descricao, unidadeBase, marca, unidade, 0, preco, iva, desconto);
        this.stock=stock;
        this.unidade=unidade;
    }

    /**
     * Stock de um produto a granel é um double
     * @return quantidade em stock
     */
    public double getStockGranel() {
        return stock;
    }

    /**
     * Destingir o tipo de produto
     * 1- Unitário
     * 2- Granel
     * @return 2
     */
    @Override
    public  int tipo(){
        return 2;
    }

    /**
     * Verifica a disponibiladade do produto
     * @return true, se produto disponivel em loja
     */
    @Override
    public boolean disponivel(){
        return stock>0;
    }

}
