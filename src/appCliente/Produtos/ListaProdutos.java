package appCliente.Produtos;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import appCliente.PrincipalCliente;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 * Listar produtos
 */
public class ListaProdutos implements GestaoFicheiros {

    protected ArrayList<Produto> lista;
    protected String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaProdutos(){
        lista=new ArrayList<>();
        localFicheiro=caminhoProdutos;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (se for diferente da interface)
     */
    public ListaProdutos(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    @Override
    public Object pesquisa(int id) {
        for(Produto produto:lista){
            if(produto.id==id){
                return produto;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(Produto produto:lista){
            count++;
        }
        return count;
    }

    /**
     * Pesquisa de Produto pelo respetivo códgigo identificação.
     * Devolve a Produto que tem o identificação igual ao argumento dado ou devolve NULL caso não encontre correspondencia.
     * @param identificador string procura
     * @return Devolve Produto ou NULL
     */
    public Produto pesquisa(String identificador) {
        for(Produto produto:lista){
            if(produto.getIdentificador().equals(identificador)){
                return produto;
            }
        }
        return null;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);
            lista.clear();
            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id;
            int id_unidadeBase;
            String nome;
            String descricao;
            String marca;
            String unidade;
            String stock;
            double preco;
            double iva;
            double desconto;
            char tipo;

            ListaHierarquias hierarquias = PrincipalCliente.getListaHierarquias();
            hierarquias.downloadFicheiro();

            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                tipo=linhaPartida[10].charAt(0);
                id= parseInt(linhaPartida[0]);
                id_unidadeBase= parseInt(linhaPartida[1]);
                nome=linhaPartida[2];
                descricao=linhaPartida[3];
                marca=linhaPartida[4];
                unidade=linhaPartida[5];
                stock=linhaPartida[6];
                preco=parseDouble(linhaPartida[7]);
                iva=parseDouble(linhaPartida[8])/100;
                desconto=parseDouble(linhaPartida[9])/100;

                BaseUnidade unidadeBase =(BaseUnidade)hierarquias.pesquisa(id_unidadeBase);

                switch (tipo){
                    case '1':{
                        lista.add(new Produto(id, nome,descricao,unidadeBase,marca,unidade,(int)parseDouble(stock),preco,iva,desconto));
                    }break;
                    case '2':{
                        lista.add(new Granel(id, nome,descricao,unidadeBase,marca,unidade,parseDouble(stock),preco,iva,desconto));
                    }break;
                    default:{
                        System.out.println("Erro Leitura");
                    }
                }
                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
            ordenarElementos();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;
            String stock;

            for(Produto produto: lista){

                if(produto.tipo()==1){
                    stock=Integer.toString(produto.getStock());
                }else{
                    stock=Double.toString(((Granel)produto).getStockGranel());
                }

                linha=produto.getId()+divisorColunas+
                        produto.getUnidadeBase().getId()+divisorColunas+
                        produto.getNome()+divisorColunas+
                        produto.getDescricao()+divisorColunas+
                        produto.getMarca()+divisorColunas+
                        produto.getUnidade()+divisorColunas+
                        stock+divisorColunas+
                        produto.getPreco()+divisorColunas+
                        (produto.getIva()*100)+divisorColunas+
                        (produto.getDesconto()*100)+divisorColunas+
                        produto.tipo();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){

        }


    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Produto)object);
    }

    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[10];
        //Cabeçalho
        infoTabela[0]="Identificador";
        infoTabela[1]="Departamento";
        infoTabela[2]="Categoria";
        infoTabela[3]="Base Unidade";
        infoTabela[4]="Nome";
        infoTabela[5]="Marca";
        infoTabela[6]="Disponíbiliade";
        infoTabela[7]="Preço";
        infoTabela[8]="Desconto";
        infoTabela[9]="Preço Final";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][10];

        //Informação
        int i=0;
        for(Produto produto:lista){
            infoTabela[i]=produto.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    /**
     * Ordenar elementos por identificador
     */
    private void ordenarElementos(){
        lista.sort(Produto.ordenarIdentificador);
    }

}