package appCliente.Produtos;

import java.util.Comparator;

/**
 * Produto é uma class que organiza a informação regeferentes ao produto, bem como os seus metodos
 */
public class Produto {

    private static int idGerado;

    protected int id;
    protected String identificador;
    protected String nome;
    protected String descricao;
    protected BaseUnidade unidadeBase;
    protected String marca;
    protected String unidade;
    protected int stock;
    protected double preco;
    protected double iva;
    protected double desconto;

    /**
     * Construtor normal
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock int
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Produto(String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, int stock, double preco, double iva, double desconto) {
        idGerado++;

        this.id = idGerado;
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = unidadeBase;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;

        this.identificador=unidadeBase.getIdentificador()+String.format(":%03d",id);
    }


    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock int
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Produto(int id, String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, int stock, double preco, double iva, double desconto) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = unidadeBase;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;

        this.identificador=unidadeBase.getIdentificador()+String.format(":%03d",id);

        if(id>idGerado){
            idGerado=id;
        }
    }

    /**
     * Destingir o tipo de produto
     * 1- Unitário
     * 2- Granel
     * @return 1
     */
    public  int tipo(){
        return 1;
    }

    /**
     * Devole o id do produto
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * Devolve o código de identificação do produto
     * @return String
     */
    public String getIdentificador() {
        return identificador;
    }

    /**
     * Devolve o nome do produto
     * @return String
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve descrição do produto
     * @return String
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve o Objecto UnidadeBase a que o produto pertence
     * @return UnidadeBase
     */
    public BaseUnidade getUnidadeBase() {
        return unidadeBase;
    }

    /**
     * Devolve a marca do produto
     * @return String
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Devolve a unidade de venda do produto
     * @return String
     */
    public String getUnidade() {
        return unidade;
    }

    /**
     * Devolve stock do produto
     * @return int
     */
    public int getStock() {
        return stock;
    }

    /**
     * Devolve o preço do produto;
     * @return double
     */
    public double getPreco() {
        return preco;
    }

    /**
     * Devolve o IVA sob o produto
     * @return double
     */
    public double getIva() {
        return iva;
    }

    /**
     * Devolve o desconto sob o produto
     * @return double
     */
    public double getDesconto() {
        return desconto;
    }

    /**
     * Devolve a categoria a que pertence
     * @return Categoria
     */
    public Categoria getCategoria(){
        return (Categoria) unidadeBase.getParent();
    }

    /**
     * Devolve o nome da deparamento a que pertence
     * @return Departamento
     */
    public Departamento getDepartamento(){
        return (Departamento) unidadeBase.getParent().getParent();
    }

    /**
     * Devolve o valor de venda ao publico (adição de IVA)
     * @return double
     */
    public double precoUnitarioVenda(){
        double valorTratado=preco*(1+iva)*100;
        valorTratado=Math.round(valorTratado);
        return valorTratado/100.0;
    }

    /**
     * Devolve o valor de venda ao publico (adição de IVA e desconto incluidos)
     * @return double
     */
    public double precoUnitarioVendaComDesconto(){
        double valorTratado=(preco*(1+iva-desconto))*100;
        valorTratado=Math.round(valorTratado);
        return valorTratado/100.0;
    }

    /**
     * Verifica a disponibiladade do produto
     * @return true, se produto disponivel em loja
     */
    public boolean disponivel(){
        return stock>0;
    }

    /**
     * Escreve a linha da tabela
     * @return Object[]
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[11];
        linha[0]=identificador;
        linha[1]=getDepartamento().getNome();
        linha[2]=getCategoria().getNome();
        linha[3]=getUnidadeBase().getNome();
        linha[4]=nome;
        linha[5]=marca;
        if(disponivel()){
            linha[6]="Disponível";
        }else{
            linha[6]="Esgotado";
        }
        linha[7]=precoUnitarioVenda();
        linha[8]=(desconto*100);
        linha[9]=precoUnitarioVendaComDesconto();

        return linha;
    }

    /**
     * Ordenação por identificador
     */
    public static Comparator<Produto> ordenarIdentificador=new Comparator<Produto>() {
        @Override
        public int compare(Produto o1, Produto o2) {
            return o1.getIdentificador().compareTo(o2.getIdentificador());
        }
    };

}
