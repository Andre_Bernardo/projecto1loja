package appLoja.Encomendas;

import Comuns.Pagamento;
import appLoja.Pessoas.Cliente;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Class Encomenda
 * Tem a informação em relação a uma encondeda, permite calcular preços, ver itens, estado e pagamento
 */
public class Encomenda {

    private int id;
    private static int idgerado;
    private Cliente cliente;
    private EstadoEncomenda estado;
    private Pagamento pagamento;
    private boolean levantarLoja;
    private ArrayList<VendaProduto> vendas;

    /**
     * Constrtor normal
     * @param cliente Cliente
     * @param pagamento Pagamento
     * @param estado Estado
     */
    public Encomenda(Cliente cliente, Pagamento pagamento, EstadoEncomenda estado){
        idgerado++;
        this.id=idgerado;
        this.cliente=cliente;
        this.vendas=new ArrayList<>();
        this.pagamento=pagamento;
        this.estado=estado;
    }

    /**
     * Cosntrutor para Guardar na Lista
     * @param id id encomenda
     * @param cliente L_cliente cliente da encomenda
     * @param pagamento pagamento da encomenda
     * @param levantarLoja boolean se é para levantar loja
     * @param estado Estado Encomenda
     */
    public Encomenda(int id, Cliente cliente, Pagamento pagamento, boolean levantarLoja, EstadoEncomenda estado){
        this.id=id;
        if (id>idgerado){
            idgerado=id;
        }
        this.cliente=cliente;
        this.pagamento=pagamento;
        this.levantarLoja=levantarLoja;
        this.estado=estado;
        this.vendas=new ArrayList<>();
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[7];
        linha[0]=getId();
        linha[1]=String.format("%tF %tR",getEstado().getDataEncomenda(),getEstado().getDataEncomenda());
        linha[2]=getCliente().getPriNome()+" "+getCliente().getUltNome();
        linha[3]=getEstado().estadoString();
        linha[4]=numeroItens();
        if(estado.getTipoEstado()==0) {
            linha[5] = "POR PAGAR";
        }
        else{
            linha[5] = pagamento.tipoString();
        }
        linha[6]=levantarLoja?"Levantar Loja":"Enviar";
        return linha;
    }

    /**
     * Devolve o número de itens na encomenda (Se produto = Granel conta apenas por 1 independentemente da quantidade)
     * @return int
     */
    public int numeroItens(){
        int nItens=0;
        for(VendaProduto venda:vendas){
            if(venda.getProduto().tipo()==1){
                nItens+=venda.getQuantidade();
            }else{
                nItens++;
            }
        }
        return nItens;
    }

    /**
     * Metodo para adicionar as vendas a uma encomenda no arranque
     * @param vendas ArrayList VendaProduto
     */
    public void setVendas(ArrayList<VendaProduto> vendas) {
        this.vendas = vendas;
    }

    public int getId() {
        return id;
    }

    public static int getIdgerado() {
        return idgerado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public ArrayList<VendaProduto> getVendas() {
        return vendas;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public int pagamentoID(){
        if(pagamento==null){
            return 0;
        }
        return pagamento.getId();
    }

    public boolean isLevantarLoja() {
        return levantarLoja;
    }

    public EstadoEncomenda getEstado() {
        return estado;
    }

    /**
     * Ordenar por estado
     */
    public static Comparator<Encomenda> ordenarEstado = new Comparator<Encomenda>() {
        @Override
        public int compare(Encomenda o1, Encomenda o2) {
            if(o1.getEstado().getTipoEstado()==o2.getEstado().getTipoEstado()){
                return o1.getEstado().getDataEncomenda().compareTo(o2.getEstado().getDataEncomenda());
            }
            return Integer.compare(o1.getEstado().getTipoEstado(),o2.getEstado().getTipoEstado());
        }
    };
}
