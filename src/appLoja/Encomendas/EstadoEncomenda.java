package appLoja.Encomendas;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class Estado Encomenda
 * Esta class além de deter a informação relativa ao estado guarda as datas em que os vários estágios ocorreram
 */
public class EstadoEncomenda {

    private static String[] estadoPossiveis = {"Carrinho",
            "Encomenda Realizada",
            "Em Preparação",
            "Enviada", "Pronto a levantar",
            "Não Recebida","Recebida"};

    protected int id;
    private static int idGerado;
    private GregorianCalendar dataEncomenda;
    private GregorianCalendar dataPreparacao;
    private GregorianCalendar dataExpedicao;
    private GregorianCalendar dataChegada;
    private int estado;

    /**
     * Construtor para carregamento de dados
     * @param id (id)
     * @param dataEncomenda GregorianCalendar
     * @param dataPreparacao GregorianCalendar
     * @param dataExpedicao GregorianCalendar
     * @param dataChegada GregorianCalendar
     */
    EstadoEncomenda(int id, GregorianCalendar dataEncomenda, GregorianCalendar dataPreparacao, GregorianCalendar dataExpedicao, GregorianCalendar dataChegada,int estado){
        if(id>idGerado){
            idGerado=id;
        }
        this.id=id;
        this.dataEncomenda=dataEncomenda;
        this.dataPreparacao=dataPreparacao;
        this.dataExpedicao=dataExpedicao;
        this.dataChegada=dataChegada;
        this.estado=estado;
    }

    public Object[] linhaTabela(){
        Object[] linha = new Object[6];
        linha[0]=getId();
        linha[1]=getDataEncomenda();
        linha[2]=getDataPreparacao();
        linha[3]=getDataExpedicao();
        linha[4]=getDataChegada();
        linha[5]= getTipoEstado();
        return linha;
    }

    public int getId() {
        return id;
    }

    public GregorianCalendar getDataEncomenda() {
        return dataEncomenda;
    }

    public GregorianCalendar getDataPreparacao() {
        return dataPreparacao;
    }

    public GregorianCalendar getDataExpedicao() {
        return dataExpedicao;
    }

    public GregorianCalendar getDataChegada() {
        return dataChegada;
    }

    public int getTipoEstado() {
        return estado;
    }

    public String estadoString(){
        if(estado==3 && dataChegada.before(new GregorianCalendar())){
            return estadoPossiveis[6];
        }
        return estadoPossiveis[estado];
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public void setDataExpedicao(GregorianCalendar dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }

    /**
     * Altera a data de chegada do estado
     * @param dataChegada GregorianCalendar
     */
    public void setDataChegada(GregorianCalendar dataChegada) {
        this.dataChegada = dataChegada;
        dataChegada.set(Calendar.HOUR,23);
        dataChegada.set(Calendar.MINUTE,59);
    }


    public void setDataPreparacao(GregorianCalendar dataPreparacao) {
        this.dataPreparacao = dataPreparacao;
    }

}
