package appLoja.Encomendas;

import appLoja.Produtos.Granel;
import appLoja.Produtos.Produto;

/**
 * VendaProduto é uma class que serve para manter a informação das vendas que compôem uma encomenda
 */
public class    VendaProduto {

    private int idEncomenda;
    private double quantidade;
    private Produto produto;
    private double preco;
    private double iva;
    private double desconto;
    private boolean empacotado;

    /**
     * Construtor para uma gerar uma nova venda (e associa-la a uma encomenda)
     * @param idEncomenda idEncomenda
     * @param quantidade double
     * @param produto Produto (Cliente)
     * @param preco double
     * @param iva double
     * @param desconto double
     * @param empacotado boolean
     */
    public VendaProduto(int idEncomenda, double quantidade, Produto produto, double preco, double iva, double desconto, boolean empacotado) {
        this.idEncomenda = idEncomenda;
        this.quantidade = quantidade;
        this.produto = produto;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;
        this.empacotado = empacotado;
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[6];
        linha[0]=getProduto().getIdentificador();
        linha[1]=getProduto().getNome();
        linha[2]=getProduto().getMarca();
        linha[3]=getQuantidade();
        if(getProduto().tipo()==1){
            linha[4]=(double)getProduto().getStock();
        }else{
            linha[4]=((Granel)getProduto()).getStockGranel();
        }
        linha[5]=isEmpacotado();
        return linha;
    }

    public double precoVenda(){
        return quantidade*(preco*(1+iva-desconto));
    }
    public int getIdEncomenda() {
        return idEncomenda;
    }

    public Produto getProduto() {
        return produto;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public boolean isEmpacotado() {
        return empacotado;
    }

    public double getPreco() {
        return preco;
    }

    public double getIva() {
        return iva;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public void setEmpacotado(boolean empacotado) {
        this.empacotado = empacotado;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }
}
