package appLoja.Logs;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import appLoja.Pessoas.Gerente;
import appLoja.Pessoas.ListaPessoas;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

/**
 * Listar os Logs
 */
public class ListaLogs implements GestaoFicheiros {
    private ArrayList<Log> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaLogs(){
        lista=new ArrayList<>();
        localFicheiro=caminhoLogs;
    }

    @Override
    public Object pesquisa(int id) {
        for(Log log:lista){
            if(log.getId()==id){
                return log;
            }
        }
        return null;
    }

    @Override
    public void downloadFicheiro() {
        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id;
            String[] dataDia;
            String[] dataHora;
            GregorianCalendar dataLog;
            Gerente responsavel;
            String tipo;
            String mensagem;

            ListaPessoas listaFuncionarios = new ListaPessoas(1);
            listaFuncionarios.downloadFicheiro();

            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id=parseInt(linhaPartida[0]);
                dataDia=linhaPartida[1].split("-");
                dataHora=linhaPartida[2].split(":");
                //Ano, Mes, Dia, Hora, Minuto
                dataLog=new GregorianCalendar(parseInt(dataDia[0]),parseInt(dataDia[1])-1,parseInt(dataDia[2]),
                        parseInt(dataHora[0]), parseInt(dataHora[1]));
                responsavel= (Gerente) listaFuncionarios.pesquisa(parseInt(linhaPartida[3]));
                tipo=linhaPartida[4];
                mensagem=linhaPartida[5];

                lista.add(new Log(id,dataLog,responsavel,tipo,mensagem));

                linha=conteudo.readLine();
            }
            ordenarElementos();
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public void uploadFicheiro() {
        ordenarElementos();
        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;
            for(Log log: lista){
                int responsavel;
                if(log.getResponsavel()==null){
                    responsavel=0;
                }else {
                    responsavel=log.getResponsavel().getId();
                }
                linha=log.getId()+divisorColunas+
                        String.format("%tF",log.getData())+divisorColunas+
                        String.format("%tR",log.getData())+divisorColunas+
                        responsavel+divisorColunas+
                        log.getTipo()+divisorColunas+
                        log.getMensagem();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();

        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            try {
                //Caso não exista cria um novo
                ficheiro.openFileWriter(localFicheiro);
                ficheiro.closeFileWriter();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public String[] listarCabecalhoTabela() {
        String[] infoTabela = new String[4];
        //Cabeçalho
        infoTabela[0]="Data";
        infoTabela[1]="Responsável";
        infoTabela[2]="Tipo";
        infoTabela[3]="Mensagem";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][4];

        //Informação
        int i=0;
        for(Log log:lista){
            infoTabela[i]=log.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Log)object);
    }

    @Override
    public int getTotal() {
        return 0;
    }

    private void ordenarElementos(){
        lista.sort(Log.ordenarData.reversed());
    }
}
