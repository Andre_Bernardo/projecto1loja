package appLoja.Logs;

import appLoja.Pessoas.Gerente;

import java.util.Comparator;
import java.util.GregorianCalendar;

/**
 * Class que serve para organizar os Logs gerados.
 * Um log contem uma data, um responsável um tipo e uma mensagem
 */
public class Log {
    private static int idGerado;

    private int id;
    private GregorianCalendar data;
    private Gerente responsavel;
    private String tipo;
    private String mensagem;

    /**
     * Construtor Normal cria um novo log atribui ID.
     * @param responsavel (Pessoa)
     * @param tipo (String) novo produto, alteração produto, novo funcionário
     * @param mensagem (String) descrição da alteração
     */
    public Log(Gerente responsavel, String tipo, String mensagem){
        idGerado++;
        this.id=idGerado;
        this.data=new GregorianCalendar();
        this.responsavel=responsavel;
        this.tipo=tipo;
        this.mensagem=mensagem;
    }

    /**
     * Construtor para carregamento de dados
     * @param id int id log
     * @param data GregorianCalendar data do log
     * @param responsavel Pessoa
     * @param tipo String
     * @param mensagem String
     */
    public Log(int id, GregorianCalendar data,Gerente responsavel, String tipo, String mensagem){
        this.id=id;
        this.data=data;
        this.responsavel=responsavel;
        this.tipo=tipo;
        this.mensagem=mensagem;

        if(id>idGerado){
            idGerado=id;
        }
    }

    /**
     * Devovle o ID do log
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * Devolve a data do log
     * @return GregorianCalendar
     */
    public GregorianCalendar getData() {
        return data;
    }

    /**
     * Devolve o tipo do Log (o tipo da ação guardada)
     * @return String
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Devolve a mensagem do Log (a descrição da ação guardada)
     * @return String
     */
    public String getMensagem() {
        return mensagem;
    }

    /**
     * Devolve a pessoa que executou a ação
     * @return Pessoa
     */
    public Gerente getResponsavel() {
        return responsavel;
    }

    /**
     * Devolve uma String com a data no formato aaaa-mm-dd hh:mm
     * @return String a data do log por extenso
     */
    public String dataExtenso(){
        return String.format("%tF %tR",data,data);
    }


    /**
     * Devolve o Objecto contruido para a tabela
     * @return a linha para a tabela
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[4];
        linha[0]=dataExtenso();
        if(responsavel==null){
            linha[1]="SISTEMA";
        }else {
            linha[1]=responsavel.getId()+" "+responsavel.getPriNome()+" "+responsavel.getUltNome();
        }
        linha[2]=tipo;
        linha[3]=mensagem;
        return linha;
    }

    /**
     * Ordenar por data em cima apresenta o mais novo
     */
    public static Comparator<Log> ordenarData = new Comparator<Log>() {
        @Override
        public int compare(Log o1, Log o2) {
            return o1.getData().compareTo(o2.data);
        }
    };

}
