package appLoja.Mensagens;

import appLoja.Pessoas.Cliente;

/**
 * Class EnvioMensagem
 * Faz a correspondencia da mensagem para um cliente (a mesma mensagem pode seguir para vários clientes diferentes)
 */
public class EnvioMensagem {

    private Cliente cliente;
    private Mensagem mensagem;

    /**
     * Construtor para nova mensagem
     * @param cliente Cliente
     * @param mensagem Mensagem
     */
    public EnvioMensagem(Cliente cliente, Mensagem mensagem){
       this.cliente=cliente;
       this.mensagem=mensagem;
    }

    /**
     * Devolve o ID do cliente da mensagem
     * @return int
     */
    public int getiD_Cliente() {
        return cliente.getId();
    }

    /**
     * Devolve o ID da mensagem
     * @return int
     */
    public int getiD_Mensagem() {
        return mensagem.getId();
    }

    /**
     * Devolve a mensagem
     * @return Mensagem
     */
    public Mensagem getMensagem() {
        return mensagem;
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[2];
        linha[0]=cliente.getId();
        linha[1]=mensagem.getId();
        return linha;
    }

}
