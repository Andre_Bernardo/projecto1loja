package appLoja.Mensagens;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import appLoja.Pessoas.Cliente;
import appLoja.Pessoas.ListaPessoas;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

/**
 * Listar os Envios Mensagens
 */
public class ListaEnvioMensagens implements GestaoFicheiros {

    private ArrayList<EnvioMensagem> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaEnvioMensagens(){
        lista=new ArrayList<>();
        localFicheiro=caminhoEnvioMensagens;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (se for um caminho diferente da interface)
     */
    public ListaEnvioMensagens(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    /**
     * Devolve a lista de envios de mensagens
     * @return ArrayList EnvioMensagen
     */
    public ArrayList<EnvioMensagem> getLista() {return lista;}

    /**
     *  BUSCAR INFO
     *  Carregar informação do ficheiro para a lista
     */
    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();
            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id_Cliente;
            int id_Mensagem;
            Cliente cliente;
            Mensagem mensagem;

            ListaPessoas pessoas = new ListaPessoas(2);
            pessoas.downloadFicheiro();//PARA QUÊ? Incluir no envio de mensagem?!

            ListaMensagens mensagens = new ListaMensagens();
            mensagens.downloadFicheiro();


            //char tipo;//PARA QUÊ? Necessário para distinguir
            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id_Cliente= parseInt(linhaPartida[0]);
                cliente = (Cliente) pessoas.pesquisa(id_Cliente);
                id_Mensagem = parseInt(linhaPartida[1]);
                mensagem = (Mensagem) mensagens.pesquisa(id_Mensagem);

                lista.add(new EnvioMensagem(cliente,mensagem));

                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    /**
     * GUARDAR INFO
     * Descarregar informação da lista para o ficheiro
     */
    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(EnvioMensagem envio: lista){
                linha=envio.getiD_Cliente()+divisorColunas+
                        envio.getiD_Mensagem();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }


    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            try {
                //Caso não exista cria um novo
                ficheiro.openFileWriter(localFicheiro);
                ficheiro.closeFileWriter();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((EnvioMensagem) object);
    }

    /**
     * Devolve o array que serve de cabeçalho para uma tabela
     * @return String[] - Cabeçalho
     */
    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[2];
        //Cabeçalho
        infoTabela[0]="id Cliente";
        infoTabela[1]="id Mensagem";
        return infoTabela;
    }

    /**
     * Devolve array de Objects que são os dados da tabela.
     *
     * @return Devolve um array de objectos com os dados da tabela
     */
    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][5];

        //Informação
        int i=0;
        for(EnvioMensagem envio:lista){
            infoTabela[i]=envio.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    /**
     * Não faz sentido no envio de mensagens
     * @param id int
     * @return null
     */
    @Override
    public Object pesquisa(int id) {
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(EnvioMensagem envioMensagem:lista){
            count++;
        }
        return count;
    }
}
