package appLoja.Mensagens;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;
import Comuns.Pessoa;
import appLoja.Pessoas.ListaPessoas;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

/**
 * Listar as Mensagens
 */
public class ListaMensagens implements GestaoFicheiros {

    private ArrayList<Mensagem> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaMensagens(){
        lista=new ArrayList<>();
        localFicheiro=caminhoMensagens;
    }


    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (Se for diferente do caminho da interface)
     */
    public ListaMensagens(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }


    public ArrayList<Mensagem> getLista() {
        return lista;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            lista.clear();

            //Separar linha em linha
            String linha= conteudo.readLine();

            String[] linhaPartida;

            int id=0;
            int id_Remetente;
            Pessoa remetente;
            String[] dataAux;
            GregorianCalendar data;
            String assunto;
            String texto;

            ListaPessoas pessoas = new ListaPessoas(1);
            pessoas.downloadFicheiro();//PARA QUÊ? Incluir no envio de mensagem?!


            //char tipo;//PARA QUÊ? Necessário para distinguir
            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                id= parseInt(linhaPartida[0]);
                id_Remetente = parseInt(linhaPartida[1]);
                remetente = (Pessoa) pessoas.pesquisa(id_Remetente);
                dataAux=linhaPartida[2].split("-");
                data =new GregorianCalendar(parseInt(dataAux[0]), parseInt(dataAux[1])-1, parseInt(dataAux[2]));
                assunto=linhaPartida[3];
                texto=linhaPartida[4];

                lista.add(new Mensagem(id,assunto,texto,remetente,data));//CRIAR OUTRO CONSTRUTOR COM TODOS OS ELEMENTOS?

                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;


            for(Mensagem mensagem: lista){
                linha=mensagem.getId()+divisorColunas+
                        mensagem.getRemetente().getId()+divisorColunas+
                        String.format("%04d-%02d-%02d",mensagem.getAnoData(),mensagem.getMesData()+1,mensagem.getDiaData())+divisorColunas+
                        mensagem.getAssunto()+divisorColunas+
                        mensagem.getTexto();

                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }


    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            try {
                //Caso não exista cria um novo
                ficheiro.openFileWriter(localFicheiro);
                ficheiro.closeFileWriter();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Local do ficheiro"+localFicheiro + " Com "+ lista.size()+" itens";
    }

    @Override
    public void adicionarElemento(Object object) {
        lista.add((Mensagem)object);
    }


    @Override
    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[3];
        //Cabeçalho
        infoTabela[0]="Data";
        infoTabela[1]="Assunto";
        infoTabela[2]="Mensagem";
        return infoTabela;
    }

    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][3];

        //Informação
        int i=0;
        for(Mensagem mensagem:lista){
            infoTabela[i]=mensagem.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    @Override
    public Object pesquisa(int id) {
        for(Mensagem mensagem:lista){
            if(mensagem.getId()==id){
                return mensagem;
            }
        }
        return null;
    }

    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(Mensagem mensagem:lista){
            count++;
        }
        return count;
    }

}
