package appLoja.Mensagens;

import Comuns.Pessoa;

import java.util.GregorianCalendar;

/**
 * Mensagem enviada para o cliente, contem um remetente, data, assunto e texto
 */
public class Mensagem {

    private int id;
    private static int idgerado;
    private Pessoa remetente;
    private GregorianCalendar data;
    private String assunto;
    private String texto;


    /**
     * Construtor para mensagem nova
     * @param assunto assunto da mensagem
     * @param texto corpo da mensagem
     * @param remetente quem escreve
     */
    public Mensagem(String assunto, String texto,Pessoa remetente){
        idgerado++;
        this.id=idgerado;
        this.remetente=remetente;
        data= new GregorianCalendar();
        this.assunto=assunto;
        this.texto=texto;
    }

    /**
     * Construtor para Guardar na Lista
     * @param id id da mensagem
     * @param assunto assunto da mensagem
     * @param texto corpo da mensagem
     * @param remetente quem escreve
     * @param data data da mensagem
     */
    public Mensagem(int id,String assunto, String texto,Pessoa remetente, GregorianCalendar data){
        this.id=id;
        if (id>idgerado){
            idgerado=id;
        }
        this.remetente=remetente;
        this.data=data;
        this.assunto=assunto;
        this.texto=texto;
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela(){
        Object[] linha = new Object[3];
        linha[0]=String.format("%04d-%02d-%02d",getAnoData(),getMesData()+1,getDiaData());
        linha[1]=getAssunto();
        linha[2]=getTexto();
        return linha;
    }

    public int getId() {
        return id;
    }

    public Pessoa getRemetente() {
        return remetente;
    }

    public String getTexto() {
        return texto;
    }

    public String getAssunto() {
        return assunto;
    }

    public int getDiaData() {
        return data.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public int getMesData() {
        return data.get(GregorianCalendar.MONTH);
    }

    public int getAnoData() {
        return data.get(GregorianCalendar.YEAR);
    }

}
