package appLoja.Pessoas;

import Comuns.Pessoa;

import java.util.GregorianCalendar;

/**
 * Cliente só é necessário como elemento para organizar informação, não executa nenhuma ação
 */
public class Cliente extends Pessoa {
    /**
     * Construtor normal, criar um novo utilizador requer apenas nome, email, password e data nascimento (verificação de idade)
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param email String - email
     * @param password String - password
     */
    public Cliente(String priNome, String ultNome, GregorianCalendar dataNasc, String email, String password) {
        super(priNome, ultNome, dataNasc, email, password);
    }
    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int - id da pessoa
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     * @param password String - password
     */
    public Cliente(int id, String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email, String password) {
        super(id, priNome, ultNome, dataNasc, morada, telefone, email, password);
    }

    @Override
    public String tipoString() {
        return "Cliente";
    }

    @Override
    public int tipo() {
        return 3;
    }
}
