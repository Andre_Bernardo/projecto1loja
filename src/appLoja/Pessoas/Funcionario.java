package appLoja.Pessoas;

import Comuns.Pessoa;
import appLoja.Encomendas.*;
import appLoja.Mensagens.EnvioMensagem;
import appLoja.Mensagens.ListaEnvioMensagens;
import appLoja.Mensagens.ListaMensagens;
import appLoja.Mensagens.Mensagem;
import appLoja.Produtos.ListaProdutos;
import appLoja.Produtos.Produto;

import java.util.GregorianCalendar;

/**
 * Funcionário é um utilizador da aplicação, esta class e a interface "UtilizadorLoja" tem todos os metodos que pode realizar
 */
public class Funcionario extends Pessoa implements UtilizadorLoja{
    /**
     * Construtor normal, criar um novo utilizador requer apenas nome, email, password e data nascimento (verificação de idade)
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param email String - email
     * @param password String - password
     */
    public Funcionario(String priNome, String ultNome, GregorianCalendar dataNasc, String email, String password) {
        super(priNome, ultNome, dataNasc, email, password);
    }
    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int - id da pessoa
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     * @param password String - password
     */
    public Funcionario(int id, String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email, String password) {
        super(id, priNome, ultNome, dataNasc, morada, telefone, email, password);
    }


    /**
     * Método para actualizar o estado da encomenda para "preparação", e respetiva data
     * @param encomenda Encomenda
     * @return true se for posivel actualizar, false se não for possivel actualizar
     */
    public boolean prepararEncomenda (Encomenda encomenda) {
        ListaEstados listaEstados = verEstados();
        EstadoEncomenda estadoActualizado = (EstadoEncomenda) listaEstados.pesquisa(encomenda.getEstado().getId());

        if(estadoActualizado!=null) {
            encomenda.getEstado().setEstado(2);
            estadoActualizado.setEstado(2);
            estadoActualizado.setDataPreparacao(new GregorianCalendar());
            listaEstados.uploadFicheiro();
            return true;
        }
        return false;
    }

    /**
     * Metodo para despachar a encomenda, altualiza o estado da encomenda e as respetivas datas
     * @param encomenda Encomenda
     * @param dias String (inteiro) dias para a entrega da encomenda
     * @return true se for posivel actualizar, false se não for possivel actualizar
     */
    public boolean despacharEncomenda(Encomenda encomenda, String dias) {
        int diasExpedicao= Integer.parseInt(dias);
        GregorianCalendar dataExpedicao= new GregorianCalendar();
        GregorianCalendar dataChegada = new GregorianCalendar();

        ListaEstados listaEstados = verEstados();
        EstadoEncomenda estadoActualizado = (EstadoEncomenda) listaEstados.pesquisa(encomenda.getEstado().getId());
        if(estadoActualizado!=null){
            if(encomenda.isLevantarLoja()) {
                encomenda.getEstado().setEstado(4);
                estadoActualizado.setEstado(4);
                estadoActualizado.setDataExpedicao(dataExpedicao);
                dataChegada= (GregorianCalendar) dataExpedicao.clone();
                dataChegada.add(GregorianCalendar.DAY_OF_YEAR,diasExpedicao);
                estadoActualizado.setDataChegada(dataChegada);
            }else{
                encomenda.getEstado().setEstado(3);
                estadoActualizado.setEstado(3);
                estadoActualizado.setDataExpedicao(dataExpedicao);
                dataChegada= (GregorianCalendar) dataExpedicao.clone();
                dataChegada.add(GregorianCalendar.DAY_OF_YEAR,diasExpedicao);
                estadoActualizado.setDataChegada(dataChegada);
            }
            listaEstados.uploadFicheiro();
            return true;
        }
        return false;
    }

    /**
     * Envio de mensagem para um cliente
     * @param assuntoTxt String assunto
     * @param mensagemTxt String mensagem
     * @param cliente cliente, destinatário
     */
    public void envioMensagem(String assuntoTxt, String mensagemTxt, Cliente cliente){
        ListaMensagens listaMensagens = verMensagens();
        Mensagem mensagem = new Mensagem(assuntoTxt,mensagemTxt,this);
        listaMensagens.adicionarElemento(mensagem);
        listaMensagens.uploadFicheiro();

        ListaEnvioMensagens listaEnvioMensagens= verEnviosMensagens();
        listaEnvioMensagens.adicionarElemento(new EnvioMensagem(cliente,mensagem));
        listaEnvioMensagens.uploadFicheiro();


    }

    @Override
    public String tipoString() {
        return "Funcionário";
    }

    @Override
    public int tipo() {
        return 1;
    }

    /**
     * Empacotar venda (tornar um produto pronto para envio). Metodo devolve falso se não tiver stock para satisfazer a venda
     * @param idProduto int
     * @param idEncomenda int
     * @param pronto boolean
     * @return true se for possivel empacotarVenda
     */
    public boolean empacotarVenda(int idProduto, int idEncomenda, boolean pronto) {
        ListaVendas listaVendas= verVendas();
        VendaProduto venda =listaVendas.pesquisa(idEncomenda,idProduto);
        if(venda!=null) {
            ListaProdutos listaProdutos = verProdutos();
            Produto produto = (Produto) listaProdutos.pesquisa(idProduto);
            if (pronto) {
                if (produto.actualizarStock(venda.getQuantidade())) { //retira a quantidade do Stock quando emitido como empacotado
                    listaProdutos.uploadFicheiro();
                    venda.setEmpacotado(true);
                    listaVendas.uploadFicheiro();
                    return true;
                }
            } else {
                if (produto.actualizarStock(-venda.getQuantidade())) {
                    listaProdutos.uploadFicheiro();
                    venda.setEmpacotado(false);
                    listaVendas.uploadFicheiro();
                    return true;
                }
            }
        }
        return false;
    }
}
