package appLoja.Pessoas;

import Comuns.ListaPagamentos;
import Comuns.Pagamento;
import Comuns.Pessoa;
import appLoja.Encomendas.*;
import appLoja.Logs.ListaLogs;
import appLoja.Logs.Log;
import appLoja.Mensagens.EnvioMensagem;
import appLoja.Mensagens.ListaEnvioMensagens;
import appLoja.Mensagens.ListaMensagens;
import appLoja.Mensagens.Mensagem;
import appLoja.PrincipalLoja;
import appLoja.Produtos.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Gerente é um utilizador da aplicação, esta class e a interface "UtilizadorLoja" tem todos os metodos que pode realizar
 */
public class Gerente extends Pessoa implements UtilizadorLoja{
    /**
     * Construtor normal, criar um novo utilizador requer apenas nome, email, password e data nascimento (verificação de idade)
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param email String - email
     * @param password String - password
     */
    public Gerente(String priNome, String ultNome, GregorianCalendar dataNasc, String email, String password) {
        super(priNome, ultNome, dataNasc, email, password);
    }
    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int - id da pessoa
     * @param priNome String - primeiro nome
     * @param ultNome String - ultimo nome
     * @param dataNasc GregorianCalendar - data nascimento
     * @param morada String - morada
     * @param telefone String - telefone
     * @param email String - email
     * @param password String - password
     */
    public Gerente(int id, String priNome, String ultNome, GregorianCalendar dataNasc, String morada, String telefone, String email, String password) {
        super(id, priNome, ultNome, dataNasc, morada, telefone, email, password);
    }

    /**
     * Adicionar um funcionário a lista de funcionários. E inserir nos appLoja.Logs a respetiva entrada.
     * @param funcionario Funcionario (a adicionar)
     */
    public void adicionarFuncionario (Funcionario funcionario) {
        ListaPessoas listaFuncionarios = verFuncionarios();
        listaFuncionarios.adicionarElemento(funcionario);
        listaFuncionarios.uploadFicheiro();
        String mensagemLog=funcionario.getPriNome()+" "+funcionario.getUltNome()+" - "+funcionario.getEmail();
        adicionarEntradaLog("Novo Funcionário",mensagemLog);
    }

    /**
     * Editar um hierarquia já existente. E inserir nos appLoja.Logs a respetiva entrada.
     * @param nome Nome da Hierarquia
     * @param descricao Descrição da Hierarquia
     * @param hierarquiaOriginal HierarquiaProdutos (hiearquia a editar)
     * @return true se ação foi realizada, false se já existe uma hierarquia com o mesmo nome
     */
    public boolean editarHierarquia (String nome, String descricao, HierarquiaProdutos hierarquiaOriginal) {
        ListaHierarquias listaHierarquias= verHierarquias();
        HierarquiaProdutos hierarquiaEditar = (HierarquiaProdutos) listaHierarquias.pesquisa(hierarquiaOriginal.getId());
        //Verificar Departamentos iguais
        if(hierarquiaEditar.tipo()==1 &&
                listaHierarquias.nomeRepetidoDepartamento(nome,hierarquiaEditar.getId())==null){
            hierarquiaEditar.editarHierarquia(nome, descricao);
            listaHierarquias.uploadFicheiro();
            adicionarEntradaLog("Editar Hierarquia",hierarquiaEditar.mensagemLog(hierarquiaOriginal));
            return true;
        }else if(hierarquiaEditar.tipo()==2 &&
                ((Departamento)hierarquiaEditar.getParent()).nomeRepetido(nome,hierarquiaEditar.getId())==null){
            hierarquiaEditar.editarHierarquia(nome, descricao);
            listaHierarquias.uploadFicheiro();
            adicionarEntradaLog("Editar Hierarquia",hierarquiaEditar.mensagemLog(hierarquiaOriginal));
            return true;
        }else if(hierarquiaEditar.tipo()==3 &&
                ((Categoria)hierarquiaEditar.getParent()).nomeRepetido(nome,hierarquiaEditar.getId())==null){
            hierarquiaEditar.editarHierarquia(nome, descricao);
            listaHierarquias.uploadFicheiro();
            adicionarEntradaLog("Editar Hierarquia",hierarquiaEditar.mensagemLog(hierarquiaOriginal));
            return true;
        }
        return false;
    }


    /**
     * Adicionar uma nova hierarquia a lista de hierarquiaProdutos. E inserir nos Logs a respetiva entrada.
     * @param hierarquiaNova HierarquiaProdutos
     * @return true se ação foi realizada, false se já existe uma hierarquia com o mesmo nome
     */
    public boolean novaHierarquia (HierarquiaProdutos hierarquiaNova) {
        ListaHierarquias listaHierarquias=verHierarquias();

        if(hierarquiaNova.tipo()==1 &&
                listaHierarquias.nomeRepetidoDepartamento(hierarquiaNova.getNome(),hierarquiaNova.getId())==null){
            listaHierarquias.adicionarElemento(hierarquiaNova);
            listaHierarquias.uploadFicheiro();
                adicionarEntradaLog("Nova Hierarquia",hierarquiaNova.mensagemLog(null));
            return true;
        }else if(hierarquiaNova.tipo()==2 &&
                ((Departamento)hierarquiaNova.getParent()).nomeRepetido(hierarquiaNova.getNome(),hierarquiaNova.getId())==null){
            listaHierarquias.adicionarElemento(hierarquiaNova);
            listaHierarquias.uploadFicheiro();
                adicionarEntradaLog("Nova Hierarquia",hierarquiaNova.mensagemLog(null));
         return true;
        }else if(hierarquiaNova.tipo()==3 &&
                ((Categoria)hierarquiaNova.getParent()).nomeRepetido(hierarquiaNova.getNome(), hierarquiaNova.getId())==null){
            listaHierarquias.adicionarElemento(hierarquiaNova);
            listaHierarquias.uploadFicheiro();
                adicionarEntradaLog("Nova Hierarquia",hierarquiaNova.mensagemLog(null));
            return true;
        }
        return false;
    }

    /**
     * metodo para verificar se pode eliminar uma hierarquia, Só pode eliminar se a hierarquai não tiver filhos
     * @param hierarquia HierarquiaProdutos
     * @return true se for possivel de eliminar
     */
    public boolean eliminiarHierarquia(HierarquiaProdutos hierarquia){
        ListaHierarquias listaHierarquias = verHierarquias();
        HierarquiaProdutos hierarquiaEliminar= (HierarquiaProdutos) listaHierarquias.pesquisa(hierarquia.getId());

        if(hierarquiaEliminar!=null && hierarquiaEliminar.tipo()==1 &&
                ((Departamento)hierarquiaEliminar).getLista().size()==0){
            listaHierarquias.eliminarElemento(hierarquiaEliminar);
            listaHierarquias.uploadFicheiro();
            adicionarEntradaLog("Eliminar Hierarquia",hierarquia.mensagemLog(null));

            return true;
        }else if(hierarquiaEliminar!=null && hierarquiaEliminar.tipo()==2 &&
                ((Categoria)hierarquiaEliminar).getLista().size()==0){
            listaHierarquias.eliminarElemento(hierarquiaEliminar);
            listaHierarquias.uploadFicheiro();
            adicionarEntradaLog("Eliminar Hierarquia",hierarquia.mensagemLog(null));
            return true;
        }else if(hierarquiaEliminar!=null && hierarquiaEliminar.tipo()==3) {
            ListaProdutos listaProdutos = verProdutos();
            if(listaProdutos.pertenceBaseUnidade(hierarquiaEliminar.getId()).size()==0){
                listaHierarquias.eliminarElemento(hierarquiaEliminar);
                listaHierarquias.uploadFicheiro();
                adicionarEntradaLog("Eliminar Hierarquia",hierarquia.mensagemLog(null));
                return true;
            }
        }
        return false;
    }

    /**
     * Envio de uma nova mensagem para todos os clientes
     * @param assuntoTxt String
     * @param mensagemTxt String
     */
    public void envioMensagem(String assuntoTxt, String mensagemTxt){
        ListaMensagens listaMensagens = verMensagens();
        Mensagem mensagem = new Mensagem(assuntoTxt,mensagemTxt,this);
        listaMensagens.adicionarElemento(mensagem);
        listaMensagens.uploadFicheiro();

        ListaEnvioMensagens listaEnvioMensagens = verEnviosMensagens();
        ListaPessoas listaClientes = verClientes();

        for(Pessoa destinatario: listaClientes.getLista()){
            listaEnvioMensagens.adicionarElemento(new EnvioMensagem((Cliente) destinatario,mensagem));
        }
        listaEnvioMensagens.uploadFicheiro();


    }

    /**
     * Adiciona novo produto a lista de produtos. E inserir nos appLoja.Logs a respetiva entrada.
     * @param novoProduto Produto (a adicionar)
     * @return true se ação foi realizada, false se já existe um produto igual
     */
    public boolean novoProduto (Produto novoProduto) {
        ListaProdutos listaProdutos = verProdutos();
        if(listaProdutos.pesquisa(novoProduto.getNome(),novoProduto.getMarca(),novoProduto.getId())==null){
            listaProdutos.adicionarElemento(novoProduto);
            listaProdutos.uploadFicheiro();
            adicionarEntradaLog("Adicionar Produto",novoProduto.mensagemLog(null));
            return true;
        }
        return false;
    }

    /**
     * Editar um produto existente. E inserir nos appLoja.Logs a respetiva entrada.
     * @param nome String
     * @param descricao String
     * @param marca String
     * @param stock String
     * @param unidade String
     * @param baseUnidade BaseUnidade
     * @param preco String
     * @param iva String
     * @param desconto String
     * @param produto Produto (a editar)
     * @return true se ação foi realizada, false se já existe um produto igual
     */
    public boolean editarProduto (String nome, String descricao, String marca, String stock, String unidade, BaseUnidade baseUnidade,
            String preco, String iva, String desconto, Produto produto) {

        ListaProdutos listaProdutos = verProdutos();
        Produto produtoEditar= (Produto) listaProdutos.pesquisa(produto.getId());

        if(listaProdutos.pesquisa(nome,marca,produto.getId())==null){
            if(produto.tipo()==1){
                produtoEditar.editar(nome,descricao,marca,Integer.parseInt(stock),unidade,baseUnidade,
                        Double.parseDouble(preco),Double.parseDouble(iva)/100.0,Double.parseDouble(desconto)/100.0);
            }else if(produto.tipo()==2){
                ((Granel)produtoEditar).editar(nome,descricao,marca,Double.parseDouble(stock),unidade,baseUnidade,
                        Double.parseDouble(preco),Double.parseDouble(iva)/100.0,Double.parseDouble(desconto)/100.0);
            }
            listaProdutos.uploadFicheiro();
            adicionarEntradaLog("Editar Produto",produtoEditar.mensagemLog(produto));
            return true;
        }
       return false;
    }

    /**
     * Emilima um produto existente. Verifica se o produto já foi vendido antes de executar E inserer nos appLoja.Logs a respetiva entrada.
     * @param produto Produto
     * @return true, se for possivel eleiminar
     */
    public boolean eliminarProduto (Produto produto) {
        ListaVendas listaVendas = verVendas();


        if(!listaVendas.existeVendaProduto(produto.getId())){
            ListaProdutos listaProdutos = verProdutos();
            Produto produtoEliminar = (Produto) listaProdutos.pesquisa(produto.getId());
            listaProdutos.eliminarElemento(produtoEliminar);
            listaProdutos.uploadFicheiro();
            adicionarEntradaLog("Eliminar Produto",produto.mensagemLog(null));
            return true;
        }
        return false;
    }

    /**
     * Adiciona uma entrada nos appLoja.Logs
     * @param tipo String (tipo de entrada)
     * @param mensagem String (mensagem das alterações)
     */
    public void adicionarEntradaLog (String tipo, String mensagem) {
        ListaLogs listaLogs = verLogs();
        listaLogs.adicionarElemento(new Log(this,tipo,mensagem));
        listaLogs.uploadFicheiro();
    }

    @Override
    public String tipoString() {
        return "Gerente";
    }

    @Override
    public int tipo() {
        return 2;
    }


    /**
     * Devolve a lista de pagamentos
     * @return ListaPagamentos
     */
    public ListaPagamentos verPagamentos(){
        ListaPagamentos lista = PrincipalLoja.getListaPagamentos();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve a lista de funcionários
     * @return ListaPessoas
     */
    public ListaPessoas verFuncionarios() {
        ListaPessoas listaFuncionarios = PrincipalLoja.getListaFuncionarios();
        listaFuncionarios.downloadFicheiro();
        return listaFuncionarios;
    }

    /**
     * Devolve a lista de appLoja.Logs
     * @return ListaLogs
     */
    public ListaLogs verLogs() {
        ListaLogs listaLogs = PrincipalLoja.getListaLogs();
        listaLogs.downloadFicheiro();
        return listaLogs;
    }

    /**
     * Resumo das vendas da loja.
     * @param listaEncomendas Lista encomendas
     * @return String []
     */
    public String[] resumoVendasLoja(ListaEncomendas listaEncomendas){
        int nVendas=0;
        double valorVendas=0;
        for(Encomenda encomenda: listaEncomendas.getLista()){
            nVendas+=encomenda.numeroItens();
            valorVendas+=encomenda.getPagamento().getValor();
        }
        return new String[]{String.format("%d",nVendas),String.format("%.2f€",valorVendas)};
    }

    /**
     * Resumo estatistico do até ao inicio da preparação encomenda
     * @param listaEstados Lista Estados
     * @return String [0] - média ; String [1] -desvioPadrão
     */
    public String[] tempoPreparacao(ListaEstados listaEstados){
        double media=0;
        int nEncomendas=0;
        long intervaloTempoAux=0;
        double intervaloTempo=0;
        ArrayList <Double> intervalosTempo = new ArrayList<>();

        for(EstadoEncomenda estado: listaEstados.getLista()){
            if(estado.getTipoEstado()>1){
                intervaloTempoAux=estado.getDataPreparacao().getTimeInMillis()-estado.getDataEncomenda().getTimeInMillis();
                //Conversão Milisegundos para Horas: 1h*60min*60s*1000ms
                intervaloTempo=intervaloTempoAux/(1*60*60*1000.0);
                intervalosTempo.add(intervaloTempo);

                media+=intervaloTempo;
                nEncomendas++;
            }
        }
        if(nEncomendas==0){
            return new String[]{"0.00 h","0.00 h"};
        }
        media=media/nEncomendas;

        double desvioPadrao=0;
        for(double i: intervalosTempo){
                desvioPadrao+=Math.pow(i-media,2);
        }
        desvioPadrao=desvioPadrao/nEncomendas;
        desvioPadrao=Math.sqrt(desvioPadrao);
        return new String[] {String.format("%.2f h",media),String.format("%.2f h",desvioPadrao)};
    }

    /**
     * Resumo estatistico do tempo de preparação
     * @param listaEstados Lista Estados
     * @return String [0] - média ; String [1] -desvioPadrão
     */
    public String[] tempoDespachar(ListaEstados listaEstados){
        double media=0;
        int nEncomendas=0;
        long intervaloTempoAux=0;
        double intervaloTempo=0;
        ArrayList <Double> intervalosTempo = new ArrayList<>();

        for(EstadoEncomenda estado: listaEstados.getLista()){
            if(estado.getTipoEstado()>2){
                intervaloTempoAux=estado.getDataExpedicao().getTimeInMillis()-estado.getDataPreparacao().getTimeInMillis();
                //Conversão Milisegundos para Horas: 1h*60min*60s*1000ms
                intervaloTempo=intervaloTempoAux/(1*60*60*1000.0);
                intervalosTempo.add(intervaloTempo);

                media+=intervaloTempo;
                nEncomendas++;
            }
        }
        if(nEncomendas==0){
            return new String[]{"0.00 h","0.00 h"};
        }
        media=media/nEncomendas;

        double desvioPadrao=0;
        for(double i: intervalosTempo){
            desvioPadrao+=Math.pow(i-media,2);
        }
        desvioPadrao=desvioPadrao/nEncomendas;
        desvioPadrao=Math.sqrt(desvioPadrao);
        return new String[] {String.format("%.2f h",media),String.format("%.2f h",desvioPadrao)};
    }

    /**
     * Devovle a percentagem e valor absoluto dos pagamentos por tipo
     * @return double[tipo][0-percentagem / 1-absoluto]
     */
    public String[][] metodosPagamentosUsados(){
        ListaPagamentos listaPagamentos = verPagamentos();

        double[][] valoresPagamentos = new double[3][2];

        //Calcular Somas
        for(Pagamento pagamento:listaPagamentos.getLista()){
            valoresPagamentos[pagamento.tipo()-1][0]++;
            valoresPagamentos[pagamento.tipo()-1][1]=pagamento.getValor();
        }

        String[][] valores = new String[3][2];
        //escrita
        for(int i=0; i<valoresPagamentos.length;i++){
            valores[i][0]=String.format("%.0f",valoresPagamentos[i][0]);
            valores[i][1]=String.format("%.2f€",valoresPagamentos[i][1]);
        }

        return valores;
    }

}
