package appLoja.Pessoas;

import appLoja.Encomendas.ListaEncomendas;
import appLoja.Encomendas.ListaEstados;
import appLoja.Encomendas.ListaVendas;
import appLoja.Mensagens.ListaEnvioMensagens;
import appLoja.Mensagens.ListaMensagens;
import appLoja.PrincipalLoja;
import appLoja.Produtos.ListaHierarquias;
import appLoja.Produtos.ListaProdutos;

/**
 * Interface Utilizador Loja
 * Contem todos os métodos que são comuns ao Gerente e Funcionários, como ver clientes, produtos.
 */
public interface UtilizadorLoja {
    /**
     * Devolve a Lista de produtos
     * @return ListaProdutos
     */
    default ListaProdutos verProdutos(){
        ListaProdutos lista = PrincipalLoja.getListaProdutos();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve a Lista de hierarquias
     * @return ListaHierarquias
     */
    default ListaHierarquias verHierarquias(){
        ListaHierarquias lista = PrincipalLoja.getListaHierarquias();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve a Lista de produtos
     * @return ListaPessoas
     */
    default ListaPessoas verClientes(){
        ListaPessoas lista = PrincipalLoja.getListaClientes();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve listaMensagens
     * @return listaMensagens
     */
    default ListaMensagens verMensagens(){
        ListaMensagens lista = PrincipalLoja.getListaMensagens();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve listaEnviosMensagens
     * @return Lista
     */
    default ListaEnvioMensagens verEnviosMensagens(){
        ListaEnvioMensagens lista = PrincipalLoja.getListaEnvioMensagens();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devovle a lista de encomendas já Realizadas
     * @return ListaEncomendas
     */
    default ListaEncomendas verEncomendas() {
        ListaEncomendas lista = PrincipalLoja.getListaEncomendas();
        lista.downloadFicheiro();
        lista=lista.encomendasRealizadas();
        return lista;
    }

    /**
     * Devolve a Lista de produtos
     * @return listaVendas
     */
    default ListaVendas verVendas(){
        ListaVendas lista = PrincipalLoja.getListaVendas();
        lista.downloadFicheiro();
        return lista;
    }

    /**
     * Devolve a Lista de produtos
     * @return listaVendas
     */
    default ListaEstados verEstados(){
        ListaEstados lista = PrincipalLoja.getListaEstados();
        lista.downloadFicheiro();
        return lista;
    }
}
