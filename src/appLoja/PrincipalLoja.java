package appLoja;

import Auxiliar.GestaoFicheiros;
import Auxiliar.VerificacaoDados;
import Comuns.ListaPagamentos;
import Comuns.Pessoa;
import GUI.Loja.LoginLoja;
import appLoja.Encomendas.ListaEncomendas;
import appLoja.Encomendas.ListaEstados;
import appLoja.Encomendas.ListaVendas;
import appLoja.Logs.ListaLogs;
import appLoja.Mensagens.ListaEnvioMensagens;
import appLoja.Mensagens.ListaMensagens;
import appLoja.Pessoas.ListaPessoas;
import appLoja.Produtos.ListaHierarquias;
import appLoja.Produtos.ListaProdutos;

import javax.swing.*;
import java.awt.*;
import java.util.Locale;

public class PrincipalLoja {
    //Utilizador cm login
    /**
     * Se não for assim após actualizar os dados é necessário REINICIAR o programa
     */
    private static Pessoa utilizador;
    private static ListaPessoas listaFuncionarios;
    private static ListaPessoas listaClientes;
    private static ListaEncomendas listaEncomendas;
    private static ListaPagamentos listaPagamentos;
    private static ListaVendas listaVendas;
    private static ListaEstados listaEstados;
    private static ListaHierarquias listaHierarquias;
    private static ListaProdutos listaProdutos;
    private static ListaMensagens listaMensagens;
    private static ListaEnvioMensagens listaEnvioMensagens;
    private static ListaLogs listaLogs;
    private static VerificacaoDados verificar;

    public static void main(String[] args) {

        listaFuncionarios=new ListaPessoas(1);
        boolean existeFuncionarios = listaFuncionarios.verificaFicheiro();
        listaClientes=new ListaPessoas(2);
        boolean existeClientes = listaClientes.verificaFicheiro();
        listaHierarquias=new ListaHierarquias();
        boolean existeHieraquias = listaHierarquias.verificaFicheiro();
        listaProdutos=new ListaProdutos();
        boolean existeProdutos = listaProdutos.verificaFicheiro();
        listaMensagens=new ListaMensagens();
        boolean existeMensagens = listaMensagens.verificaFicheiro();
        listaEnvioMensagens=new ListaEnvioMensagens();
        boolean existeEnvioMensagens = listaEnvioMensagens.verificaFicheiro();
        listaLogs=new ListaLogs();
        boolean existeLogs = listaLogs.verificaFicheiro();
        listaEncomendas=new ListaEncomendas();
        boolean existeEncomendas = listaEncomendas.verificaFicheiro();
        listaPagamentos= new ListaPagamentos();
        boolean existePagamentos=listaPagamentos.verificaFicheiro();
        listaVendas= new ListaVendas();
        boolean existeVendas = listaVendas.verificaFicheiro();
        listaEstados= new ListaEstados();
        boolean existeEstados = listaEstados.verificaFicheiro();


        Locale.setDefault(Locale.US);
        ImageIcon icon = new ImageIcon("resources/Icon.png");

        //Gerar os "cards"
        LoginLoja login = new LoginLoja(listaFuncionarios);

        //Inicio Janela
        JFrame janela = new JFrame("EletroJava BackOffice");

        verificar=VerificacaoDados.getInstancia();
        verificar.setPainelPai(janela.getContentPane());


        janela.setIconImage(icon.getImage());
        janela.setLayout(new CardLayout());
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        janela.add(login.adicionarJanela(), "Login");
        janela.setSize(1200,700);
        janela.setVisible(true);



        if(!existeFuncionarios || !existeClientes || ! existeEncomendas||!existeEnvioMensagens||!existeMensagens
        ||!existeEstados||!existeVendas||!existeHieraquias||!existeProdutos||!existeLogs||!existePagamentos){
            String mensagemErro="Foram gerados novos documenentos:\n";
            if(!existeFuncionarios){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoFuncionarios+"';\n";
            }
            if(!existeClientes){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoCliente+"';\n";
            }
            if(!existeMensagens){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoMensagens+"';\n";
            }
            if(!existeEnvioMensagens){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEnvioMensagens+"';\n";
            }
            if(!existeHieraquias){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoHierarquias+"';\n";
            }
            if(!existeProdutos){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoProdutos+"';\n";
            }
            if(!existeLogs){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoLogs+"';\n";
            }
            if(!existeEncomendas){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEncomendas+"';\n";
            }
            if(!existeVendas){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoVendas+"';\n";
            }
            if(!existeEstados){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoEstados+"';\n";
            }
            if(!existePagamentos){
                mensagemErro+= ">'"+GestaoFicheiros.caminhoPagamentos+"';\n";
            }
            JOptionPane.showMessageDialog(janela,
                    mensagemErro,
                    "Erro Sistema", JOptionPane.INFORMATION_MESSAGE);

        }

    }

    public static void setUtilizador(Pessoa utilizador) {
        PrincipalLoja.utilizador = utilizador;
    }

    public static Pessoa getUtilizador() {
        return utilizador;
    }

    public static ListaPessoas getListaFuncionarios() {
        return listaFuncionarios;
    }

    public static ListaPessoas getListaClientes() {
        return listaClientes;
    }

    public static ListaHierarquias getListaHierarquias() {
        return listaHierarquias;
    }

    public static ListaProdutos getListaProdutos() {
        return listaProdutos;
    }

    public static ListaEncomendas getListaEncomendas() {
        return listaEncomendas;
    }

    public static ListaPagamentos getListaPagamentos() {
        return listaPagamentos;
    }

    public static ListaVendas getListaVendas() {
        return listaVendas;
    }

    public static ListaEstados getListaEstados() {
        return listaEstados;
    }

    public static ListaMensagens getListaMensagens() {
        return listaMensagens;
    }

    public static ListaEnvioMensagens getListaEnvioMensagens() {
        return listaEnvioMensagens;
    }

    public static ListaLogs getListaLogs() {
        return listaLogs;
    }

}
