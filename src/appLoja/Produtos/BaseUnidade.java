package appLoja.Produtos;

import java.util.ArrayList;

/**
 * Class que representa o nivel mais baixo na hiearquia de produtos
 */
public class BaseUnidade extends HierarquiaProdutos {

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaProdutos - pai da BaseUnidade
     */
    public BaseUnidade(String nome, String descricao, HierarquiaProdutos parent) {
        super(nome, descricao, parent);
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaProdutos - pai da BaseUnidade
     */
    public BaseUnidade(int id, String nome, String descricao, HierarquiaProdutos parent) {
        super(id, nome, descricao, parent);
    }

    @Override
    public int tipo (){
        return 3;
    }

    @Override
    public String tipoString() {
        return "Base Unidade";
    }

    @Override
    public int produtosStock(ListaProdutos listaProdutos){
        ArrayList<Produto> produtos=  listaProdutos.pertenceBaseUnidade(this.id);
        int nProdutos=0;
        for(Produto produto: produtos){
            //Se for granel conta como 1 item
            if(produto.tipo()==2 && ((Granel)produto).getStockGranel()>0){
                nProdutos++;
            }else if(produto.tipo()==1){
                nProdutos+=produto.getStock();
            }
        }
        return nProdutos;
    }

    @Override
    public int produtosDiferentes(ListaProdutos listaProdutos) {
        return listaProdutos.pertenceBaseUnidade(this.getId()).size();
    }
}
