package appLoja.Produtos;

import java.util.ArrayList;

/**
 * Categoria contem um lista de baseUnidade associado
 */
public class Categoria extends HierarquiaProdutos {

    protected ArrayList<BaseUnidade> lista;

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaProdutos - pai da Categoria
     */
    public Categoria(String nome, String descricao, HierarquiaProdutos parent) {
        super(nome, descricao, parent);
        this.lista = new ArrayList<>();
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiaProdutos - pai da Categoria
     */
    public Categoria(int id, String nome, String descricao, HierarquiaProdutos parent) {
        super(id, nome, descricao, parent);
        this.lista = new ArrayList<>();
    }


    @Override
    public int tipo (){
        return 2;
    }

    @Override
    public String tipoString() {
        return "Categoria";
    }

    /**
     * Adiciona um elemento à listagem, serve para adicionar um filho
     * @param elemento HierarquiasProdutos - o que se quiser adicionar
     */
    public void adicionarElemento(BaseUnidade elemento){
        lista.add(elemento);
    }

    /**
     * Devolve um array list com os filhos da categoria
     * @return ArrayList UnidadeBase
     */
    public ArrayList<BaseUnidade> getLista() {
        return lista;
    }

    /**
     * Gerar uma lista de hierarquias com os filhos, é importante para gerar tabelas
     * @return ListaHierarquias com os filhos do categoria
     */
    public ListaHierarquias getListaFilhos(){
        ListaHierarquias listaFilhos=new ListaHierarquias();
        for(HierarquiaProdutos hierarquiaProdutos:lista){
            listaFilhos.adicionarElemento(hierarquiaProdutos);
        }
        return listaFilhos;
    }

    /**
     * Metodo que devovle um array de objectos, pronto para inserir na combox.
     * @return Object[] BaseUnidade
     */
    public Object[] getComboBoxFilhos(){
        ArrayList<HierarquiaProdutos> categorias = new ArrayList<>();
        for(HierarquiaProdutos hierarquiaProdutos:lista){
            categorias.add(hierarquiaProdutos);
        }
        return categorias.toArray();
    }

    /**
     * Procurar categoria pelo nome na Categoria importante para evitar UnidadesBase sob a mesma Categoria com o mesmo nome
     * @param nome String
     * @param idBaseUnidade int
     * @return BaseUnidade se encontrar, null se não existir
     */
    public BaseUnidade nomeRepetido(String nome, int idBaseUnidade){
        for(BaseUnidade baseUnidade: lista){
            if(baseUnidade.getId()!=idBaseUnidade && baseUnidade.getNome().toLowerCase().equals(nome.toLowerCase())){
                return baseUnidade;
            }
        }
        return null;
    }

    @Override
    public int produtosStock(ListaProdutos listaProdutos){
        int nProdutos=0;
        for(BaseUnidade baseUnidade:lista){
            nProdutos+=baseUnidade.produtosStock(listaProdutos);
        }
        return nProdutos;
    }

    @Override
    public int produtosDiferentes(ListaProdutos listaProdutos) {
        int nProdutos=0;
        for(BaseUnidade baseUnidade:lista){
            nProdutos+=baseUnidade.produtosDiferentes(listaProdutos);
        }
        return nProdutos;
    }
}
