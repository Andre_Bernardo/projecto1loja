package appLoja.Produtos;

import java.util.ArrayList;

/**
 * Departamento contem um lista de categorias associado
 */
public class Departamento extends HierarquiaProdutos {

    protected ArrayList<Categoria> lista;

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     */
    public Departamento(String nome, String descricao) {
        super(nome, descricao, null);
        this.lista = new ArrayList<>();
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     */
    public Departamento(int id, String nome, String descricao) {
        super(id, nome, descricao, null);
        this.lista = new ArrayList<>();
    }

    @Override
    public int tipo (){
        return 1;
    }

    @Override
    public String tipoString() {
        return "Departamento";
    }

    /**
     * Adiciona um elemento à listagem, serve para adicionar um filho
     * @param elemento HierarquiasProdutos - o que se quiser adicionar
     */
    public void adicionarElemento(Categoria elemento){
        lista.add(elemento);
    }


    /**
     * Devolve um array list com os filhos do Departamento
     * @return ArrayList Categoria
     */
    public ArrayList<Categoria> getLista() {
        return lista;
    }

    /**
     * Gerar uma lista de hierarquias com os filhos, é importante para gerar tabelas
     * @return ListaHierarquias com os filhos do departamento
     */
    public ListaHierarquias getListaFilhos(){
        ListaHierarquias listaFilhos=new ListaHierarquias();
        for(HierarquiaProdutos hierarquiaProdutos:lista){
            listaFilhos.adicionarElemento(hierarquiaProdutos);
        }
        return listaFilhos;
    }

    /**
     * Metodo que devovle um array de objectos, pronto para inserir na combox.
     * @return Object[] Categorias
     */
    public Object[] getComboBoxFilhos(){
        return lista.toArray();
    }

    /**
     * Procurar categoria pelo nome no departamento importante para evitar categorias sob o mesmo departamento com o mesmo nome
     * @param nome String
     * @param idCategoria int
     * @return Categoria, ou null se não encontrar
     */
    public Categoria nomeRepetido(String nome, int idCategoria){
        for(Categoria categoria: lista){
            if(categoria.getId()!=idCategoria && categoria.getNome().toLowerCase().equals(nome.toLowerCase())){
                return categoria;
            }
        }
        return null;
    }

    @Override
    public int produtosStock(ListaProdutos listaProdutos){
        int nProdutos=0;
        for(Categoria categoria:lista){
            nProdutos+=categoria.produtosStock(listaProdutos);
        }
        return nProdutos;
    }

    @Override
    public int produtosDiferentes(ListaProdutos listaProdutos) {
        int nProdutos=0;
        for(Categoria categoria:lista){
            nProdutos+=categoria.produtosDiferentes(listaProdutos);
        }
        return nProdutos;
    }
}
