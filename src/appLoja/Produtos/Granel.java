package appLoja.Produtos;

/**
 * Granel extende a funcionalidades do produto, permitindo o programa ter produtos que não sejam unitários
 */
public class Granel extends Produto{

    protected String unidade;
    protected double stock;

    /**
     * Construtor normal
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock double (em Granel, o stock tem casas décimais)
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Granel(String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, double stock, double preco, double iva, double desconto) {
        super(nome, descricao, unidadeBase, marca, unidade, 0, preco, iva, desconto);
        this.stock=stock;
        this.unidade=unidade;
    }

    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock double (em Granel, o stock tem casas décimais)
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Granel(int id, String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, double stock, double preco, double iva, double desconto) {
        super(id, nome, descricao, unidadeBase, marca, unidade, 0, preco, iva, desconto);
        this.stock=stock;
        this.unidade=unidade;
    }

    /**
     * Stock de um produto a granel é um double
     * @return quantidade em stock
     */
    public double getStockGranel() {
        return stock;
    }

    /**
     * Destingir o tipo de produto
     * 1- Unitário
     * 2- Granel
     * @return 2
     */
    @Override
    public  int tipo(){
        return 2;
    }


    /**
     * Edição de um produto
     * @param nome String
     * @param descricao String
     * @param marca String
     * @param stock double
     * @param unidade String
     * @param baseUnidade BaseUnidade
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public void editar(String nome, String descricao, String marca, double stock, String unidade,BaseUnidade baseUnidade, double preco, double iva, double desconto) {
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = baseUnidade;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;
        this.identificador=baseUnidade.getIdentificador()+String.format(":%03d",id);
    }

    /**
     * Escrita da mensgem para Log
     * @param produtoAntigo (produto original, para verificar difereças OU null caso seja um produto novo)
     * @return Mensagem para Log
     */
    @Override
    public String mensagemLog(Produto produtoAntigo){
        Granel produtoComparar = (Granel)produtoAntigo;
        String mensagem="";
        if(produtoComparar==null){
            mensagem=String.format("ID: %03d; IDENTIFICADOR: %s; NOME: %s; DESCRIÇÃO: %s; " +
                            "MARCA: %s; STOCK: %2f; UNI_VENDA: %s; PREÇO: %.2f; IVA: %.0f%%; DESCONTO: %.0f%%",
                    id,identificador,nome,descricao,marca,stock,unidade,preco,iva*100,desconto*100);
        }else{
            mensagem=String.format("ID: %03d;",id);
            if(!produtoComparar.identificador.equals(identificador)){
                mensagem+=String.format(" IDENTIFICADOR %s->%s;",produtoComparar.identificador,identificador);
            }
            if(!produtoComparar.nome.equals(nome)){
                mensagem+=String.format(" NOME %s->%s;",produtoComparar.nome,nome);
            }
            if(!produtoComparar.descricao.equals(descricao)){
                mensagem+=String.format(" DESCRIÇÃO %s->%s;",produtoComparar.descricao,descricao);
            }
            if(!produtoComparar.marca.equals(marca)){
                mensagem+=String.format(" MARCA %s->%s;",produtoComparar.marca,marca);
            }
            if(produtoComparar.stock!=stock){
                mensagem+=String.format(" STOCK %2f->%2f;",produtoComparar.stock,stock);
            }
            if(!produtoComparar.unidade.equals(unidade)){
                mensagem+=String.format(" UNI_VENDA %s->%s;",produtoComparar.unidade,unidade);
            }
            if(produtoComparar.preco!=preco){
                mensagem+=String.format(" PREÇO: %.2f€->%.2f€;",produtoComparar.preco,preco);
            }
            if(produtoComparar.iva!=iva){
                mensagem+=String.format(" IVA: %.0f%%->%.0f%%;",produtoComparar.iva*100,iva*100);
            }
            if(produtoComparar.desconto!=desconto){
                mensagem+=String.format(" DESCONTO: %.0f%%->%.0f%%;",produtoComparar.desconto*100,desconto*100);
            }
        }
        return mensagem;
    }
    /**
     * Criar uma instancia exatamente igual do produto (Granel), util para verificar diferenças
     * @return Produto
     */
    @Override
    public Granel clone(){
        return new Granel(id, nome, descricao, unidadeBase, marca, unidade, stock, preco, iva, desconto);
    }

    /**
     * Correção do stock ao empacotar uma encomenda
     * @param quantidade double
     */
    public boolean actualizarStock(double quantidade){
        if(stock>=quantidade){
            return true;
        }
        return false;
    }
}
