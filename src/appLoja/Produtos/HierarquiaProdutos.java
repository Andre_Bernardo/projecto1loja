package appLoja.Produtos;

import java.util.Comparator;

/**
 * Classe abstrata Hierarquia de produtos
 * Todas as hierarquias tem um ID, identificador nome e descrição
 */
public abstract class HierarquiaProdutos {
    private static int idGerado;

    protected int id;
    protected String identificador;
    protected String nome;
    protected String descricao;
    protected HierarquiaProdutos parent;

    /**
     * Construtor normal para a criação de uma nova hierarquia
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent HierarquiasProdutos - objecto Pai (é null no caso do departamento)
     */
    public HierarquiaProdutos(String nome, String descricao, HierarquiaProdutos parent) {
        idGerado++;
        this.id=idGerado;
        if(parent!=null){
            this.identificador = parent.getIdentificador()+String.format(":%03d",id);
        }else {
            this.identificador = String.format("%03d",id);
        }
        this.nome = nome;
        this.descricao = descricao;
        this.parent = parent;
    }

    /**
     * Construtor para o carregamento de dados.
     * @param id int - id
     * @param nome String - nome
     * @param descricao String - descrição
     * @param parent  HierarquiasProdutos - objecto Pai (é null no caso do departamento)
     */
    public HierarquiaProdutos(int id, String nome, String descricao, HierarquiaProdutos parent) {
        this.id=id;

        if(parent!=null){
            this.identificador = parent.getIdentificador()+String.format(":%03d",id);
        }else {
            this.identificador = String.format("%03d",id);
        }
        this.nome = nome;
        this.descricao = descricao;
        this.parent = parent;

        if(id>idGerado){
            idGerado=id;
        }
    }

    /**
     * Devolve o tipo de hierarquia
     * @return 1-Departamento, 2-Categoria, 3-UnidadeBase
     */
    public abstract int tipo ();

    /**
     * Escrita por extenso do tipo de hierarquia
     * @return Departamento, Categoria ou Base de Unidade
     */
    public abstract String tipoString();

    /**
     * Devolve o a quantidade de produtos existent em stock
     * @param listaProdutos ListaProdutos
     * @return int contagem
     */
    public abstract int produtosStock(ListaProdutos listaProdutos);

    /**
     * Devolve o numero diferente de produtos existentes
     * @param listaProdutos ListaProdutos
     * @return int contagem
     */
    public abstract int produtosDiferentes(ListaProdutos listaProdutos);

    /**
     * Devolve o id da hierarquia
     * @return int- id da hierarquia
     */
    public int getId() {
        return id;
    }

    /**
     * Devolve o "pai" da hierarquia
     * @return HiarquiaProdutos pai da hierarquia (Se for um Departamento pai = null)
     */
    public HierarquiaProdutos getParent() {
        return parent;
    }

    /**
     * Devovle o nome da hierarquia
     * @return String nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devovle a descrição da hierarquia
     * @return String descricao
     */
    public String getDescricao() {
        return descricao;
    }


    /**
     * Devovle o código de identificação da hierarquia
     * @return String indentificador
     */
    public String getIdentificador() {
        return identificador;
    }

    /**
     * Devolve uma array de objetos preparado para gerar uma tabela.
     * @return Devolve uma array de objetos preparado para gerar uma tabela.
     */
    public Object[] linhaTabela() {
        Object[] linha = new Object[5];
        linha[0]=getIdentificador();
        linha[1]=tipoString();

        if(tipo()==1){
            linha[2]="";
        }else{
            linha[2]=getParent().getNome();
        }

        linha[3]=getNome();
        linha[4]=getDescricao();

        return linha;
    }

    /**
     * Ordenação por identificador
     */
    public static Comparator<HierarquiaProdutos> ordenarIdentificador=new Comparator<HierarquiaProdutos>() {
        @Override
        public int compare(HierarquiaProdutos o1, HierarquiaProdutos o2) {
            return o1.getIdentificador().compareTo(o2.getIdentificador());
        }
    };

    /**
     * Editar os campos da hierarquia
     * @param nome String
     * @param descricao String
     */
    public void editarHierarquia(String nome, String descricao){
        this.nome=nome;
        this.descricao=descricao;
    }

    @Override
    public String toString() {
        return nome;
    }

    /**
     * Escreve a mensagem para log com as alterações efectuadas a hiearquia
     * @param original HierarquiaProdutos
     * @return String
     */
    public String mensagemLog(HierarquiaProdutos original){
        String mensagem="";

        if(original==null){
            mensagem=String.format("ID: %03d; IDENTIFICADOR %s; NOME: %s; DESCRIÇÃO: %s;",
                    id,identificador,nome,descricao);
        }else{
            mensagem=String.format("ID: %03d;",id);
            if(!original.identificador.equals(identificador)){
                mensagem+=String.format(" IDENTIFICADOR %s->%s;",original.identificador,identificador);
            }
            if(!original.nome.equals(nome)){
                mensagem+=String.format(" NOME %s->%s;",original.nome,nome);
            }
            if(!original.descricao.equals(descricao)){
                mensagem+=String.format(" DESCRIÇÃO %s->%s;",original.descricao,descricao);
            }
        }

        return mensagem;

    }
}
