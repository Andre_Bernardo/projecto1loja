package appLoja.Produtos;

import Auxiliar.FileHandler;
import Auxiliar.GestaoFicheiros;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Listar hiearquias
 */
public class ListaHierarquias implements GestaoFicheiros {

    private ArrayList<HierarquiaProdutos> lista;
    private String localFicheiro;

    /**
     * Construtor vazio
     */
    public ListaHierarquias(){
        lista=new ArrayList<>();
        localFicheiro=caminhoHierarquias;
    }

    /**
     * Construtor para criar uma lista a aprtir de uma listagem de produtos
     * @param listagem ArrayList HierarquiaProdutos
     */
    public ListaHierarquias(ArrayList<HierarquiaProdutos> listagem){
        lista=listagem;
    }

    /**
     * Construtor com indicação do ficheiro que serve de "base de dados"
     * @param localFicheiro String - caminho até ficheiro (caso diferente do que está na interface)
     */
    public ListaHierarquias(String localFicheiro){
        this.localFicheiro=localFicheiro;
        lista=new ArrayList<>();
    }

    /**
     * Pesquisa de HierarquiaProdutos pelo respetivo ID.
     * Devolve a HierarquiaProdutos que tem o ID igual ao argumento dado ou devolve NULL caso não encontre correspondencia.
     * @param id int
     * @return Devolve (HierarquiaProdutos)Object ou NULL
     */
    @Override
    public Object pesquisa(int id) {
        for(HierarquiaProdutos hierarquiasProdutos:lista){
            if(hierarquiasProdutos.id==id){
                return hierarquiasProdutos;
            }
        }
        return null;
    }


    /**
     * Pesquisa de HierarquiaProdutos pelo respetivo códgigo identificação.
     * Devolve a HierarquiaProdutos que tem o identificação igual ao argumento dado ou devolve NULL caso não encontre correspondencia.
     * @param identificador string procura
     * @return Devolve HierarquiaProdutos ou NULL
     */
    public HierarquiaProdutos pesquisa(String identificador){
        for(HierarquiaProdutos hierarquiasProdutos:lista){
            if(hierarquiasProdutos.identificador.equals(identificador)){
                return hierarquiasProdutos;
            }
        }
        return null;
    }

    /**
     * Devolve o array do tipo com os departamentos.
     * @return departamentos[]
     */
    public Departamento[] departamentos(){
        int i=0;
        for(HierarquiaProdutos hierarquia: lista){
            if(hierarquia.tipo()==1){
                i++;
            }
        }
        Departamento[] departamentos=new Departamento[i];
        i=0;
        for(HierarquiaProdutos hierarquia: lista){
            if(hierarquia.tipo()==1){
                departamentos[i]=(Departamento) hierarquia;
                i++;
            }
        }
        return departamentos;
    }

    /**
     * Procurar na lista de departamentnos, um departamento que tenha o nome igual ao argumento.
     * @param nome String
     * @param idDepartamento int
     * @return Departamento, ou null se não encontrar.
     */
    public Departamento nomeRepetidoDepartamento(String nome, int idDepartamento){
        Departamento[] departamentos = departamentos();
        for(Departamento departamento: departamentos){
            if(departamento.getId()!=idDepartamento && departamento.getNome().toLowerCase().equals(nome.toLowerCase())){
                return departamento;
            }
        }
        return null;
    }

    @Override
    public void downloadFicheiro() {

        try{
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedReader conteudo = ficheiro.openFileReader(localFicheiro);

            //Separar linha em linha
            String linha= conteudo.readLine();

            lista.clear();

            String[] linhaPartida;

            int id;
            HierarquiaProdutos aCriar;
            int parentID;
            HierarquiaProdutos parent;
            String nome;
            String descricao;

            char tipo;
            while (linha!=null){
                linhaPartida=linha.split(divisorColunas);
                tipo=linhaPartida[2].charAt(0);

                id=Integer.parseInt(linhaPartida[0]);
                parentID=Integer.parseInt(linhaPartida[1]);
                nome=linhaPartida[3];
                descricao=linhaPartida[4];

                switch (tipo){
                    case '1':{
                        aCriar=new Departamento(id,nome,descricao);
                        lista.add(aCriar);
                    }break;
                    case '2':{
                        parent=(Departamento)pesquisa(parentID);
                        aCriar=new Categoria(id,nome,descricao,parent);
                        lista.add(aCriar);
                        ((Departamento)parent).adicionarElemento((Categoria) aCriar);
                    }break;
                    case '3':{
                        parent=(Categoria)pesquisa(parentID);
                        aCriar=new BaseUnidade(id,nome,descricao,parent);
                        lista.add(aCriar);
                        ((Categoria)parent).adicionarElemento((BaseUnidade) aCriar);
                    }break;
                    default:{
                        System.out.println("Erro de leitura da linha");
                    }break;
                }
                linha=conteudo.readLine();
            }
            ficheiro.closeFileReader();
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    /**
     * Descarrefar informação da lista para o ficheiro
     */
    @Override
    public void uploadFicheiro() {

        try {
            //Abrir Ficheiro
            FileHandler ficheiro = new FileHandler();

            //Ler conteudo do ficheiro
            BufferedWriter conteudo = ficheiro.tryOpenAndLockFileWriter(localFicheiro);

            String linha;

            //Ordenar elementos para evitar erros de consistencia
            ordenarElementos();
            for(HierarquiaProdutos hierarquia: lista){
                if(hierarquia.getParent()==null){
                    linha=hierarquia.getId()+divisorColunas+
                            "0"+divisorColunas+
                            hierarquia.tipo()+divisorColunas+
                            hierarquia.getNome()+divisorColunas+
                            hierarquia.getDescricao();
                }else{
                    linha=hierarquia.getId()+divisorColunas+
                            hierarquia.getParent().getId()+divisorColunas+
                            hierarquia.tipo()+divisorColunas+
                            hierarquia.getNome()+divisorColunas+
                            hierarquia.getDescricao();
                }



                conteudo.write(linha,0,linha.length());
                conteudo.newLine();
            }
            ficheiro.releaseLockWriter();
            ficheiro.closeFileWriter();



        }catch (IOException e){
            JOptionPane.showMessageDialog(null,
                    "ERRO LIGAÇÃO BD: ERRO:\n"+e+"\n" +
                            "Programa irá encerrar",
                    "Erro Sistema", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }


    }

    @Override
    public boolean verificaFicheiro(){
        FileHandler ficheiro = new FileHandler();
        try{
            //Verificar existencia do ficheiro
            ficheiro.openFileReader(localFicheiro);
            ficheiro.closeFileWriter();
            return true;
        }catch (IOException e){
            try {
                //Caso não exista cria um novo
                ficheiro.openFileWriter(localFicheiro);
                ficheiro.closeFileWriter();
            } catch (Exception ex) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public String[] listarCabecalhoTabela(){
        String[] infoTabela = new String[5];
        //Cabeçalho
        infoTabela[0]="Identificador";
        infoTabela[1]="Tipo";
        infoTabela[2]="Pai";
        infoTabela[3]="Nome";
        infoTabela[4]="Descrição";
        return infoTabela;
    }

    /**
     * Devolve array de Objects que são os dados da tabela.
     *
     * @return Devolve um array de objectos com os dados da tabela
     */
    @Override
    public Object[][] listarDadosTabela() {
        Object[][] infoTabela = new Object[lista.size()][6];

        ordenarElementos();
        //Informação
        int i=0;
        for(HierarquiaProdutos hierarquia:lista){
            infoTabela[i]=hierarquia.linhaTabela();
            i++;
        }
        return infoTabela;
    }

    /**
     * metodo para adicionar elelmentos a lista
     * @param object Object o objecto a adicionar a listagem
     */
    @Override
    public void adicionarElemento(Object object) {
        lista.add((HierarquiaProdutos)object);
        ordenarElementos();
    }

    /**
     * metodo para eliminar um elemento da lista
     * @param hierarquia objecto a revomer da lista
     */
    public void eliminarElemento(HierarquiaProdutos hierarquia){
        lista.remove(hierarquia);
        ordenarElementos();
    }

    /**
     * Ordenar elementos por identificador
     */
    private void ordenarElementos(){
        lista.sort(HierarquiaProdutos.ordenarIdentificador);
    }

    /**
     * Conta quantos itens estão guardados no ficheiro .txt
     * @return quantidade total
     */
    @Override
    public int getTotal(){
        downloadFicheiro();
        int count=0;
        for(HierarquiaProdutos hierarquia:lista){
            count++;
        }
        return count;
    }

}
