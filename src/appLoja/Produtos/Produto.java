package appLoja.Produtos;

import java.util.Comparator;

/**
 * Produto é uma class que organiza a informação regeferentes ao produto, bem como os seus metodos
 */
public class Produto {

    private static int idGerado;

    protected int id;
    protected String identificador;
    protected String nome;
    protected String descricao;
    protected BaseUnidade unidadeBase;
    protected String marca;
    protected String unidade;
    protected int stock;
    protected double preco;
    protected double iva;
    protected double desconto;

    /**
     * Construtor normal
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock int
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Produto(String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, int stock, double preco, double iva, double desconto) {
        idGerado++;

        this.id = idGerado;
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = unidadeBase;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;

        this.identificador=unidadeBase.getIdentificador()+String.format(":%03d",id);
    }


    /**
     * Construtor para a inportação de dados a partir de um documento.
     * Nesta situação o ID já está definido e portanto tenho apenas que actualizar o idGerado para o valor maxímo existente
     * @param id int
     * @param nome String
     * @param descricao String
     * @param unidadeBase UnidadeBase
     * @param marca String
     * @param unidade String (a forma como é vendida)
     * @param stock int
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public Produto(int id, String nome, String descricao, BaseUnidade unidadeBase, String marca, String unidade, int stock, double preco, double iva, double desconto) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = unidadeBase;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;

        this.identificador=unidadeBase.getIdentificador()+String.format(":%03d",id);

        if(id>idGerado){
            idGerado=id;
        }
    }

    /**
     * Destingir o tipo de produto
     * 1- Unitário
     * 2- Granel
     * @return 1
     */
    public  int tipo(){
        return 1;
    }

    /**
     * Devole o id do produto
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * Devolve o código de identificação do produto
     * @return String
     */
    public String getIdentificador() {
        return identificador;
    }

    /**
     * Devolve o nome do produto
     * @return String
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve descrição do produto
     * @return String
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve o Objecto UnidadeBase a que o produto pertence
     * @return UnidadeBase
     */
    public BaseUnidade getUnidadeBase() {
        return unidadeBase;
    }

    /**
     * Devolve a marca do produto
     * @return String
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Devolve a unidade de venda do produto
     * @return String
     */
    public String getUnidade() {
        return unidade;
    }

    /**
     * Devolve stock do produto
     * @return int
     */
    public int getStock() {
        return stock;
    }

    /**
     * Devolve o preço do produto;
     * @return double
     */
    public double getPreco() {
        return preco;
    }

    /**
     * Devolve o IVA sob o produto
     * @return double
     */
    public double getIva() {
        return iva;
    }

    /**
     * Devolve o desconto sob o produto
     * @return double
     */
    public double getDesconto() {
        return desconto;
    }

    /**
     * Devolve a categoria a que pertence
     * @return Categoria
     */
    public Categoria getCategoria(){
        return (Categoria) unidadeBase.getParent();
    }

    /**
     * Devolve o nome da deparamento a que pertence
     * @return Departamento
     */
    public Departamento getDepartamento(){
        return (Departamento) unidadeBase.getParent().getParent();
    }

    /**
     * Devolve o valor de venda ao publico (adição de IVA e desconto incluidos)
     * @return double
     */
    public double precoUnitarioVendaComDesconto(){
        double valorTratado=(preco*(1+iva-desconto))*100;
        valorTratado=Math.round(valorTratado);
        return valorTratado/100.0;
    }

    public Object[] linhaTabela(){
        Object[] linha = new Object[11];
        linha[0]=identificador;
        linha[1]=getDepartamento().getNome();
        linha[2]=getCategoria().getNome();
        linha[3]=getUnidadeBase().getNome();
        linha[4]=nome;
        linha[5]=marca;
        if(tipo()==1){
            linha[6]=(double)stock;
        }else{
            linha[6]=((Granel)this).getStockGranel();
        }
        linha[7]=preco;
        linha[8]=(iva*100);
        linha[9]=(desconto*100);
        linha[10]=precoUnitarioVendaComDesconto();

        return linha;
    }

    /**
     * Ordenação por identificador
     */
    public static Comparator<Produto> ordenarIdentificador=new Comparator<Produto>() {
        @Override
        public int compare(Produto o1, Produto o2) {
            return o1.getIdentificador().compareTo(o2.getIdentificador());
        }
    };

    /**
     * Edição de um produto
     * @param nome String
     * @param descricao String
     * @param marca String
     * @param stock int
     * @param unidade String
     * @param baseUnidade BaseUnidade
     * @param preco double
     * @param iva double
     * @param desconto double
     */
    public void editar(String nome, String descricao, String marca, int stock, String unidade, BaseUnidade baseUnidade, double preco, double iva, double desconto) {
        this.nome = nome;
        this.descricao = descricao;
        this.unidadeBase = baseUnidade;
        this.marca = marca;
        this.unidade = unidade;
        this.stock = stock;
        this.unidade= unidade;
        this.preco = preco;
        this.iva = iva;
        this.desconto = desconto;
        this.identificador=baseUnidade.getIdentificador()+String.format(":%03d",id);
    }

    /**
     * Escrita da mensagem para log
     * @param produtoAntigo (produto original, para verificar difereças OU null caso seja um produto novo)
     * @return Mensagem para Log
     */
    public String mensagemLog(Produto produtoAntigo){
        String mensagem="";
        if(produtoAntigo==null){
            mensagem=String.format("ID: %03d; IDENTIFICADOR: %s; NOME: %s; DESCRIÇÃO: %s; " +
                    "MARCA: %s; STOCK: %2d; UNI_VENDA: %s; PREÇO: %.2f; IVA: %.0f%%; DESCONTO: %.0f%%",
                    id,identificador,nome,descricao,marca,stock,unidade,preco,iva*100,desconto*100);
        }else{
            mensagem=String.format("ID: %03d;",id);
            if(!produtoAntigo.identificador.equals(identificador)){
                mensagem+=String.format(" IDENTIFICADOR %s->%s;",produtoAntigo.identificador,identificador);
            }
            if(!produtoAntigo.nome.equals(nome)){
                mensagem+=String.format(" NOME %s->%s;",produtoAntigo.nome,nome);
            }
            if(!produtoAntigo.descricao.equals(descricao)){
                mensagem+=String.format(" DESCRIÇÃO %s->%s;",produtoAntigo.descricao,descricao);
            }
            if(!produtoAntigo.marca.equals(marca)){
                mensagem+=String.format(" MARCA %s->%s;",produtoAntigo.marca,marca);
            }
            if(produtoAntigo.stock!=stock){
                mensagem+=String.format(" STOCK %2d->%2d;",produtoAntigo.stock,stock);
            }
            if(!produtoAntigo.unidade.equals(unidade)){
                mensagem+=String.format(" UNI_VENDA %s->%s;",produtoAntigo.unidade,unidade);
            }
            if(produtoAntigo.preco!=preco){
                mensagem+=String.format(" PREÇO: %.2f€->%.2f€;",produtoAntigo.preco,preco);
            }
            if(produtoAntigo.iva!=iva){
                mensagem+=String.format(" IVA: %.0f%%->%.0f%%;",produtoAntigo.iva*100,iva*100);
            }
            if(produtoAntigo.desconto!=desconto){
                mensagem+=String.format(" DESCONTO: %.0f%%->%.0f%%;",produtoAntigo.desconto*100,desconto*100);
            }
        }
        return mensagem;
    }

    /**
     * Criar uma instancia exatamente igual do produto, util para verificar diferenças
     * @return Produto
     */
    @Override
    public Produto clone(){
        return new Produto(id, nome, descricao, unidadeBase, marca, unidade, stock, preco, iva, desconto);
    }

    /**
     * metodo para actualizar o stock do produto
     * @param quantidade double
     * @return true se foi actualizado
     */
    public boolean actualizarStock(double quantidade){
        if(stock>=quantidade){
            stock-=(int)quantidade;
            return true;
        }
        return false;
    }
}
